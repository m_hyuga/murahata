<?php
	require_once("./dbini_mht.php");
	
	/* とりあえずDB接続 */
	$con = mysql_connect($DBSERVER,$DBUSER,$DBPASSWORD);
	$selectdb = mysql_select_db($DBNAME,$con);
	// SET NAMES クエリの発行
	$sql = "SET NAMES utf8";
	$rst = mysql_query($sql,$con);
	
	if($_POST['mode'] == "list"){
		//新着リスト
		echo getNewsList();
	}else if($_POST['mode'] == "detail"){
		echo getNewsDetail();
	}
	
	
	
	function getNewsList(){
		global $con;
		
		if($_POST['key'] != "mht"){
			return "error";
		}

		
		$sql = "select ";
		$sql .= " mht_item.id as id, ";
		$sql .= " mht_item.title as title, ";
		$sql .= " mht_item.body as body, ";
		$sql .= " coalesce(aaa.filename,'') as filename, ";
		$sql .= " date_format(mht_item.date,'%Y/%c/%d') as date, ";
		$sql .= " mht_item.alive as alive ";
		$sql .= " from (mht_item ";
		$sql .= " left join mht_category ";
		$sql .= " on mht_category.id = mht_item.category_id) ";
		$sql .= " left join (select item_id,filename from mht_images where order_no = 0) as aaa ";
		$sql .= " on mht_item.id = aaa.item_id ";
		$sql .= " where mht_category.type = 'news'";
		$sql .= " and mht_item.alive = 1";
		$sql .= " order by mht_item.date desc,id desc ";
		$sql .= " limit 0,5 ";
		
		$rst = mysql_query($sql,$con);
		if($rst){
			$data = "<data>\n";
			while($col = mysql_fetch_array($rst)){
				$data .= "<itemdata>\n";
				foreach($col as $key => $value){
					if(!is_numeric($key)){
						$data .= "<".$key.">".$value."</".$key.">\n";
					}
				}
				$data .= "</itemdata>\n";
			}
			$data .= "</data>\n";
			return $data;
		}else{
			return "error";
		}

	}
	
	/* 詳細情報 */
	function getNewsDetail(){
		global $con;
		
		if($_POST['key'] != "mht"){
			return "error";
		}

		$data = "<data>\n";
		
		$sql = "select ";
		$sql .= " id,title,body,order_no,alive, ";
		$sql .= " date_format(mht_item.date,'%Y/%c/%d') as date ";
		$sql .= " from mht_item ";
		$sql .= " where id = ".$_POST['id'];

		$rst = mysql_query($sql,$con);
		if($rst){
			$col = mysql_fetch_array($rst);
			$data .= "<itemdata>\n";
			foreach($col as $key => $value){
				if(!is_numeric($key)){
					$data .= "<".$key.">".$value."</".$key.">\n";
				}
			}
			$data .= "</itemdata>\n";
		}else{
			return "error";
		}
		
		$sql = "select ";
		$sql .= " id,filename,order_no ";
		$sql .= " from mht_images ";
		$sql .= " where item_id = ".$_POST['id'];
		$sql .= " order by order_no ";
		
		$rst = mysql_query($sql,$con);
		if($rst){
			while($col = mysql_fetch_array($rst)){
				$data .= "<imagedata>\n";
				foreach($col as $key => $value){
					if(!is_numeric($key)){
						$data .= "<".$key.">".$value."</".$key.">\n";
					}
				}
				$data .= "</imagedata>\n";
			}
		}else{
			return "error";
		}
		
		$data .= "</data>\n";
		
		return $data;

	}
?>