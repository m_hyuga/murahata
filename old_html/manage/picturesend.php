<?php
	require_once("../dbini_mht.php");
	
	/* とりあえずDB接続 */
	$con = mysql_connect($DBSERVER,$DBUSER,$DBPASSWORD);
	$selectdb = mysql_select_db($DBNAME,$con);
	// SET NAMES クエリの発行
	$sql = "SET NAMES utf8";
	$rst = mysql_query($sql,$con);

	if(empty($_GET['iid'])){
		echo "error";
	}else{
		echo getPictureData();
	}
	exit;
	
	function getPictureData(){
		$dir = "../images/";
		
		if( !is_uploaded_file( $_FILES[ 'userfile' ][ 'tmp_name' ] ) ) {
			echo "error";
		} else {
			$filename = uniqid("", TRUE).".jpg";
			$savefilename = $filename;

			move_uploaded_file( $_FILES[ 'userfile' ][ 'tmp_name' ], $dir.$filename);
			chmod($dir.$filename, 0666);

			if(preg_match('/jpg$/i',$filename) || preg_match('/jpeg$/i',$filename )){
				/* 縮小画像 */
				if($_GET['m'] == "news"){
					imagesave2($dir,$filename);
				}else{
					imagesave($dir,$filename);
				}
				echo saveDb($savefilename);
			}else{
				echo "error";
			}
		}
	}
	
	function imagesave($path,$filename) {
		$image = ImageCreateFromJPEG($path.$filename);
		$width = ImageSX($image);
		$height = ImageSY($image);
		if($width != 320 && $height != 320){
			converImage($image,$path."m_".$filename,320);
		}else {
			copyImage($image,$path."m_".$filename);
		}
		if($width != 64 && $height != 64){
			converImage($image,$path."s_".$filename,64);
		}else {
			copyImage($image,$path."s_".$filename);
		}
	}
	
	function imagesave2($path,$filename) {
		$image = ImageCreateFromJPEG($path.$filename);
		$width = ImageSX($image);
		$height = ImageSY($image);
		if($width != 476){
			converImage2($image,$path."m_".$filename,476);
		}else {
			copyImage($image,$path."m_".$filename);
		}
		if($width != 64){
			converImage2($image,$path."s_".$filename,64);
		}else {
			copyImage($image,$path."s_".$filename);
		}
	}
	
	function converImage($image,$pathfilename,$new_size){
		$width = ImageSX($image);
		$height = ImageSY($image);
		if($width >= $height){
			$rate = $new_size / $width;
			$new_width = $new_size;
			$new_height = $rate * $height;
		}else{
			$rate = $new_size / $height;
			$new_width = $rate * $width;
			$new_height = $new_size;
		}
		
		$new_image = ImageCreateTrueColor($new_size, $new_size);
		imagefill($new_image,0,0,0xFFFFFF);
		
		if($width >= $height){
			ImageCopyResampled($new_image,$image,0,($new_size - $new_height) /2,0,0,$new_width,$new_height,$width,$height);
		}else{
			ImageCopyResampled($new_image,$image,($new_size - $new_width) /2,0,0,0,$new_width,$new_height,$width,$height);
		}
		
		ImageJPEG($new_image, $pathfilename,80);
		chmod($pathfilename, 0666);
	}
	
	function converImage2($image,$pathfilename,$new_width){
		$width = ImageSX($image);
		$height = ImageSY($image);
		$rate = $new_width / $width;
		$new_height = $rate * $height;
		
		$new_image = ImageCreateTrueColor($new_width, $new_height);
		ImageCopyResampled($new_image,$image,0,0,0,0,$new_width,$new_height,$width,$height);
		
		ImageJPEG($new_image, $pathfilename,80);
		chmod($pathfilename, 0666);
	}
	
	function copyImage($image,$pathfilename){
		$new_image = ImageCreateTrueColor(ImageSX($image), ImageSY($image));
		ImageCopyResampled($new_image,$image,0,0,0,0,ImageSX($image),ImageSY($image),ImageSX($image),ImageSY($image));
		ImageJPEG($new_image, $pathfilename,80);
		chmod($pathfilename, 0666);
	}
	
	function saveDb($savefilename){
		global $con;
		
		if($_GET['iid'] != -1){
			$iid = $_GET['iid'];
		}else{
			$iid = "null";
		}
		$sql = "insert into mht_images(item_id,filename) values(".$iid.",'".$savefilename."')";
		$rst = mysql_query($sql,$con);
		if($rst){
			return mysql_insert_id($con);
		}else{
			return "error";
		}
	}


?>