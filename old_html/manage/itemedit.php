<?php
	require_once("../dbini_mht.php");
	
	/* とりあえずDB接続 */
	$con = mysql_connect($DBSERVER,$DBUSER,$DBPASSWORD);
	$selectdb = mysql_select_db($DBNAME,$con);
	// SET NAMES クエリの発行
	$sql = "SET NAMES utf8";
	$rst = mysql_query($sql,$con);
	
	if($_POST['mode'] == "itemdetail"){
		//編集情報
		echo getItemData();
	}else if($_POST['mode'] == "search"){
		//一覧表示
		echo getItemList();
	}else if($_POST['mode'] == "save"){
		//保存
		echo saveData();
	}else if($_POST['mode'] == "imagelist"){
		//画像リスト
		echo getImageList();
	}else if($_POST['mode'] == "deleteimage"){
		//画像削除
		echo deleteImage();
	}	
	
	exit;
	
	/* 編集情報 */
	function getItemData(){
		global $con;
		
		if($_POST['key'] != "mht"){
			return "error";
		}

		$data = "<data>\n";
		
		$sql = "select ";
		$sql .= " id,title,body,order_no,alive ";
		$sql .= " from mht_item ";
		$sql .= " where id = ".$_POST['id'];

		$rst = mysql_query($sql,$con);
		if($rst){
			$col = mysql_fetch_array($rst);
			$data .= "<itemdata>\n";
			foreach($col as $key => $value){
				if(!is_numeric($key)){
					$data .= "<".$key.">".$value."</".$key.">\n";
				}
			}
			$data .= "</itemdata>\n";
		}else{
			return "error";
		}
		
		$sql = "select ";
		$sql .= " id,filename,order_no ";
		$sql .= " from mht_images ";
		$sql .= " where item_id = ".$_POST['id'];
		$rst = mysql_query($sql,$con);
		if($rst){
			while($col = mysql_fetch_array($rst)){
				$data .= "<imagedata>\n";
				foreach($col as $key => $value){
					if(!is_numeric($key)){
						$data .= "<".$key.">".$value."</".$key.">\n";
					}
				}
				$data .= "</imagedata>\n";
			}
		}else{
			return "error";
		}
		
		$data .= "</data>\n";
		
		return $data;

	}
	
	
	
	/* アイテム一覧 */
	function getItemList(){
		global $con;
		
		if($_POST['key'] != "mht"){
			return "error";
		}
		
		$sql = "select ";
		$sql .= " count(mht_item.id) as cnt ";
		$sql .= " from mht_item ";
		$sql .= " left join mht_category ";
		$sql .= " on mht_item.category_id = mht_category.id ";
		$sql .= " where mht_category.type = 'bridal'";
		$sql .= " or mht_category.type = 'cake'";
		$sql .= " or mht_category.type = 'gift'";
		$rst = mysql_query($sql,$con);
		if($rst){
			$data = "<data>\n";
			$col = mysql_fetch_array($rst);
			$data .= "<count>\n";
			$data .= $col['cnt'];
			$data .= "</count>\n";
		}else{
			return "error";
		}
		
		$sql = "select ";
		$sql .= " mht_item.id as id, ";
		$sql .= " mht_item.title as title, ";
		$sql .= " mht_item.body as body, ";
		$sql .= " coalesce(aaa.filename,'') as filename, ";
		$sql .= " mht_category.name as category_name ";
		$sql .= " from (mht_item ";
		$sql .= " left join mht_category ";
		$sql .= " on mht_category.id = mht_item.category_id) ";
		$sql .= " left join (select item_id,filename from mht_images where order_no = 0) as aaa ";
		$sql .= " on mht_item.id = aaa.item_id ";
		$sql .= " where mht_category.type = 'bridal'";
		$sql .= " or mht_category.type = 'cake'";
		$sql .= " or mht_category.type = 'gift'";
		$rst = mysql_query($sql,$con);
		if($rst){
			while($col = mysql_fetch_array($rst)){
				$data .= "<itemdata>\n";
				foreach($col as $key => $value){
					if(!is_numeric($key)){
						$data .= "<".$key.">".$value."</".$key.">\n";
					}
				}
				$data .= "</itemdata>\n";
			}
			$data .= "</data>\n";
			return $data;
		}else{
			return "error";
		}
	}

	//保存
	function saveData(){
		global $con;
		
		if($_POST['key'] != "mht"){
			return "error";
		}
		
		//item保存
		$id = $_POST['id'];
		
		$sql = "update mht_item set ";
		$sql .= " title = '".str_replace("\'","'",str_replace('\"','"',$_POST['title']))."',";
		$sql .= " body = '".str_replace("\'","'",str_replace('\"','"',$_POST['body']))."',";
		$sql .= " alive = 1 ";
		$sql .= " where id = ".$_POST['id'];
		
		$rst = mysql_query($sql,$con);
		if(!$rst){
			return "error";
		}
		
		//画像
		$sql = "update mht_images set ";
		$sql .= " order_no = -1 ";
		$sql .= " where item_id = ".$id;
		$rst = mysql_query($sql,$con);
		
		$image_id_array = explode(",",$_POST['image']);
		for($i = 0;$i<count($image_id_array);$i++){
			if($image_id_array[$i] != -1){
				$sql = "update mht_images set ";
				$sql .= " order_no = ".$i;
				$sql .= " where id = ".$image_id_array[$i];
				$rst = mysql_query($sql,$con);
			}
		}
		
		return $id;
	}

	/* 画像リスト */
	function getImageList(){
		global $con;
		
		if($_POST['key'] != "mht"){
			return "error";
		}
		$sql = "select  ";
		$sql .= " count(id) as cnt ";
		$sql .= " from mht_images ";
		if($_POST['item_id'] != -1){
			$sql .= " where item_id = ".$_POST['item_id'];
		}else{
			$sql .= " where id = 0 ".$_POST['addsql'];
		}
		$rst = mysql_query($sql,$con);
		if($rst){
			$col = mysql_fetch_array($rst);
			$data = "<data>\n";
			$data .= "<count>".$col['cnt']."</count>\n";
			$data .= "<sql>".$sql."</sql>\n";
			
		}else{
			return "error";
		}
		
		$sql = "select ";
		$sql .= " id,filename ";
		$sql .= " from mht_images ";
		if($_POST['item_id'] != -1){
			$sql .= " where item_id = ".$_POST['item_id'];
		}else{
			$sql .= " where id = 0 ".$_POST['addsql'];
		}
		$sql .= " order by id desc";
		$sql .= " limit ".($_POST['page'] * 12).",12";
		$rst = mysql_query($sql,$con);
		if($rst){
			while($col = mysql_fetch_array($rst)){
				$data .= "<imagedata>\n";
				foreach($col as $key => $value){
					if(!is_numeric($key)){
						$data .= "<".$key.">".$value."</".$key.">\n";
					}
				}
				$data .= "</imagedata>\n";
			}
			$data .= "</data>\n";
			return $data;
		}else{
			return "error";
		}
	}
	
	/* 画像削除 */
	function deleteImage(){
		global $con;
		
		if($_POST['key'] != "mht"){
			return "error";
		}
		
		if($_POST['item_id'] != -1){
			$sql = "select count(mht_images.id) as cnt ";
			$sql .= " from mht_images ";
			$sql .= " where id = ".$_POST['image_id'];
			$sql .= " and order_no <> -1 ";
			$rst = mysql_query($sql,$con);
			if($rst){
				$col = mysql_fetch_array($rst);
				if($col['cnt'] > 0){
					return $col['cnt'];
				}
			}else{
				return "error";
			}
		}
		
		$sql = "delete from mht_images ";
		$sql .= " where id = ".$_POST['image_id'];
		$rst = mysql_query($sql,$con);
		if($rst){
			return "ok";
		}else{
			return "error";
		}

	}
	

?>