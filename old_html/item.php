<?php
	require_once("./dbini_mht.php");
	
	/* とりあえずDB接続 */
	$con = mysql_connect($DBSERVER,$DBUSER,$DBPASSWORD);
	$selectdb = mysql_select_db($DBNAME,$con);
	// SET NAMES クエリの発行
	$sql = "SET NAMES utf8";
	$rst = mysql_query($sql,$con);
	
	if($_POST['mode'] == "parfait"){
		//厳選パフェ
		echo getCakeData("parfait");
	}else if($_POST['mode'] == "cake"){
		//フルーツケーキ
		echo getCakeData("cake");
	}else if($_POST['mode'] == "gift"){
		//ギフト
		echo getCakeData("gift");
	}else if($_POST['mode'] == "bridal"){
		//ブライダル
		echo getCakeData("bridal");
	}else if($_POST['mode'] == "dictionary"){
		//辞典
		echo getCakeData("dic");
	}else if($_POST['mode'] == "shipment"){
		//辞典
		echo getShipmentData();
	}
	
	exit;
	
	/* フルーツケーキ */
	function getCakeData($type){
		global $con;
		
		if($_POST['key'] != "mht"){
			return "error";
		}
		
		$sql = "select ";
		$sql .= " mht_item.id as id, ";
		$sql .= " mht_item.title as title, ";
		$sql .= " mht_item.body as body, ";
		$sql .= " mht_item.order_no as item_order_no, ";
		$sql .= " date_format(mht_item.date,'%Y') as year, ";
		$sql .= " date_format(mht_item.date,'%c') as month, ";
		$sql .= " coalesce(aaa.filename,'') as filename, ";
		$sql .= " coalesce(aaa.order_no,'') as order_no, ";
		$sql .= " mht_category.id as category_no, ";
		$sql .= " mht_category.name as category_name ";
		$sql .= " from (mht_item ";
		$sql .= " left join mht_category ";
		$sql .= " on mht_category.id = mht_item.category_id) ";
		$sql .= " left join (select item_id,filename,order_no from mht_images where order_no <> -1) as aaa ";
		$sql .= " on mht_item.id = aaa.item_id ";
		$sql .= " where mht_category.type = '".$type."'";
		$sql .= " and mht_item.alive = 1 ";
		$sql .= " order by date desc,aaa.order_no ";
		$rst = mysql_query($sql,$con);
		if($rst){
			$data = "<data>\n";
			while($col = mysql_fetch_array($rst)){
				$data .= "<itemdata>\n";
				foreach($col as $key => $value){
					if(!is_numeric($key)){
						$data .= "<".$key.">".$value."</".$key.">\n";
					}
				}
				$data .= "</itemdata>\n";
			}
			$data .= "</data>\n";
			return $data;
		}else{
			return "error";
		}
	}
	
	
	function getShipmentData(){
		global $con;
		
		if($_POST['key'] != "mht"){
			return "error";
		}

		$data = "<data>\n";
		
		$sql = "select ";
		$sql .= " shipment,alive ";
		$sql .= " from mht_shipment ";
		$sql .= " where item_id = ".$_POST['id'];
		$rst = mysql_query($sql,$con);
		if($rst){
			while($col = mysql_fetch_array($rst)){
				$data .= "<shipmentdata>\n";
				foreach($col as $key => $value){
					if(!is_numeric($key)){
						$data .= "<".$key.">".$value."</".$key.">\n";
					}
				}
				$data .= "</shipmentdata>\n";
			}
		}else{
			return "error";
		}
		
		$data .= "</data>\n";
		
		return $data;

	}
?>