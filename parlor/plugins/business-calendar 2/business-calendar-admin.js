//
// 休業日の変更
//
function check_holiday(obj) {
	var val = jQuery("input:hidden", obj).val();
	if(val == "") {
		val = "all";
	} else if(val == "all") {
		jQuery(obj).removeClass(val);
		val = "am";
	} else if(val == "am") {
		jQuery(obj).removeClass(val);
		val = "pm";
	} else {
		jQuery(obj).removeClass(val);
		val = "";
	}

	if(val.length > 0) jQuery(obj).addClass(val);

	jQuery("input:hidden", obj).val(val);
}
