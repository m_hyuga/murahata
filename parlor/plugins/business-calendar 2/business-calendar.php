<?php
/*
Plugin Name: Business Calendar
Plugin URI: http://www.is-p.cc/wordpress/plug-in/business-calendar/700
Description: 営業日を表示するウィジェット
Author: IS Planning
Version: 3.20
Author URI: http://www.is-p.cc/
*/

class ISP_Widget_BusinessCalendar extends WP_Widget {

	//
	// コンストラクタ
	//
	function ISP_Widget_BusinessCalendar() {
		$widget_ops = array('description' => '営業日カレンダーの表示');
		parent::WP_Widget(false, $name = '営業日カレンダー', $widget_ops);
	}

	//
	// 出力
	//
	function widget( $args, $instance ) {
		extract($args);

		// 定休日を取得
		$h_day1 = $instance['sun1'].",".$instance['mon1'].",".$instance['tue1'].",".$instance['wed1'].",".$instance['thu1'].",".$instance['fri1'].",".$instance['sat1'];
		$h_day2 = $instance['sun2'].",".$instance['mon2'].",".$instance['tue2'].",".$instance['wed2'].",".$instance['thu2'].",".$instance['fri2'].",".$instance['sat2'];
		$h_day3 = $instance['sun3'].",".$instance['mon3'].",".$instance['tue3'].",".$instance['wed3'].",".$instance['thu3'].",".$instance['fri3'].",".$instance['sat3'];
		$h_day4 = $instance['sun4'].",".$instance['mon4'].",".$instance['tue4'].",".$instance['wed4'].",".$instance['thu4'].",".$instance['fri4'].",".$instance['sat4'];
		$h_day5 = $instance['sun5'].",".$instance['mon5'].",".$instance['tue5'].",".$instance['wed5'].",".$instance['thu5'].",".$instance['fri5'].",".$instance['sat5'];

		$holiday = "1=$h_day1&amp;2=$h_day2&amp;3=$h_day3&amp;4=$h_day4&amp;5=$h_day5";

		// 臨時休業日を取得
		$include_holiday = "";
		$include = explode("\n", $instance['include']);
		foreach($include as $val) {
			if(strlen($val) > 0) {
				$tm = strtotime($val);
				$inc[date('Y-n', $tm)] .= ',' . date('j', $tm);
			}
		}
		if(!empty($inc)) {
			foreach($inc as $key => $val) {
				$include_holiday .= '&amp;' . $key . '=' . mb_substr($val, 1);
			}
			$include_holiday = mb_substr($include_holiday, 5);
		}

		// 臨時営業日を取得
		$exclude_holiday = "";
		$exclude = explode("\n", $instance['exclude']);
		foreach($exclude as $val) {
			if(strlen($val) > 0) {
				$tm = strtotime($val);
				$exc[date('Y-n', $tm)] .= ',' . date('j', $tm);
			}
		}
		if(!empty($exc)) {
			foreach($exc as $key => $val) {
				$exclude_holiday .= '&amp;' . $key . '=' . mb_substr($val, 1);
			}
			$exclude_holiday = mb_substr($exclude_holiday, 5);
		}

		// タイトルの表示
		$title = apply_filters('widget_title', $instance['title']);
		echo $before_widget;
		if($title) echo $before_title . $title . $after_title;

		// カレンダーのフレーム表示
?>

<div class="isp-calendar-frame">
  <input type="hidden" class="base-holiday" value="<?php echo $holiday; ?>" />
  <input type="hidden" class="inc-holiday" value="<?php echo $include_holiday; ?>" />
  <input type="hidden" class="exc-holiday" value="<?php echo $exclude_holiday; ?>" />
  <input type="hidden" class="month_future" value="<?php echo $instance['future']; ?>" />
  <input type="hidden" class="month_past" value="<?php echo $instance['past']; ?>" />
  <input type="hidden" class="s_day" value="<?php echo $instance['s_day']; ?>" />
  <div class="isp-calendar-table"></div>
</div>
<?php
		echo $after_widget;

	}

	//
	// ウィジェット管理画面
	//
	function form($instance) {
		$instance = wp_parse_args( (array)$instance, array('title' => '',
																											'mon1' => '', 'tue1' => '', 'wed1' => '', 'thu1' => '', 'fri1' => '', 'sat1' => '', 'sun1' => '',
																											'mon2' => '', 'tue2' => '', 'wed2' => '', 'thu2' => '', 'fri2' => '', 'sat2' => '', 'sun2' => '',
																											'mon3' => '', 'tue3' => '', 'wed3' => '', 'thu3' => '', 'fri3' => '', 'sat3' => '', 'sun3' => '',
																											'mon4' => '', 'tue4' => '', 'wed4' => '', 'thu4' => '', 'fri4' => '', 'sat4' => '', 'sun4' => '',
																											'mon5' => '', 'tue5' => '', 'wed5' => '', 'thu5' => '', 'fri5' => '', 'sat5' => '', 'sun5' => '',
																											'include' => '', 'exclude' => '', 'future' => '0', 'past' => '0', 'mainte' => '0', 's_day' => '0') );
		$title = esc_attr($instance['title']);
		$include = esc_attr($instance['include']);
		$exclude = esc_attr($instance['exclude']);
		$future = esc_attr($instance['future']);
		$past = esc_attr($instance['past']);
		$mainte = esc_attr($instance['mainte']);
?>

<p>
<label for="<?php echo $this->get_field_id('title'); ?>">タイトル:<br />
<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
</label>
</p>

<p style="margin-bottom: 0px; padding-bottom: 0px;">定休日設定:<br />
<sapn style="font-size: 0.8em;">※ クリックで変更　all=全休　am=午前　pm=午後</span>
</p>
<table id="weekly_set" class="widefat" cellspacing="0" style="margin-bottom: 1em;" cellpadding="0">
<thead><tr><th>第</th><th>月</th><th>火</th><th>水</th><th>木</th><th>金</th><th>土</th><th>日</th></tr></thead>
<tbody>
<tr>
<td>1</td>
<td onclick="check_holiday(this);" class="<?php echo $instance['mon1']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('mon1'); ?>" name="<?php echo $this->get_field_name('mon1'); ?>" value="<?php echo $instance['mon1']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['tue1']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('tue1'); ?>" name="<?php echo $this->get_field_name('tue1'); ?>" value="<?php echo $instance['tue1']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['wed1']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('wed1'); ?>" name="<?php echo $this->get_field_name('wed1'); ?>" value="<?php echo $instance['wed1']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['thu1']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('thu1'); ?>" name="<?php echo $this->get_field_name('thu1'); ?>" value="<?php echo $instance['thu1']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['fri1']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('fri1'); ?>" name="<?php echo $this->get_field_name('fri1'); ?>" value="<?php echo $instance['fri1']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['sat1']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('sat1'); ?>" name="<?php echo $this->get_field_name('sat1'); ?>" value="<?php echo $instance['sat1']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['sun1']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('sun1'); ?>" name="<?php echo $this->get_field_name('sun1'); ?>" value="<?php echo $instance['sun1']; ?>" /></td>
</tr>
<tr>
<td>2</td>
<td onclick="check_holiday(this);" class="<?php echo $instance['mon2']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('mon2'); ?>" name="<?php echo $this->get_field_name('mon2'); ?>" value="<?php echo $instance['mon2']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['tue2']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('tue2'); ?>" name="<?php echo $this->get_field_name('tue2'); ?>" value="<?php echo $instance['tue2']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['wed2']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('wed2'); ?>" name="<?php echo $this->get_field_name('wed2'); ?>" value="<?php echo $instance['wed2']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['thu2']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('thu2'); ?>" name="<?php echo $this->get_field_name('thu2'); ?>" value="<?php echo $instance['thu2']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['fri2']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('fri2'); ?>" name="<?php echo $this->get_field_name('fri2'); ?>" value="<?php echo $instance['fri2']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['sat2']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('sat2'); ?>" name="<?php echo $this->get_field_name('sat2'); ?>" value="<?php echo $instance['sat2']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['sun2']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('sun2'); ?>" name="<?php echo $this->get_field_name('sun2'); ?>" value="<?php echo $instance['sun2']; ?>" /></td>
</tr>
<tr>
<td>3</td>
<td onclick="check_holiday(this);" class="<?php echo $instance['mon3']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('mon3'); ?>" name="<?php echo $this->get_field_name('mon3'); ?>" value="<?php echo $instance['mon3']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['tue3']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('tue3'); ?>" name="<?php echo $this->get_field_name('tue3'); ?>" value="<?php echo $instance['tue3']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['wed3']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('wed3'); ?>" name="<?php echo $this->get_field_name('wed3'); ?>" value="<?php echo $instance['wed3']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['thu3']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('thu3'); ?>" name="<?php echo $this->get_field_name('thu3'); ?>" value="<?php echo $instance['thu3']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['fri3']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('fri3'); ?>" name="<?php echo $this->get_field_name('fri3'); ?>" value="<?php echo $instance['fri3']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['sat3']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('sat3'); ?>" name="<?php echo $this->get_field_name('sat3'); ?>" value="<?php echo $instance['sat3']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['sun3']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('sun3'); ?>" name="<?php echo $this->get_field_name('sun3'); ?>" value="<?php echo $instance['sun3']; ?>" /></td>
</tr>
<tr>
<td>4</td>
<td onclick="check_holiday(this);" class="<?php echo $instance['mon4']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('mon4'); ?>" name="<?php echo $this->get_field_name('mon4'); ?>" value="<?php echo $instance['mon4']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['tue4']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('tue4'); ?>" name="<?php echo $this->get_field_name('tue4'); ?>" value="<?php echo $instance['tue4']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['wed4']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('wed4'); ?>" name="<?php echo $this->get_field_name('wed4'); ?>" value="<?php echo $instance['wed4']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['thu4']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('thu4'); ?>" name="<?php echo $this->get_field_name('thu4'); ?>" value="<?php echo $instance['thu4']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['fri4']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('fri4'); ?>" name="<?php echo $this->get_field_name('fri4'); ?>" value="<?php echo $instance['fri4']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['sat4']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('sat4'); ?>" name="<?php echo $this->get_field_name('sat4'); ?>" value="<?php echo $instance['sat4']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['sun4']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('sun4'); ?>" name="<?php echo $this->get_field_name('sun4'); ?>" value="<?php echo $instance['sun4']; ?>" /></td>
</tr>
<tr>
<td>5</td>
<td onclick="check_holiday(this);" class="<?php echo $instance['mon5']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('mon5'); ?>" name="<?php echo $this->get_field_name('mon5'); ?>" value="<?php echo $instance['mon5']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['tue5']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('tue5'); ?>" name="<?php echo $this->get_field_name('tue5'); ?>" value="<?php echo $instance['tue5']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['wed5']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('wed5'); ?>" name="<?php echo $this->get_field_name('wed5'); ?>" value="<?php echo $instance['wed5']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['thu5']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('thu5'); ?>" name="<?php echo $this->get_field_name('thu5'); ?>" value="<?php echo $instance['thu5']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['fri5']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('fri5'); ?>" name="<?php echo $this->get_field_name('fri5'); ?>" value="<?php echo $instance['fri5']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['sat5']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('sat5'); ?>" name="<?php echo $this->get_field_name('sat5'); ?>" value="<?php echo $instance['sat5']; ?>" /></td>
<td onclick="check_holiday(this);" class="<?php echo $instance['sun5']; ?>"><input type="hidden" id="<?php echo $this->get_field_id('sun5'); ?>" name="<?php echo $this->get_field_name('sun5'); ?>" value="<?php echo $instance['sun5']; ?>" /></td>
</tr>
</tbody>
</table>

<p>
臨時休業日（祝日等）:<br />
<textarea class="widefat" rows="5" id="<?php echo $this->get_field_id('include'); ?>" name="<?php echo $this->get_field_name('include'); ?>">
<?php echo $include; ?>
</textarea>
</p>

<p>
臨時営業日:<br />
<textarea class="widefat" rows="5" id="<?php echo $this->get_field_id('exclude'); ?>" name="<?php echo $this->get_field_name('exclude'); ?>">
<?php echo $exclude; ?>
</textarea>
</p>

<p>
週始め:<br />
<input type="radio" id="<?php echo $this->get_field_id('s_day'); ?>1" name="<?php echo $this->get_field_name('s_day'); ?>" value="1"<?php if($instance['s_day'] == '1') echo ' checked="checked"'; ?>>月</input>
<input type="radio" id="<?php echo $this->get_field_id('s_day'); ?>2" name="<?php echo $this->get_field_name('s_day'); ?>" value="2"<?php if($instance['s_day'] == '2') echo ' checked="checked"'; ?>>火</input>
<input type="radio" id="<?php echo $this->get_field_id('s_day'); ?>3" name="<?php echo $this->get_field_name('s_day'); ?>" value="3"<?php if($instance['s_day'] == '3') echo ' checked="checked"'; ?>>水</input>
<input type="radio" id="<?php echo $this->get_field_id('s_day'); ?>4" name="<?php echo $this->get_field_name('s_day'); ?>" value="4"<?php if($instance['s_day'] == '4') echo ' checked="checked"'; ?>>木</input>
<input type="radio" id="<?php echo $this->get_field_id('s_day'); ?>5" name="<?php echo $this->get_field_name('s_day'); ?>" value="5"<?php if($instance['s_day'] == '5') echo ' checked="checked"'; ?>>金</input>
<input type="radio" id="<?php echo $this->get_field_id('s_day'); ?>6" name="<?php echo $this->get_field_name('s_day'); ?>" value="6"<?php if($instance['s_day'] == '6') echo ' checked="checked"'; ?>>土</input>
<input type="radio" id="<?php echo $this->get_field_id('s_day'); ?>0" name="<?php echo $this->get_field_name('s_day'); ?>" value="0"<?php if($instance['s_day'] == '0') echo ' checked="checked"'; ?>>日</input>
</p>

<p>
月送り設定:<br />
<select id="<?php echo $this->get_field_id('future'); ?>" name="<?php echo $this->get_field_name('future'); ?>">
<option value="0"<?php if($future == '0') echo ' selected="selected"'; ?>>--</option>
<option value="1"<?php if($future == '1') echo ' selected="selected"'; ?>>1</option>
<option value="2"<?php if($future == '2') echo ' selected="selected"'; ?>>2</option>
<option value="3"<?php if($future == '3') echo ' selected="selected"'; ?>>3</option>
<option value="4"<?php if($future == '4') echo ' selected="selected"'; ?>>4</option>
<option value="5"<?php if($future == '5') echo ' selected="selected"'; ?>>5</option>
<option value="6"<?php if($future == '6') echo ' selected="selected"'; ?>>6</option>
<option value="7"<?php if($future == '7') echo ' selected="selected"'; ?>>7</option>
<option value="8"<?php if($future == '8') echo ' selected="selected"'; ?>>8</option>
<option value="9"<?php if($future == '9') echo ' selected="selected"'; ?>>9</option>
<option value="10"<?php if($future == '10') echo ' selected="selected"'; ?>>10</option>
<option value="11"<?php if($future == '11') echo ' selected="selected"'; ?>>11</option>
<option value="12"<?php if($future == '12') echo ' selected="selected"'; ?>>12</option>
</select> ヶ月先まで月送り可能とする<br />
<select id="<?php echo $this->get_field_id('past'); ?>" name="<?php echo $this->get_field_name('past'); ?>">
<option value="0"<?php if($past == '0') echo ' selected="selected"'; ?>>--</option>
<option value="1"<?php if($past == '1') echo ' selected="selected"'; ?>>1</option>
<option value="2"<?php if($past == '2') echo ' selected="selected"'; ?>>2</option>
<option value="3"<?php if($past == '3') echo ' selected="selected"'; ?>>3</option>
<option value="4"<?php if($past == '4') echo ' selected="selected"'; ?>>4</option>
<option value="5"<?php if($past == '5') echo ' selected="selected"'; ?>>5</option>
<option value="6"<?php if($past == '6') echo ' selected="selected"'; ?>>6</option>
<option value="7"<?php if($past == '7') echo ' selected="selected"'; ?>>7</option>
<option value="8"<?php if($past == '8') echo ' selected="selected"'; ?>>8</option>
<option value="9"<?php if($past == '9') echo ' selected="selected"'; ?>>9</option>
<option value="10"<?php if($past == '10') echo ' selected="selected"'; ?>>10</option>
<option value="11"<?php if($past == '11') echo ' selected="selected"'; ?>>11</option>
<option value="12"<?php if($past == '12') echo ' selected="selected"'; ?>>12</option>
</select> ヶ月前まで月送り可能とする<br />
<sapn style="font-size: 0.8em;">※ -- は月送りをさせない</span>
</p>

<p>
メンテナンス:<br />
保存時に
<select id="<?php echo $this->get_field_id('mainte'); ?>" name="<?php echo $this->get_field_name('mainte'); ?>">
<option value="0"<?php if($mainte == '0') echo ' selected="selected"'; ?>>--</option>
<option value="1"<?php if($mainte == '1') echo ' selected="selected"'; ?>>1</option>
<option value="2"<?php if($mainte == '2') echo ' selected="selected"'; ?>>2</option>
<option value="3"<?php if($mainte == '3') echo ' selected="selected"'; ?>>3</option>
<option value="4"<?php if($mainte == '4') echo ' selected="selected"'; ?>>4</option>
<option value="5"<?php if($mainte == '5') echo ' selected="selected"'; ?>>5</option>
<option value="6"<?php if($mainte == '6') echo ' selected="selected"'; ?>>6</option>
<option value="7"<?php if($mainte == '7') echo ' selected="selected"'; ?>>7</option>
<option value="8"<?php if($mainte == '8') echo ' selected="selected"'; ?>>8</option>
<option value="9"<?php if($mainte == '9') echo ' selected="selected"'; ?>>9</option>
<option value="10"<?php if($mainte == '10') echo ' selected="selected"'; ?>>10</option>
<option value="11"<?php if($mainte == '11') echo ' selected="selected"'; ?>>11</option>
<option value="12"<?php if($mainte == '12') echo ' selected="selected"'; ?>>12</option>
</select> ヶ月以上前のデータを自動で削除する<br />
<sapn style="font-size: 0.8em;">※ -- は削除しない</span>
</p>

<?php
	}

	// アップデート
	function update($new_instance, $old_instance) {
		if(intval($new_instance['mainte']) > 0) {
			// データの自動削除が設定されている場合
			$del_date = mktime("0", "0", "0", date("m") - (intval($new_instance['mainte'])-1), 1 ,date("Y"));

			$inc = '';
			$include = explode("\n", $new_instance['include']);
			foreach($include as $val) {
				if(strlen($val) > 0) {
					$tm = strtotime($val);
					if(($tm - $del_date) > 0) {
						$inc .= $val . "\n";
					}
				}
			}
			$new_instance['include'] = $inc;

			$exc = '';
			$exclude = explode("\n", $new_instance['exclude']);
			foreach($exclude as $val) {
				if(strlen($val) > 0) {
					$tm = strtotime($val);
					if(($tm - $del_date) > 0) {
						$exc .= $val . "\n";
					}
				}
			}
			$new_instance['exclude'] = $exc;
		}

		return $new_instance;
	}

} // class ISP_Widget_BusinessCalendar


add_action('widgets_init', create_function('', 'return register_widget("ISP_Widget_BusinessCalendar");'));
add_action('wp_print_scripts', 'Add_Calendar_JS');
add_action('wp_head', 'Add_Calendar_CSS');
add_action('admin_head', 'Add_Calendar_CSS');

//
// JSファイルの登録
//
function Add_Calendar_JS() {
	$plugin_url = WP_PLUGIN_URL."/business-calendar";

	wp_enqueue_script('jquery');
	wp_enqueue_script('wp_business_calendar', $plugin_url . '/business-calendar.js');
	if(is_admin()) {
		wp_enqueue_script('wp_business_calendar_admin', $plugin_url . '/business-calendar-admin.js');
	}
}

//
// CSSファイルの登録
//
function Add_Calendar_CSS() {
	$plugin_url = WP_PLUGIN_URL."/business-calendar";

	$add_html = '<link type="text/css" href="' . $plugin_url . '/business-calendar.css" rel="stylesheet" />';

	print $add_html;
}

?>
