<?php
/**
 * The main template file
 *
 */

get_header(); ?>

	<div id="content">
		<h2><img src="<?php bloginfo('template_url'); ?>/common/images/top/main_img.jpg" alt="mainimg" width="1000" height="460"></h2>
		<div id="news"><?php query_posts( Array('showposts' => 1));
										if (have_posts()) : while (have_posts()) : the_post(); ?>
											<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
										<?php endwhile; ?>
										<?php else : ?>
											新着のお知らせはありません。
										<?php endif; ?></div>
		<div class="cf">
			<div class="menu">
				<div class="ttl_menu cf">
					<h3><img src="<?php bloginfo('template_url'); ?>/common/images/top/ttl_menu.jpg" alt="MENU" width="86" height="25"></h3>
					<p><a href="<?php bloginfo('url'); ?>/menu/">メニューを見る</a></p>
				</div>
				<ul class="top_menu_ttl cf">
					<li>
						<h4 class="menu_cate mg_l5">今月の厳選パフェ</h4>
						<?php query_posts( Array(
							'post_type' => array('menu'),
							'showposts' => 1,
							'menu_cat' => 'special'
							));
						if (have_posts()) : while (have_posts()) : the_post(); ?>
						<a href="<?php the_permalink(); ?>"><p class="bg_img" style="background-image: url('<?php	$image_id = SCF::get('img-main');$image = wp_get_attachment_image_src($image_id, 'full');echo $image[0]; ?>')"></p>
						<p><?php the_title(); ?><br>
								<?php $val =  scf::get('txt-price');
								if (empty($val)) {
									echo '';
								} else {
									echo $val.'円';
								} ?></p></a>
						<?php endwhile; endif; ?>
					</li>
					<li>
						<h4 class="menu_cate">HAPPYWEEKパフェ</h4>
						<?php query_posts( Array(
							'post_type' => array('menu'),
							'showposts' => 1,
							'menu_cat' => 'happyweek'
							));
						if (have_posts()) : while (have_posts()) : the_post(); ?>
						<a href="<?php the_permalink(); ?>"><p class="bg_img" style="background-image: url('<?php	$image_id = SCF::get('img-main');$image = wp_get_attachment_image_src($image_id, 'full');echo $image[0]; ?>')"></p>
						<p><?php the_title(); ?><br>
								<?php $val =  scf::get('txt-price');
								if (empty($val)) {
									echo '';
								} else {
									echo $val.'円';
								} ?></p></a>
						<?php endwhile; endif; ?>
					</li>
				</ul>
				<h4 class="menu_cate">今月のおすすめメニュー</h4>
				<ul class="cf">
						<?php query_posts( Array(
							'post_type' => array('menu'),
							'showposts' => 3,
							'menu_cat' => 'osusume'
							));
						if (have_posts()) : while (have_posts()) : the_post(); ?>
						<li>
						<a href="<?php the_permalink(); ?>"><p class="bg_img" style="background-image: url('<?php	$image_id = SCF::get('img-main');$image = wp_get_attachment_image_src($image_id, 'full');echo $image[0]; ?>')"></p>
						<p><?php the_title(); ?><br>
								<?php $val =  scf::get('txt-price');
								if (empty($val)) {
									echo '';
								} else {
									echo $val.'円';
								} ?></p></a>
						</li>
						<?php endwhile; endif; ?>
				</ul>
			</div><!-- /menu -->
			<?php get_sidebar(); ?>
		</div>
	</div>

<?php get_footer(); ?>
