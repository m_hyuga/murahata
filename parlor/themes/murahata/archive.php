<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
	<div id="content" class="menu">
		<h2><img src="<?php bloginfo('template_url'); ?>/common/images/news/ttl_h2_01.jpg" alt="NEWS" width="1001" height="134"></h2>
		<div class="cf">
			<div id="menu-list" class="fll">
				<div class="ttl_menu">
					<ul id="name-list" class="tbl">
						<li><span class="arrow"></span><a href="<?php bloginfo('url'); ?>/lists/news/">新着情報一覧</a></li>
					</ul>
				</div>
				<ul class="news_list">
				<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
					<li><p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
					<span><?php the_time('Y/m/d'); ?></span>
					</li>
				<?php endwhile; endif; ?>
				</ul>
				
			<?php  if (function_exists('wp_pagenavi')) { wp_pagenavi(); } wp_reset_query(); ?>

			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>

<?php
get_footer();
