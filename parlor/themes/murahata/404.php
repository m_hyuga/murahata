<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
	<div id="content" class="menu">
		<div class="cf">
			<div id="menu-list" class="fll">
				<br><br>
				<p>ページが見つかりませんでした。</p>
				<br><br>
				<p><a href="<?php bloginfo('url'); ?>">トップページへ戻る</a></p>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>

<?php
get_footer();
