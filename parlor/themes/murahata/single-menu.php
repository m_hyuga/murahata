<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
	<div id="content" class="menu">
		<h2><img src="<?php bloginfo('template_url'); ?>/common/images/menu/ttl_h2_01.jpg" alt="MENU" width="1001" height="134"></h2>
		<div class="cf">
			<div id="menu-list" class="fll">
				<div class="ttl_menu">
					<ul id="name-list" class="tbl">
						<li><span class="arrow"></span><a href="<?php bloginfo('url'); ?>/menu/">今月の厳選・おすすめ・HAPPY WEEKパフェ</a></li>
						<li><span class="arrow"></span><a href="<?php bloginfo('url'); ?>/parfait/">パフェ</a></li>
						<li><span class="arrow"></span><a href="<?php bloginfo('url'); ?>/sweets/">スイーツ</a></li>
						<li><span class="arrow"></span><a href="<?php bloginfo('url'); ?>/food/">軽食</a></li>
						<li><span class="arrow"></span><a href="<?php bloginfo('url'); ?>/drink/">ドリンク</a></li>
					</ul>
				</div>
				<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
				<div class="menu_datail cf">
				<div class="menu_l">
				<img src="<?php	$image_id = SCF::get('img-main');$image = wp_get_attachment_image_src($image_id, 'full');echo $image[0]; ?>">				
				</div>
				<div class="menu_r">
				<h4 class="single_title"><?php the_title(); ?><?php $val =  scf::get('txt-price');
								if (empty($val)) {
									echo '';
								} else {
									echo '<span>'.$val.'円</span>';
								} ?></h4>
					<div class="mceContentBody">
					<?php the_content(); ?>
					</div>
				</div>
				</div>
				<?php endwhile; endif; ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>

<?php
get_footer();
