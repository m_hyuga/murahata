<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
	<div id="content" class="menu">
		<h2><img src="<?php bloginfo('template_url'); ?>/common/images/menu/ttl_h2_01.jpg" alt="MENU" width="1001" height="134"></h2>
		<div class="cf">
			<div id="menu-list" class="fll">
				<div class="ttl_menu">
					<ul id="name-list" class="tbl">
						<li><span class="arrow"></span><a href="<?php bloginfo('url'); ?>/menu/">今月の厳選・おすすめ・HAPPY WEEKパフェ</a></li>
						<li><span class="arrow"></span><a href="<?php bloginfo('url'); ?>/parfait/">パフェ</a></li>
						<li><span class="arrow"></span><a href="<?php bloginfo('url'); ?>/sweets/">スイーツ</a></li>
						<li><span class="arrow"></span><a href="<?php bloginfo('url'); ?>/food/">軽食</a></li>
						<li><span class="arrow"></span><a href="<?php bloginfo('url'); ?>/drink/">ドリンク</a></li>
					</ul>
				</div>
				<div class="m_list">
						<h4 class="menu_cate mg_l5">今月の厳選パフェ</h4>
				<ul class="cf">
						<?php query_posts( Array(
							'post_type' => array('menu'),
							'showposts' => -1,
							'menu_cat' => 'special'
							));
						if (have_posts()) : while (have_posts()) : the_post(); ?>
						<li><a href="<?php the_permalink(); ?>">
						<p class="bg_img" style="background-image: url('<?php	$image_id = SCF::get('img-main');$image = wp_get_attachment_image_src($image_id, 'full');echo $image[0]; ?>')"></p>
						<p><?php the_title(); ?><br>
								<?php $val =  scf::get('txt-price');
								if (empty($val)) {
									echo '';
								} else {
									echo $val.'円';
								} ?></p>
						</a></li>
						<?php endwhile; endif; ?>
				</ul>
						<h4 class="menu_cate">HAPPYWEEKパフェ</h4>
				<ul class="cf">
						<?php query_posts( Array(
							'post_type' => array('menu'),
							'showposts' => -1,
							'menu_cat' => 'happyweek'
							));
						if (have_posts()) : while (have_posts()) : the_post(); ?>
						<li><a href="<?php the_permalink(); ?>">
						<p class="bg_img" style="background-image: url('<?php	$image_id = SCF::get('img-main');$image = wp_get_attachment_image_src($image_id, 'full');echo $image[0]; ?>')"></p>
						<p><?php the_title(); ?><br>
								<?php $val =  scf::get('txt-price');
								if (empty($val)) {
									echo '';
								} else {
									echo $val.'円';
								} ?></p>
						</a></li>
						<?php endwhile; endif; ?>
				</ul>
				<h4 class="menu_cate">今月のおすすめメニュー</h4>
				<ul class="cf">
						<?php query_posts( Array(
							'post_type' => array('menu'),
							'showposts' => -1,
							'menu_cat' => 'osusume'
							));
						if (have_posts()) : while (have_posts()) : the_post(); ?>
						<li><a href="<?php the_permalink(); ?>">
						<p class="bg_img" style="background-image: url('<?php	$image_id = SCF::get('img-main');$image = wp_get_attachment_image_src($image_id, 'full');echo $image[0]; ?>')"></p>
						<p><?php the_title(); ?><br>
								<?php $val =  scf::get('txt-price');
								if (empty($val)) {
									echo '';
								} else {
									echo $val.'円';
								} ?></p>
						</a></li>
						<?php endwhile; endif; ?>
				</ul>
				</div>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>

<?php
get_footer();
