<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 */
?>
<!doctype html>
<html lang="ja-JP">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
	<meta charset="UTF-8">
	<meta content="" name="copyright">
	<meta content="<?php wp_title( '｜', true, 'right' ); ?>" name="description">
	<meta name="viewport" content="user-scalable=yes" />
	<meta name="format-detection" content="telephone=no" />
	<title><?php wp_title( '｜', true, 'right' ); ?></title>
	<link rel="stylesheet" href="http://www.css-reset.com/css/meyer.css" />
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/common.css" />
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/editor-style.css" />
	<?php if ( is_home() ) : ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/top.css" />
	<?php else: ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/style.css" />
	<?php endif; ?>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/js/colorbox/colorbox.css" />
	<script src="<?php bloginfo('template_url'); ?>/common/js/colorbox/jquery.colorbox-min.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
			$(".colorbox").colorbox();
		//メニューのスタイル(ie8対応)
		$('.menu ul li:nth-child(3n+1)').css('margin-left', '0');
		$('.m_list ul li:nth-child(3n+1)').css('clear', 'both');
	
	$('.mceContentBody p:has(.aligncenter)').css("text-align","center"); 
	
	});
	</script>
	<!--[if lt IE 9]>
		<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
	<![endif]-->
<?php if ( is_page('infomation') ) : ?>
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script type="text/javascript">
		function googleMap() {
		var latlng = new google.maps.LatLng(36.570986,136.654466);/* 座標 */
		var myOptions = {
		zoom: 17, /*拡大比率*/
		center: latlng,
		scrollwheel: false // スクロール無効
		};
		var map = new google.maps.Map(document.getElementById('googlemap'), myOptions);
		var markerOptions = {
		position: latlng,
		map: map
		};
		var marker = new google.maps.Marker(markerOptions);
		};
		
		google.maps.event.addDomListener(window, 'load', function() {
		googleMap();
		});
	</script>
	<?php endif; ?>
</head>
<?php wp_head(); ?>
<body>
	<header class="cf">
		<h1><a href="http://demdm.net/murahata_ec/ec/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/common/images/common/logo_head.jpg" alt="フルーツパーラー" width="97" height="41"></a></h1>
		<ul id="nav">
			<li<?php if ( !is_singular('menu') ) {if ( is_category('news') or is_single() ){echo ' class="on_page"' ;} ;}; ?>><a href="<?php bloginfo('url'); ?>/lists/news/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/nav_news.jpg" alt="お知らせ" width="99" height="41"></a></li>
			<li<?php if ( is_post_type_archive('menu') or is_singular('menu') or is_page('parfait') or is_page('sweets') or is_page('food') or is_page('drink') )  {echo ' class="on_page"' ;};  ?>><a href="<?php bloginfo('url'); ?>/menu/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/nav_menu.jpg" alt="メニュー" width="99" height="41"></a></li>
			<li<?php if ( is_page('infomation') ) {echo ' class="on_page"' ;}; ?>><a href="<?php bloginfo('url'); ?>/infomation/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/nav_info.jpg" alt="店舗情報" width="99" height="41"></a></li>
			<li><a href="http://demdm.net/murahata_ec/ec/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/common/images/common/nav_shop.jpg" alt="ショッピング" width="129" height="41"></a></li>
		</ul>
	</header>
