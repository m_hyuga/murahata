$(function(){

		// リストの高さ修正
    //一行あたりの要素数=列数を変数に格納
    var columns = 3;
    //該当するli要素の数を取得
    var liLen = $('.menu .m_list li').length;
    //行の数を取得
    var lineLen = Math.ceil(liLen / columns);
    //各li要素の高さの配列
    var liHiArr = [];
    //行の高さの配列
    var lineHiArr = [];

    //liの高さを配列に格納後、それが行の中で一番高い値であれば行の高さとして格納していく繰り返し文
    for (i = 0; i <= liLen; i++) {
        liHiArr[i] = $('.menu .m_list li').eq(i).height();
        if (lineHiArr.length <= Math.floor(i / columns) 
            || lineHiArr[Math.floor(i / columns)] < liHiArr[i]) 
        {
            lineHiArr[Math.floor(i / columns)] = liHiArr[i];
        }
    }
    
    //.eachでそれぞれのliの高さを変更
    $('.menu .m_list li').each(function(i){
        $(this).css('height',lineHiArr[Math.floor(i / columns)] + 'px');
    });

});