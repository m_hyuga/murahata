<?php
/**
 * Template Name: menu-page
 */
get_header(); ?>
	<script type="text/javascript">
	$(function(){
		//メニューのスタイル(ie8対応)
		$('.menu ul li:nth-child(3n+1)').css('margin-left', '0');
	});
	</script>
	<div id="content" class="menu">
		<h2><img src="<?php bloginfo('template_url'); ?>/common/images/menu/ttl_h2_01.jpg" alt="MENU" width="1001" height="134"></h2>
		<div class="cf">
			<div id="menu-list" class="fll">
				<div class="ttl_menu">
					<ul id="name-list" class="tbl">
						<li><span class="arrow"></span><a href="<?php bloginfo('url'); ?>/menu/">今月の厳選・おすすめ・HAPPY WEEKパフェ</a></li>
						<li><span class="arrow"></span><a href="<?php bloginfo('url'); ?>/parfait/">パフェ</a></li>
						<li><span class="arrow"></span><a href="<?php bloginfo('url'); ?>/sweets/">スイーツ</a></li>
						<li><span class="arrow"></span><a href="<?php bloginfo('url'); ?>/food/">軽食</a></li>
						<li><span class="arrow"></span><a href="<?php bloginfo('url'); ?>/drink/">ドリンク</a></li>
					</ul>
				</div>
				<h4 class="menu_cate"><?php the_title(); ?></h4>
				<ul class="m_list cf">
				<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
				
				<?php
				$repeat_group = scf::get('menu-list');
				foreach ( $repeat_group as $field_name => $field_value ) :
				?>
						<li>
						<p class="bg_img" style="background-image: url('<?php 
														$val =  $field_value["img-main"];
															if (empty($val)) {
																echo '';
															} else {
															$image = wp_get_attachment_image_src($val, 'medium');echo $image[0];
															}
													?>')"></p>
						<p><?php $val =  $field_value["txt-name"];
								if (empty($val)) {
									echo '';
								} else {
								echo $val;
								} ?><br>
								<?php $val =  $field_value["txt-price"];
								if (empty($val)) {
									echo '';
								} else {
								echo $val.'円';
								} ?></p>
						</li>
				<?php endforeach; ?>
				<?php endwhile; endif; ?>
				</ul>
			</div>
				<?php get_sidebar(); ?>
		</div>
	</div>

<?php
get_footer();
