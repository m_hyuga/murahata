<?php
/**
 * The sidebar containing the main widget area
 */?>
			<div id="side">
				<div class="calendar">
					<?php dynamic_sidebar(calendar); ?>
				</div>
				<ul class="side_btn">
					<li id="no02"><a href="<?php bloginfo('url'); ?>/infomation/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/btn_shopinfo.jpg" alt="店舗情報" width="282" height="70"></a></li>
				</ul>
			</div>
