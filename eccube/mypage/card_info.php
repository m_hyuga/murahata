<?php
require_once('../require.php');
require_once(MODULE_REALDIR . 'mdl_sln/defines.php');
require_once(SLN_CLASS_PATH . 'SLN_ChangeCard.php');
/*
 ** カード情報設定
 *
 * @package スマートリンクネットワーク決済プラグイン
 * @author SmartLink Network, Inc.
 * @version 1.1.0
*/
$objectPage = new SLN_ChangeCard();
$objectPage->init();
$objectPage->process();

