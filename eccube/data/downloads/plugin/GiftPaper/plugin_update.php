<?php
/*
 * GiftPaper
 *
 * Copyright(c) 2009-2014 CUORE CO.,LTD. All Rights Reserved.
 *
 * http://ec.cuore.jp/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/**
 * のし対応プラグイン のアップデートクラス.
 *
 * @package GiftPaper
 * @author CUORE CO.,LTD.
 */
class plugin_update {

    /**
     * アップデート
     * updateはアップデート時に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param  array $arrPlugin プラグイン情報の連想配列(dtb_plugin)
     * @return void
     */
    function update($arrPlugin) {
        // ファイル更新
        if(copy(DOWNLOADS_TEMP_PLUGIN_UPDATE_DIR . 'plugin_info.php',                                                     PLUGIN_UPLOAD_REALDIR . 'GiftPaper/plugin_info.php') === false) print_r('失敗');
        if(copy(DOWNLOADS_TEMP_PLUGIN_UPDATE_DIR . 'plugin_update.php',                                                   PLUGIN_UPLOAD_REALDIR . 'GiftPaper/plugin_update.php') === false) print_r('失敗');
        if(copy(DOWNLOADS_TEMP_PLUGIN_UPDATE_DIR . 'GiftPaper.php',                                                       PLUGIN_UPLOAD_REALDIR . 'GiftPaper/GiftPaper.php') === false) print_r('失敗');
        if(copy(DOWNLOADS_TEMP_PLUGIN_UPDATE_DIR . 'logo.png',                                                            PLUGIN_UPLOAD_REALDIR . 'GiftPaper/logo.png') === false) print_r('失敗');
        if(copy(DOWNLOADS_TEMP_PLUGIN_UPDATE_DIR . 'plg_GiftPaper_SC_Helper_Mail.php',                                    PLUGIN_UPLOAD_REALDIR . 'GiftPaper/plg_GiftPaper_SC_Helper_Mail.php') === false) print_r('失敗');
        if(copy(DOWNLOADS_TEMP_PLUGIN_UPDATE_DIR . 'plg_order_mail.tpl',                                                  PLUGIN_UPLOAD_REALDIR . 'GiftPaper/plg_order_mail.tpl') === false) print_r('失敗');
        if(copy(DOWNLOADS_TEMP_PLUGIN_UPDATE_DIR . 'templates/admin/products/plg_GiftPaper_product_tr.tpl',               PLUGIN_UPLOAD_REALDIR . 'GiftPaper/templates/admin/products/plg_GiftPaper_product_tr.tpl') === false) print_r('失敗');
        if(copy(DOWNLOADS_TEMP_PLUGIN_UPDATE_DIR . 'templates/admin/products/plg_GiftPaper_confirm_tr.tpl',               PLUGIN_UPLOAD_REALDIR . 'GiftPaper/templates/admin/products/plg_GiftPaper_confirm_tr.tpl') === false) print_r('失敗');
        if(copy(DOWNLOADS_TEMP_PLUGIN_UPDATE_DIR . 'templates/admin/order/plg_GiftPaper_disp_tr.tpl',                     PLUGIN_UPLOAD_REALDIR . 'GiftPaper/templates/admin/order/plg_GiftPaper_disp_tr.tpl') === false) print_r('失敗');
        if(copy(DOWNLOADS_TEMP_PLUGIN_UPDATE_DIR . 'templates/admin/order/plg_GiftPaper_edit_tr.tpl',                     PLUGIN_UPLOAD_REALDIR . 'GiftPaper/templates/admin/order/plg_GiftPaper_edit_tr.tpl') === false) print_r('失敗');
        if(copy(DOWNLOADS_TEMP_PLUGIN_UPDATE_DIR . 'templates/default/mypage/plg_GiftPaper_history_span.tpl',             PLUGIN_UPLOAD_REALDIR . 'GiftPaper/templates/default/mypage/plg_GiftPaper_history_span.tpl') === false) print_r('失敗');
        if(copy(DOWNLOADS_TEMP_PLUGIN_UPDATE_DIR . 'templates/default/shopping/plg_GiftPaper_payment_div.tpl',            PLUGIN_UPLOAD_REALDIR . 'GiftPaper/templates/default/shopping/plg_GiftPaper_payment_div.tpl') === false) print_r('失敗');
        if(copy(DOWNLOADS_TEMP_PLUGIN_UPDATE_DIR . 'templates/default/shopping/plg_GiftPaper_confirm_tr.tpl',             PLUGIN_UPLOAD_REALDIR . 'GiftPaper/templates/default/shopping/plg_GiftPaper_confirm_tr.tpl') === false) print_r('失敗');
        if(copy(DOWNLOADS_TEMP_PLUGIN_UPDATE_DIR . 'templates/mobile/mypage/plg_GiftPaper_history.tpl',                   PLUGIN_UPLOAD_REALDIR . 'GiftPaper/templates/mobile/mypage/plg_GiftPaper_history.tpl') === false) print_r('失敗');
        if(copy(DOWNLOADS_TEMP_PLUGIN_UPDATE_DIR . 'templates/mobile/shopping/plg_GiftPaper_payment.tpl',                 PLUGIN_UPLOAD_REALDIR . 'GiftPaper/templates/mobile/shopping/plg_GiftPaper_payment.tpl') === false) print_r('失敗');
        if(copy(DOWNLOADS_TEMP_PLUGIN_UPDATE_DIR . 'templates/mobile/shopping/plg_GiftPaper_confirm.tpl',                 PLUGIN_UPLOAD_REALDIR . 'GiftPaper/templates/mobile/shopping/plg_GiftPaper_confirm.tpl') === false) print_r('失敗');
        if(copy(DOWNLOADS_TEMP_PLUGIN_UPDATE_DIR . 'templates/sphone/mypage/plg_GiftPaper_history_em.tpl',                PLUGIN_UPLOAD_REALDIR . 'GiftPaper/templates/sphone/mypage/plg_GiftPaper_history_em.tpl') === false) print_r('失敗');
        if(copy(DOWNLOADS_TEMP_PLUGIN_UPDATE_DIR . 'templates/sphone/shopping/plg_GiftPaper_payment_section.tpl',         PLUGIN_UPLOAD_REALDIR . 'GiftPaper/templates/sphone/shopping/plg_GiftPaper_payment_section.tpl') === false) print_r('失敗');
        if(copy(DOWNLOADS_TEMP_PLUGIN_UPDATE_DIR . 'templates/sphone/shopping/plg_GiftPaper_payment_section_replace.tpl', PLUGIN_UPLOAD_REALDIR . 'GiftPaper/templates/sphone/shopping/plg_GiftPaper_payment_section_replace.tpl') === false) print_r('失敗');
        if(copy(DOWNLOADS_TEMP_PLUGIN_UPDATE_DIR . 'templates/sphone/shopping/plg_GiftPaper_confirm_div.tpl',             PLUGIN_UPLOAD_REALDIR . 'GiftPaper/templates/sphone/shopping/plg_GiftPaper_confirm_div.tpl') === false) print_r('失敗');

        if(copy(DOWNLOADS_TEMP_PLUGIN_UPDATE_DIR . 'plg_order_mail.tpl', TEMPLATE_REALDIR . 'mail_templates/plg_order_mail.tpl') === false) print_r('失敗');
        if(copy(DOWNLOADS_TEMP_PLUGIN_UPDATE_DIR . 'plg_order_mail.tpl', MOBILE_TEMPLATE_REALDIR . 'mail_templates/plg_order_mail.tpl') === false) print_r('失敗');

        // logo.png更新
        if(copy(DOWNLOADS_TEMP_PLUGIN_UPDATE_DIR . 'logo.png', PLUGIN_HTML_REALDIR . 'GiftPaper/logo.png') === false) print_r('失敗');

        // インストール済みのバージョンが0.4未満の場合
        if(version_compare($arrPlugin['plugin_version'], '0.4', '<')) {
            $objQuery =& SC_Query_Ex::getSingletonInstance();
            $objQuery->begin();
            // mtb_constantsテーブルにレコードを追加する
            $arrSqlVal = array(
                    'id'      => 'PLG_GIFTPAPER_NOSHI_NAME',
                    'name'    => '"のし"',
                    'rank'    => 101,
                    'remarks' => '「のし」項目表記文言'
            );
            $objQuery->insert('mtb_constants', $arrSqlVal);
            $arrSqlVal = array(
                    'id'      => 'PLG_GIFTPAPER_NOSHI_TYPE',
                    'name'    => '1',
                    'rank'    => 102,
                    'remarks' => '「のしの指定」指定方法値(1:セレクトボックス 2:ラジオボタン)'
            );
            $objQuery->insert('mtb_constants', $arrSqlVal);
            $objQuery->commit();
        }

        // EC-CUBEのバージョンが2.13.0未満の場合
        if(version_compare(ECCUBE_VERSION , '2.13.0', '<')) {
            // リフレクションオブジェクトを生成.
            $objReflection = new ReflectionClass('plugin_info');
            $arrPluginInfo = getPluginInfo($objReflection);
            // プラグイン情報を更新
            updateData($arrPlugin['plugin_id'], $arrPluginInfo);
        }
    }
}

/**
 * プラグイン情報を取得します.
 *
 * @param  ReflectionClass $objReflection
 * @return array           プラグイン情報の配列
 */
function getPluginInfo(ReflectionClass $objReflection) {
    $arrStaticProps = $objReflection->getStaticProperties();
    $arrConstants   = $objReflection->getConstants();

    $arrPluginInfoKey = array(
        'PLUGIN_CODE',
        'PLUGIN_NAME',
        'CLASS_NAME',
        'PLUGIN_VERSION',
        'COMPLIANT_VERSION',
        'AUTHOR',
        'DESCRIPTION',
        'PLUGIN_SITE_URL',
        'AUTHOR_SITE_URL',
        'HOOK_POINTS',
    );
    $arrPluginInfo = array();
    foreach ($arrPluginInfoKey as $key) {
        // クラス変数での定義を優先
        if (isset($arrStaticProps[$key])) {
            $arrPluginInfo[$key] = $arrStaticProps[$key];
        // クラス変数定義がなければ, クラス定数での定義を読み込み.
        } elseif ($arrConstants[$key]) {
            $arrPluginInfo[$key] = $arrConstants[$key];
        } else {
            $arrPluginInfo[$key] = null;
        }
    }

    return $arrPluginInfo;
}

/**
 * プラグイン情報を更新.
 *
 * @param  string $plugin_id     プラグインID
 * @param  array  $arrPluginInfo プラグイン情報を格納した連想配列.
 * @return array                 エラー情報を格納した連想配列.
 */
function updateData($plugin_id, $arrPluginInfo) {
    $objQuery =& SC_Query_Ex::getSingletonInstance();
    $objQuery->begin();

    // プラグイン情報を更新
    $arr_sqlval_plugin = array();
    $arr_sqlval_plugin['plugin_name'] = $arrPluginInfo['PLUGIN_NAME'];
    $arr_sqlval_plugin['class_name'] = $arrPluginInfo['CLASS_NAME'];
    $arr_sqlval_plugin['author'] = $arrPluginInfo['AUTHOR'];
    // AUTHOR_SITE_URLが定義されているか判定.
    $author_site_url = $arrPluginInfo['AUTHOR_SITE_URL'];
    if ($author_site_url !== null) {
        $arr_sqlval_plugin['author_site_url'] = $arrPluginInfo['AUTHOR_SITE_URL'];
    }
    // PLUGIN_SITE_URLが定義されているか判定.
    $plugin_site_url = $arrPluginInfo['PLUGIN_SITE_URL'];
    if ($plugin_site_url !== null) {
        $arr_sqlval_plugin['plugin_site_url'] = $plugin_site_url;
    }
    $arr_sqlval_plugin['plugin_version'] = $arrPluginInfo['PLUGIN_VERSION'];
    $arr_sqlval_plugin['compliant_version'] = $arrPluginInfo['COMPLIANT_VERSION'];
    $arr_sqlval_plugin['plugin_description'] = $arrPluginInfo['DESCRIPTION'];
    $arr_sqlval_plugin['update_date'] = 'CURRENT_TIMESTAMP';
    $objQuery->update('dtb_plugin', $arr_sqlval_plugin, 'plugin_id = ?', array($plugin_id));

    // 該当プラグインのフックポイントを一旦削除
    $objQuery->delete('dtb_plugin_hookpoint', 'plugin_id = ? ', array($plugin_id));

    // フックポイントをDB登録.
    $hook_point = $arrPluginInfo['HOOK_POINTS'];
    if ($hook_point !== null) {
        /**
         * FIXME コードが重複しているため、要修正
         */
        // フックポイントが配列で定義されている場合
        if (is_array($hook_point)) {
            foreach ($hook_point as $h) {
                $arr_sqlval_plugin_hookpoint = array();
                $id = $objQuery->nextVal('dtb_plugin_hookpoint_plugin_hookpoint_id');
                $arr_sqlval_plugin_hookpoint['plugin_hookpoint_id'] = $id;
                $arr_sqlval_plugin_hookpoint['plugin_id'] = $plugin_id;
                $arr_sqlval_plugin_hookpoint['hook_point'] = $h[0];
                $arr_sqlval_plugin_hookpoint['callback'] = $h[1];
                $arr_sqlval_plugin_hookpoint['update_date'] = 'CURRENT_TIMESTAMP';
                $objQuery->insert('dtb_plugin_hookpoint', $arr_sqlval_plugin_hookpoint);
            }
        // 文字列定義の場合
        } else {
            $array_hook_point = explode(',', $hook_point);
            foreach ($array_hook_point as $h) {
                $arr_sqlval_plugin_hookpoint = array();
                $id = $objQuery->nextVal('dtb_plugin_hookpoint_plugin_hookpoint_id');
                $arr_sqlval_plugin_hookpoint['plugin_hookpoint_id'] = $id;
                $arr_sqlval_plugin_hookpoint['plugin_id'] = $plugin_id;
                $arr_sqlval_plugin_hookpoint['hook_point'] = $h;
                $arr_sqlval_plugin_hookpoint['update_date'] = 'CURRENT_TIMESTAMP';
                $objQuery->insert('dtb_plugin_hookpoint', $arr_sqlval_plugin_hookpoint);
            }
        }
    }

    return $objQuery->commit();
}
?>
