<!--{*
 * GiftPaper
 *
 * Copyright(c) 2009-2012 CUORE CO.,LTD. All Rights Reserved.
 *
 * http://ec.cuore.jp/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *}-->

        <!--{if $plg_GiftPaper_gift_paper_flg}-->
            <!--{assign var=key value="plg_giftpaper_gift_paper"}-->
            <!--{if $arrErr[$key] != ""}-->
                <font color="#FF0000"><!--{$arrErr[$key]}--></font>
            <!--{/if}-->
            ■<!--{$smarty.const.PLG_GIFTPAPER_NOSHI_NAME}-->の種類<br />
            ご希望の方は、<!--{$smarty.const.PLG_GIFTPAPER_NOSHI_NAME}-->の種類を選択してください。<br />選択肢の中にご希望のものがございませんでしたら、<br />「その他」をお選びいただき、<br />その他お問い合わせ欄にお書き添えくださいませ。<br>
            <!--{if $smarty.const.PLG_GIFTPAPER_NOSHI_TYPE == 1}-->
                <select name="<!--{$key}-->">
                    <!--{html_options options=$plg_GiftPaper_arrGiftPaper selected=$arrForm[$key]}-->
                </select>
            <!--{elseif $smarty.const.PLG_GIFTPAPER_NOSHI_TYPE == 2}-->
                <!--{foreach from=$plg_GiftPaper_arrGiftPaper key=k item=v}-->
                    <input type="radio" name="<!--{$key}-->" value="<!--{$k}-->" <!--{$k|sfGetChecked:$arrForm[$key].value}--> /><label><!--{$v|h}--></label><br />
                <!--{/foreach}-->
            <!--{/if}-->
            <br />
            <br />
        <!--{/if}-->
