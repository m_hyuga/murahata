<!--{*
 * GiftPaper
 *
 * Copyright(c) 2009-2012 CUORE CO.,LTD. All Rights Reserved.
 *
 * http://ec.cuore.jp/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *}-->

        <!--{if $plg_GiftPaper_gift_paper_flg}-->
            <section class="pay_area02">
                <h3 class="subtitle"><!--{$smarty.const.PLG_GIFTPAPER_NOSHI_NAME}-->の選択</h3>
                <div class="form_area">
                    <p>ご希望の方は、<!--{$smarty.const.PLG_GIFTPAPER_NOSHI_NAME}-->の種類を選択してください。<br />選択肢の中にご希望のものがございませんでしたら、<br />「その他」をお選びいただき、その他お問い合わせ欄にお書き添えくださいませ。</p>
                    <!--★のしの選択★-->
                    <!--{assign var=key value="plg_giftpaper_gift_paper"}-->
                    <span class="attention"><!--{$arrErr[$key]}--></span>
                    <!--{if $smarty.const.PLG_GIFTPAPER_NOSHI_TYPE == 1}-->
                        <select name="<!--{$key}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->">
                            <!--{html_options options=$plg_GiftPaper_arrGiftPaper selected=$arrForm[$key]}-->
                        </select>
                    <!--{elseif $smarty.const.PLG_GIFTPAPER_NOSHI_TYPE == 2}-->
                        <ul>
                            <!--{foreach from=$plg_GiftPaper_arrGiftPaper key=k item=v}-->
                                <li><input type="radio" name="<!--{$key}-->" value="<!--{$k}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" <!--{$k|sfGetChecked:$arrForm[$key].value}--> class="data-role-none" /><label><!--{$v|h}--></label></li>
                            <!--{/foreach}-->
                        </ul>
                    <!--{/if}-->
                </div>
            </section>
        <!--{/if}-->