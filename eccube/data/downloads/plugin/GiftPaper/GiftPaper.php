<?php
/*
 * GiftPaper
 *
 * Copyright(c) 2009-2012 CUORE CO.,LTD. All Rights Reserved.
 *
 * http://ec.cuore.jp/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/**
 * のし対応プラグイン のメインクラス.
 *
 * @package GiftPaper
 * @author CUORE CO.,LTD.
 */
class GiftPaper extends SC_Plugin_Base {

    /**
     * コンストラクタ
     *
     */
    public function __construct(array $arrSelfInfo) {
        parent::__construct($arrSelfInfo);
    }

    /**
     * インストール
     * installはプラグインのインストール時に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param array $arrPlugin plugin_infoを元にDBに登録されたプラグイン情報(dtb_plugin)
     * @return void
     */
    function install($arrPlugin) {
        $objQuery = SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();
        // dtb_productsテーブルにnoshiカラムを追加する
        $objQuery->query("ALTER TABLE dtb_products ADD COLUMN plg_giftpaper_noshi smallint NOT NULL DEFAULT 2");
        // dtb_orderテーブルにgift_paperカラムを追加する
        $objQuery->query("ALTER TABLE dtb_order ADD COLUMN plg_giftpaper_gift_paper smallint");
        // dtb_order_tempテーブルにgift_paperカラムを追加する
        $objQuery->query("ALTER TABLE dtb_order_temp ADD COLUMN plg_giftpaper_gift_paper smallint");
        // mtb_noshiテーブルを作成する
        $objQuery->query("CREATE TABLE plg_giftpaper_mtb_noshi (id smallint, name text, rank smallint NOT NULL DEFAULT 0, PRIMARY KEY (id))");
        // mtb_gift_paperテーブルを作成する
        $objQuery->query("CREATE TABLE plg_giftpaper_mtb_gift_paper (id smallint, name text, rank smallint NOT NULL DEFAULT 0, PRIMARY KEY (id))");

        // dtb_csvテーブルにレコードを追加
        $csv_id = array(1,3);
        $col = array('plg_giftpaper_noshi','plg_giftpaper_gift_paper');
        $disp_name = array('のし選択(1:可 2:不可)','のしの種類');
        $rank = array(72,58);
        $error_check_types = array('EXIST_CHECK,NUM_CHECK,MAX_LENGTH_CHECK','NUM_CHECK,MAX_LENGTH_CHECK');
        for($i=0;$i<2;$i++){
            $sqlval_dtb_csv = array();
            $sqlval_dtb_csv['no'] = $no[$i] = $objQuery->nextVal('dtb_csv_no');
            $sqlval_dtb_csv['csv_id'] = $csv_id[$i];
            $sqlval_dtb_csv['col'] = $col[$i];
            $sqlval_dtb_csv['disp_name'] = $disp_name[$i];
            $sqlval_dtb_csv['rank'] = $rank[$i];
            $sqlval_dtb_csv['rw_flg'] = 1;
            $sqlval_dtb_csv['status'] = 1;
            $sqlval_dtb_csv['create_date'] = CURRENT_TIMESTAMP;
            $sqlval_dtb_csv['update_date'] = CURRENT_TIMESTAMP;
            $sqlval_dtb_csv['mb_convert_kana_option'] = "n";
            $sqlval_dtb_csv['size_const_type'] = "INT_LEN";
            $sqlval_dtb_csv['error_check_types'] = $error_check_types[$i];
            $objQuery->insert("dtb_csv", $sqlval_dtb_csv);
        }

        // mtb_gift_paperテーブルにレコードを追加する
        $name = array('希望しない','無地のし（紅白）','内祝（蝶むすび）','内祝（むすび切り）','内祝（婚礼）','快気祝','粗供養','志（蓮のし）','無地のし（仏）','御礼','御挨拶','その他');
        for($i=0;$i<12;$i++){
            $sqlval_mtb_gift_paper = array();
            $sqlval_mtb_gift_paper['id'] = $i;
            $sqlval_mtb_gift_paper['name'] = $name[$i];
            $sqlval_mtb_gift_paper['rank'] = $i;
            $objQuery->insert("plg_giftpaper_mtb_gift_paper", $sqlval_mtb_gift_paper);
        }

        // mtb_noshiテーブルにレコードを追加する
        $name = array('可','不可');
        for($i=0;$i<2;$i++){
            $sqlval_mtb_noshi = array();
            $sqlval_mtb_noshi['id'] = $i + 1;
            $sqlval_mtb_noshi['name'] = $name[$i];
            $sqlval_mtb_noshi['rank'] = $i;
            $objQuery->insert("plg_giftpaper_mtb_noshi", $sqlval_mtb_noshi);
        }

        // mtb_constantsテーブルにレコードを追加する
        $arrSqlVal = array("id"      => "PLG_GIFTPAPER_DEFAULT_PRODUCT_NOSHI",
                           "name"    => "2",
                           "rank"    => 100,
                           "remarks" => "「のし選択」デフォルト選択値(1:可 2:不可)"
        );
        $objQuery->insert("mtb_constants", $arrSqlVal);
        $arrSqlVal = array("id"      => "PLG_GIFTPAPER_NOSHI_NAME",
                           "name"    => "\"のし\"",
                           "rank"    => 101,
                           "remarks" => "「のし」項目表記文言"
        );
        $objQuery->insert("mtb_constants", $arrSqlVal);
        $arrSqlVal = array("id"      => "PLG_GIFTPAPER_NOSHI_TYPE",
                           "name"    => "1",
                           "rank"    => 102,
                           "remarks" => "「のしの指定」指定方法値(1:セレクトボックス 2:ラジオボタン)"
        );
        $objQuery->insert("mtb_constants", $arrSqlVal);
        $objQuery->commit();

        // ファイルコピー
        if(copy(PLUGIN_UPLOAD_REALDIR . "GiftPaper/logo.png", PLUGIN_HTML_REALDIR . "GiftPaper/logo.png") === false) print_r("失敗");
        if(copy(PLUGIN_UPLOAD_REALDIR . "GiftPaper/plg_order_mail.tpl", DATA_REALDIR . "Smarty/templates/default/mail_templates/plg_order_mail.tpl") === false) print_r("失敗");
        if(copy(PLUGIN_UPLOAD_REALDIR . "GiftPaper/plg_order_mail.tpl", DATA_REALDIR . "Smarty/templates/mobile/mail_templates/plg_order_mail.tpl") === false) print_r("失敗");
    }

    /**
     * アンインストール
     * uninstallはアンインストール時に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param array $arrPlugin プラグイン情報の連想配列(dtb_plugin)
     * @return void
     */
    function uninstall($arrPlugin) {
        $masterData = new SC_DB_MasterData_Ex();
        $objQuery = SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();
        // dtb_productsテーブルのnoshiカラムを削除する
        $objQuery->query("ALTER TABLE dtb_products DROP COLUMN plg_giftpaper_noshi");
        // dtb_orderテーブルのgift_paperカラムを削除する
        $objQuery->query("ALTER TABLE dtb_order DROP COLUMN plg_giftpaper_gift_paper");
        // dtb_order_tempテーブルのgift_paperカラムを削除する
        $objQuery->query("ALTER TABLE dtb_order_temp DROP COLUMN plg_giftpaper_gift_paper");
        // mtb_noshiテーブルを削除する
        $objQuery->query("DROP TABLE plg_giftpaper_mtb_noshi");
        // mtb_gift_paperテーブルを削除する
        $objQuery->query("DROP TABLE plg_giftpaper_mtb_gift_paper");
        // dtb_csvテーブルのレコードを削除する
        $csv_no = $objQuery->select('no', 'dtb_csv', 'col = ? AND csv_id = ?', array("plg_giftpaper_noshi", 1));
        $objQuery->delete("dtb_csv", "no=?", array($csv_no[0]['no']));
        $csv_no = $objQuery->select('no', 'dtb_csv', 'col = ? AND csv_id = ?', array("plg_giftpaper_gift_paper", 3));
        $objQuery->delete("dtb_csv", "no=?", array($csv_no[0]['no']));

        // mtb_constantsテーブルのレコードを削除する
        $objQuery->delete("mtb_constants", "id=?", array("PLG_GIFTPAPER_DEFAULT_PRODUCT_NOSHI"));
        $objQuery->delete("mtb_constants", "id=?", array("PLG_GIFTPAPER_NOSHI_NAME"));
        $objQuery->delete("mtb_constants", "id=?", array("PLG_GIFTPAPER_NOSHI_TYPE"));
        $objQuery->commit();

        //メールテンプレートをのし用のものからデフォルトに変更する
        $masterData->updateMasterData("mtb_mail_tpl_path", array('name','name'), array('mail_templates/plg_order_mail.tpl' => 'mail_templates/order_mail.tpl'));
        $masterData->createCache('mtb_mail_tpl_path');

        // ファイル削除
        if(SC_Helper_FileManager_Ex::deleteFile(PLUGIN_HTML_REALDIR . "GiftPaper/logo.png") === false); // TODO エラー処理
        if(SC_Helper_FileManager_Ex::deleteFile(DATA_REALDIR . "smarty/templates/default/mail_templates/plg_order_mail.tpl") === false); // TODO エラー処理
        if(SC_Helper_FileManager_Ex::deleteFile(DATA_REALDIR . "smarty/templates/mobile/mail_templates/plg_order_mail.tpl") === false); // TODO エラー処理
    }

    /**
     * アップデート
     * updateはアップデート時に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param array $arrPlugin プラグイン情報の連想配列(dtb_plugin)
     * @return void
     */
    function update($arrPlugin) {
        // nop
    }

    /**
     * 稼働
     * enableはプラグインを有効にした際に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param array $arrPlugin プラグイン情報の連想配列(dtb_plugin)
     * @return void
     */
    function enable($arrPlugin) {
        $masterData = new SC_DB_MasterData_Ex();

        //メールテンプレートをのし用のものに変更する
        $masterData->updateMasterData("mtb_mail_tpl_path", array('name','name'), array('mail_templates/order_mail.tpl' => 'mail_templates/plg_order_mail.tpl'));
        $masterData->createCache('mtb_mail_tpl_path');

    }

    /**
     * 停止
     * disableはプラグインを無効にした際に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param array $arrPlugin プラグイン情報の連想配列(dtb_plugin)
     * @return void
     */
    function disable($arrPlugin) {
        $masterData = new SC_DB_MasterData_Ex();

        //メールテンプレートをのし用のものからデフォルトに変更する
        $masterData->updateMasterData("mtb_mail_tpl_path", array('name','name'), array('mail_templates/plg_order_mail.tpl' => 'mail_templates/order_mail.tpl'));
        $masterData->createCache('mtb_mail_tpl_path');

    }

    /**
     * .
     *
     * @param LC_Page $objPage ページ基底クラス
     * @return void
     */
    function get_noshi_data($objPage) {
        $masterData = new SC_DB_MasterData_Ex();
        $objPage->plg_GiftPaper_arrNoshi = $masterData->getMasterData("plg_giftpaper_mtb_noshi");

        if(!defined("PLG_GIFTPAPER_NOSHI_NAME")) define("PLG_GIFTPAPER_NOSHI_NAME", "のし");
    }

    /**
     * .
     *
     * @param LC_Page $objPage ページ基底クラス
     * @return void
     */
    function get_gift_paper_data($objPage) {
        $masterData = new SC_DB_MasterData_Ex();
        $objPage->plg_GiftPaper_arrGiftPaper = $masterData->getMasterData("plg_giftpaper_mtb_gift_paper");

        if(!defined("PLG_GIFTPAPER_NOSHI_NAME")) define("PLG_GIFTPAPER_NOSHI_NAME", "のし");
    }

    /**
     * .
     *
     * @param LC_Page_Admin_Products $objPage 商品管理のページクラス
     * @return void
     */
    function products_product_after($objPage) {
        switch($objPage->getMode($objPage)) {
            case "pre_edit":
            case "copy" :
                // 何もしない
                break;
            case "edit":
            case "upload_image":
            case "delete_image":
            case "upload_down":
            case "delete_down":
            case "recommend_select":
            case "confirm_return":
                // TODO 本来はSC_FormParamを使用してコンバートした値をセットしている
                $objPage->arrForm['plg_giftpaper_noshi'] = $_POST['plg_giftpaper_noshi'];
                break;
            case "complete":
                $objQuery =& SC_Query_Ex::getSingletonInstance();
                $table = "dtb_products";
                $where = "product_id = ?";
                $arrParam['plg_giftpaper_noshi'] = $_POST['plg_giftpaper_noshi'];
                $arrParam['update_date'] = "CURRENT_TIMESTAMP";
                $arrWhere[] = $objPage->arrForm['product_id'];
                $objQuery->update($table, $arrParam, $where, $arrWhere);
                break;
            default:
                if(!defined("PLG_GIFTPAPER_DEFAULT_PRODUCT_NOSHI")) define("PLG_GIFTPAPER_DEFAULT_PRODUCT_NOSHI", 2);
                if(empty($objPage->arrForm['plg_giftpaper_noshi'])) $objPage->arrForm['plg_giftpaper_noshi'] = PLG_GIFTPAPER_DEFAULT_PRODUCT_NOSHI;
                break;
        }
    }

    /**
     * .
     *
     * @param LC_Page_Admin_Order_Disp $objPage 受注情報表示のページクラス
     * @return void
     */
    function order_disp_after($objPage) {
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        // 検索条件設定
        $column = "plg_giftpaper_gift_paper";
        $table = "dtb_order";
        $where = "order_id = ?";

        // 検索パラメータ設定
        $arrWhere[] = $objPage->arrForm['order_id']['value'];

        // SELECT実行
        $arrOrder = $objQuery->getRow($column, $table, $where, $arrWhere);

        // 値セット
        $objPage->arrForm['plg_giftpaper_gift_paper'] = $arrOrder['plg_giftpaper_gift_paper'];
    }

    /**
     * .
     *
     * @param LC_Page_Admin_Order_Edit $objPage 受注修正のページクラス
     * @return void
     */
    function order_edit_after($objPage) {
        $objPage->arrForm['plg_giftpaper_gift_paper'] = "0";
        if(!SC_Utils_Ex::isBlank($_REQUEST['order_id'])) {
            $objQuery =& SC_Query_Ex::getSingletonInstance();
            $column = "plg_giftpaper_gift_paper";
            $table = "dtb_order";
            $where = "order_id = ?";
            $arrWhere[] = $objPage->arrForm['order_id']['value'];
            $arrOrder = $objQuery->getRow($column, $table, $where, $arrWhere);
            $objPage->arrForm['plg_giftpaper_gift_paper'] = $arrOrder['plg_giftpaper_gift_paper'];
        }
        switch($objPage->getMode()) {
            case "edit":
            case "add":
                if(SC_Utils_Ex::isBlank($objPage->arrErr)) {
                    $objQuery =& SC_Query_Ex::getSingletonInstance();
                    $table = "dtb_order";
                    $where = "order_id = ?";
                    $arrParam['plg_giftpaper_gift_paper'] = $_POST['plg_giftpaper_gift_paper'];
                    $arrParam['update_date'] = "CURRENT_TIMESTAMP";
                    $arrWhere[] = $objPage->arrForm['order_id']['value'];
                    $objQuery->update($table, $arrParam, $where, $arrWhere);
                }
                break;
            case "pre_edit":
            case "order_id":
            case "recalculate":
            case "payment":
            case "deliv":
            case "delete_product":
            case "select_product_detail":
            case "search_customer":
            case "multiple":
            case "multiple_set_to":
            case "append_shipping":
            default:
                break;
        }
        // 値セット
        if(isset($_POST['plg_giftpaper_gift_paper']) && !empty($_POST['plg_giftpaper_gift_paper'])) $objPage->arrForm['plg_giftpaper_gift_paper'] = $_POST['plg_giftpaper_gift_paper'];
        // のしの種類表示フラグセット
        $objPage->plg_GiftPaper_gift_paper_flg = $this->chkShowGiftPaper($objPage->arrForm['product_id']['value']);

        if(!defined("PLG_GIFTPAPER_NOSHI_TYPE")) define("PLG_GIFTPAPER_NOSHI_TYPE", 1);
    }

    /**
     * .
     *
     * @param LC_Page_Shopping_Payment $objPage 支払い方法選択のページクラス
     * @return void
     */
    function shopping_payment_after($objPage) {
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        // 検索条件設定
        $column = "plg_giftpaper_gift_paper";
        $table = "dtb_order_temp";
        $where = "order_temp_id = ?";

        // 検索パラメータ設定
        $arrWhere[] = $objPage->tpl_uniqid;

        // SELECT実行
        $arrOrder = $objQuery->getRow($column, $table, $where, $arrWhere);

        // 値セット
        $objPage->arrForm['plg_giftpaper_gift_paper'] = "0";
        if(!empty($arrOrder['plg_giftpaper_gift_paper'])) $objPage->arrForm['plg_giftpaper_gift_paper'] = $arrOrder['plg_giftpaper_gift_paper'];
        if(isset($_POST['plg_giftpaper_gift_paper']) && !empty($_POST['plg_giftpaper_gift_paper'])) $objPage->arrForm['plg_giftpaper_gift_paper'] = $_POST['plg_giftpaper_gift_paper'];
        // のしの選択表示フラグセット
        $objPage->plg_GiftPaper_gift_paper_flg = $this->chkShowGiftPaper($this->getCartProductID($objPage->cartKey));

        if(!defined("PLG_GIFTPAPER_NOSHI_TYPE")) define("PLG_GIFTPAPER_NOSHI_TYPE", 1);
    }

    /**
     * .
     *
     * @param LC_Page_Shopping_Payment $objPage 支払い方法選択のページクラス
     * @return void
     */
    function shopping_payment_confirm($objPage) {
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        // 更新条件設定
        $table = "dtb_order_temp";
        $where = "order_temp_id = ?";

        // 更新パラメータ設定
        $arrParam['plg_giftpaper_gift_paper'] = $_POST['plg_giftpaper_gift_paper'];
        $arrParam['update_date'] = "CURRENT_TIMESTAMP";
        $arrWhere[] = $objPage->tpl_uniqid;

        // UPDATE実行
        $objQuery->update($table, $arrParam, $where, $arrWhere);
    }

    function prefilterTransform(&$source, LC_Page_Ex $objPage, $filename) {
        $objTransform = new SC_Helper_Transform($source);
        $template_dir = PLUGIN_UPLOAD_REALDIR ."GiftPaper/templates/";
        switch($objPage->arrPageLayout['device_type_id']) {
            // 端末種別：PC
            case DEVICE_TYPE_PC:
                $template_dir .= "default/";
                // 購入商品詳細画面
                if(strpos($filename, "mypage/history.tpl") !== false) {
                    $objTransform->select("div#mypagecolumn div#mycontents_area div.mycondition_area.clearfix p br", 2)->insertAfter(file_get_contents($template_dir . "mypage/plg_GiftPaper_history_span.tpl"));

                }
                // お支払方法・お届け時間等の指定画面
                if(strpos($filename, "shopping/payment.tpl") !== false) {
                    $objTransform->select("div#undercolumn div#undercolumn_shopping div.pay_area02", 1)->insertBefore(file_get_contents($template_dir . "shopping/plg_GiftPaper_payment_div.tpl"));
                }
                // ご入力内容のご確認画面
                if(strpos($filename, "shopping/confirm.tpl") !== false) {
                   $objTransform->select("div#undercolumn div#undercolumn_shopping table.delivname tr", 11)->insertAfter(file_get_contents($template_dir . 'shopping/plg_GiftPaper_confirm_tr.tpl'));
                }
                break;
            // 端末種別：モバイル
            case DEVICE_TYPE_MOBILE:
                $template_dir .= "mobile/";
                // 購入商品詳細画面
                if(strpos($filename, "mypage/history.tpl") !== false) {
                    $objTransform->select("form", 0)->appendFirst(file_get_contents($template_dir . "mypage/plg_GiftPaper_history.tpl"));
                }
                // お支払方法・お届け時間等の指定画面
                if(strpos($filename, "shopping/payment.tpl") !== false) {
                    $objTransform->select("form br", 12)->insertAfter(file_get_contents($template_dir . "shopping/plg_GiftPaper_payment.tpl"));
                }
                // ご入力内容のご確認画面
                if(strpos($filename, "shopping/confirm.tpl") !== false) {
                    $objTransform->select("form br", 45)->insertAfter(file_get_contents($template_dir . "shopping/plg_GiftPaper_confirm.tpl"));
                }
                break;
            // 端末種別：スマートフォン
            case DEVICE_TYPE_SMARTPHONE:
                $template_dir .= "sphone/";
                // 購入商品詳細画面
                if(strpos($filename, "mypage/history.tpl") !== false) {
                    $objTransform->select("section#mypagecolumn div.form_area div#historyBox p", 0)->appendChild(file_get_contents($template_dir . "mypage/plg_GiftPaper_history_em.tpl"));
                }
                // お支払方法・お届け時間等の指定画面
                if(strpos($filename, "shopping/payment.tpl") !== false) {
                    //リプレイスでの対応
                    $objTransform->select('section#undercolumn')->replaceElement(file_get_contents($template_dir . 'shopping/plg_GiftPaper_payment_section_replace.tpl'));
                    //$objTransform->select("section#undercolumn section.point_area", 0)->insertAfter(file_get_contents($template_dir . "shopping/plg_GiftPaper_payment_section.tpl"));
                }
                // ご入力内容のご確認画面
                if(strpos($filename, "shopping/confirm.tpl") !== false) {
                    $objTransform->select('div.innerBox',1)->insertAfter(file_get_contents($template_dir . 'shopping/plg_GiftPaper_confirm_div.tpl'));
                }
                break;
            // 端末種別：管理画面
            case DEVICE_TYPE_ADMIN:
            default:
                $template_dir .= "admin/";
                // 商品登録画面
                if(strpos($filename, "products/product.tpl") !== false) {
                    $objTransform->select("div#products.contents-main table tr", 5)->insertAfter(file_get_contents($template_dir . "products/plg_GiftPaper_product_tr.tpl"));
                }
                // 商品登録確認画面
                if(strpos($filename, "products/confirm.tpl") !== false) {
                    $objTransform->select("div#products.contents-main table tr", 3)->insertAfter(file_get_contents($template_dir . "products/plg_GiftPaper_confirm_tr.tpl"));
                }
                // 受注情報表示画面
                if(strpos($filename, "order/disp.tpl") !== false) {
                    $objTransform->select("table.form tr", 26)->insertBefore(file_get_contents($template_dir . "order/plg_GiftPaper_disp_tr.tpl"));
                }
                // 受注情報登録・編集画面
                if(strpos($filename, "order/edit.tpl") !== false) {
                    $objTransform->select("div#order.contents-main table.form tr", 29)->insertBefore(file_get_contents($template_dir . "order/plg_GiftPaper_edit_tr.tpl"));
                }
                break;
        }
        $source = $objTransform->getHTML();
    }

    /**
     * 処理の介入箇所とコールバック関数を設定
     * registerはプラグインインスタンス生成時に実行されます
     *
     * @param SC_Helper_Plugin $objHelperPlugin
     */
    function register(SC_Helper_Plugin $objHelperPlugin) {
        $objHelperPlugin->addAction("LC_Page_Admin_Products_Product_action_before", array($this, "get_noshi_data"), $this->arrSelfInfo['priority']);
        $objHelperPlugin->addAction("LC_Page_Admin_Order_Disp_action_before", array($this, "get_gift_paper_data"), $this->arrSelfInfo['priority']);
        $objHelperPlugin->addAction("LC_Page_Admin_Order_Edit_action_before", array($this, "get_gift_paper_data"), $this->arrSelfInfo['priority']);
        $objHelperPlugin->addAction("LC_Page_Shopping_Payment_action_before", array($this, "get_gift_paper_data"), $this->arrSelfInfo['priority']);
        $objHelperPlugin->addAction("LC_Page_Shopping_Confirm_action_before", array($this, "get_gift_paper_data"), $this->arrSelfInfo['priority']);
        $objHelperPlugin->addAction("LC_Page_Mypage_History_action_before", array($this, "get_gift_paper_data"), $this->arrSelfInfo['priority']);
        $objHelperPlugin->addAction("LC_Page_Admin_Order_Mail_action_before", array($this, "get_gift_paper_data"), $this->arrSelfInfo['priority']);
        $objHelperPlugin->addAction("LC_Page_Admin_Order_MailView_action_before", array($this, "get_gift_paper_data"), $this->arrSelfInfo['priority']);

        $objHelperPlugin->addAction("LC_Page_Admin_Products_Product_action_after", array($this, "products_product_after"), $this->arrSelfInfo['priority']);
        $objHelperPlugin->addAction("LC_Page_Admin_Order_Disp_action_after", array($this, "order_disp_after"), $this->arrSelfInfo['priority']);
        $objHelperPlugin->addAction("LC_Page_Admin_Order_Edit_action_after", array($this, "order_edit_after"), $this->arrSelfInfo['priority']);
        $objHelperPlugin->addAction("LC_Page_Shopping_Payment_action_after", array($this, "shopping_payment_after"), $this->arrSelfInfo['priority']);
        $objHelperPlugin->addAction("LC_Page_Shopping_Payment_action_confirm", array($this, "shopping_payment_confirm"), $this->arrSelfInfo['priority']);

        $objHelperPlugin->addAction("prefilterTransform", array(&$this, "prefilterTransform"), $this->arrSelfInfo['priority']);
        $objHelperPlugin->addAction("loadClassFileChange", array(&$this, "loadClassFileChange"), $this->arrSelfInfo['priority']);
    }

    /**
     * カート内の商品IDを取得する.
     *
     * @param integer $productTypeId 商品種別ID
     * @return array カート内の商品IDの配列
     */
    function getCartProductID($productTypeId) {
        $objCartSess = new SC_CartSession_Ex();
        // カートキーの最大要素番号を取得する
        $max = $objCartSess->getMax($productTypeId);
        // 全てのカートに含まれている商品IDを取得する
        for($i = 0; $i <= $max; $i++) {
            if($objCartSess->cartSession[$productTypeId][$i]['cart_no'] != "") {
                $arrRet[] = $objCartSess->cartSession[$productTypeId][$i]['productsClass']['product_id'];
            }
        }
        return $arrRet;
    }

    /**
     * 指定されたIDの商品にのし選択可能商品が
     * 存在するかで「のし選択」項目の表示有無を決定する.
     *
     * @param object $product_id 商品ID情報
     * @return boolean のし選択可能商品有無(true：のし選択可能商品がある false：のし選択可能商品が無し)
     */
    function chkShowGiftPaper($product_id) {

        $table = "dtb_products";
        $where = "plg_giftpaper_noshi = 1 AND product_id ";
        if(is_array($product_id)) {
            $first = true;
            $where .= "IN(";
            foreach($product_id as $val) {
                if(!$first) {
                    $where .= ",";
                } else {
                    $first = false;
                }
                $where .= "?";
                $arrParam[] = $val;
            }
            $where .= ")";
        } else {
            // 商品IDが0の場合、falseで終了
            if($product_id == 0) return false;

            $where .= "= ?";
            $arrParam[] = $product_id;
        }

        // のし選択可能商品の件数を取得する
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $cnt = $objQuery->count($table, $where, $arrParam);

        return $cnt > 0;
    }

    function loadClassFileChange(&$classname, &$classpath) {
        if($classname == "SC_Helper_Mail_Ex") {
            $classpath = PLUGIN_UPLOAD_REALDIR . "GiftPaper/plg_GiftPaper_SC_Helper_Mail.php";
            $classname = "plg_GiftPaper_SC_Helper_Mail";
        }
    }
}
?>
