<?php
class plugin_info{
    /** プラグインコード(必須)：プラグインを識別する為キーで、他のプラグインと重複しない一意な値である必要がありま. */
    static $PLUGIN_CODE       = "Status4Bloc";
    /** プラグイン名(必須)：EC-CUBE上で表示されるプラグイン名. */
    static $PLUGIN_NAME       = "商品ステータス「オススメ」商品をまとめブロック表示プラグイン";
    /** クラス名(必須)：プラグインのクラス（拡張子は含まない） */
    static $CLASS_NAME        = "Status4Bloc";
    /** プラグインバージョン(必須)：プラグインのバージョン. */
    static $PLUGIN_VERSION    = "0.1";
    /** 対応バージョン(必須)：対応するEC-CUBEバージョン. */
    static $COMPLIANT_VERSION = "2.12.2";
    /** 作者(必須)：プラグイン作者. */
    static $AUTHOR            = "日本電子工藝社";
    /** 説明(必須)：プラグインの説明. */
    static $DESCRIPTION       = "商品登録画面の商品ステータスの「オススメ」にチェックをすると、チェックした商品郡をブロックとして表示することができます。
トップページ等のランディングページに対して、簡単に「オススメ商品」を訴求することが可能になります。";
    /** プラグインURL：プラグイン毎に設定出来るURL（説明ページなど） */
    static $PLUGIN_SITE_URL   = "http://ec-custom.com/";
    /** ライセンス */
    static $LICENSE        = "LGPL";
}
?>