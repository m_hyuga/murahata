<?php
require_once CLASS_REALDIR . 'pages/frontparts/bloc/LC_Page_FrontParts_Bloc.php';
class LC_Page_FrontParts_Bloc_Plg_Status4Bloc extends LC_Page_FrontParts_Bloc {
    function init() {
        parent::init();
    }
    function process() {
		$this->tpl_mainpage = 'frontparts/bloc/plg_Status4Bloc.tpl';
        $this->action();
        $this->sendResponse();
    }
    function action() {
        $objSiteInfo = SC_Helper_DB_Ex::sfGetBasisData();
        $this->arrInfo = $objSiteInfo->data;
		$status = 4;
        $this->arrStatusBloc = $this->lfGetstatusProduct($status);
    }
    function destroy() {
        parent::destroy();
    }
    function lfGetstatusProduct($status) {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
		if(preg_match('/^[0-9]+$/',ECP_STATUS_NO4_SHOW_NUM)){
			$limit = ECP_STATUS_NO4_SHOW_NUM;
		}else{
			$limit = 8;
		}
		$query = "
		SELECT
			*
		FROM
		(
			SELECT
				product_id
			FROM
				dtb_product_status
			WHERE
				product_status_id = ?
			ORDER BY RAND()
		) s INNER JOIN
		dtb_products p USING(product_id)
		WHERE
			p.del_flg = 0 AND 
			p.status = 1
		LIMIT 8
		";
		$arrproduct = $objQuery->getAll($query,
										array(
											$status,
											$limit
										)
									);
		foreach($arrproduct as $key=> $value){
			$arrProductId[] = $value["product_id"];
		}
		
		
        $objProduct = new SC_Product_Ex();
		$arrProductList = $objProduct->getListByProductIds($objQuery, $arrProductId);
		return $arrProductList;

    }
}
