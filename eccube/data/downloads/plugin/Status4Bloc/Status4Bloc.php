<?php

class Status4Bloc extends SC_Plugin_Base {


    public function __construct(array $arrSelfInfo) {
        parent::__construct($arrSelfInfo);
    }

    function install($arrPlugin) {
        Status4Bloc::insertBloc($arrPlugin);
        copy(PLUGIN_UPLOAD_REALDIR . "Status4Bloc/logo.png", PLUGIN_HTML_REALDIR . "Status4Bloc/logo.png");
        copy(PLUGIN_UPLOAD_REALDIR . "Status4Bloc/templates/plg_Status4Bloc.tpl", TEMPLATE_REALDIR . "frontparts/bloc/plg_Status4Bloc.tpl");
        copy(PLUGIN_UPLOAD_REALDIR . "Status4Bloc/bloc/plg_Status4Bloc.php", HTML_REALDIR . "frontparts/bloc/plg_Status4Bloc.php");
        mkdir(PLUGIN_HTML_REALDIR . "Status4Bloc/media");
        $objQuery = SC_Query_Ex::getSingletonInstance();
		
		$query = 'INSERT IGNORE INTO mtb_constants SET id = ?,name=8,rank = 103,remarks=?;';
		$objQuery->query($query,array('ECP_STATUS_NO4_SHOW_NUM','statusが「オススメ 」の商品表示件数'));

        $masterData = new SC_DB_MasterData_Ex();
        // キャッシュを生成
        $masterData->createCache('mtb_constants', array(), true, array('id', 'remarks'));
        SC_Utils_Ex::sfCopyDir(PLUGIN_UPLOAD_REALDIR . "Status4Bloc/media/", PLUGIN_HTML_REALDIR . "Status4Bloc/media/");
    }

    function uninstall($arrPlugin) {
        $objQuery = SC_Query_Ex::getSingletonInstance();
        $arrBlocId = $objQuery->getCol('bloc_id', "dtb_bloc", "device_type_id = ? AND filename = ?", array(DEVICE_TYPE_PC , "plg_Status4Bloc"));
        $bloc_id = (int) $arrBlocId[0];
        $where = "bloc_id = ?";
        $objQuery->delete("dtb_bloc", $where, array($bloc_id));
        $objQuery->delete("dtb_blocposition", $where, array($bloc_id));
        SC_Helper_FileManager_Ex::deleteFile(TEMPLATE_REALDIR . "frontparts/bloc/plg_Status4Bloc.tpl");
        SC_Helper_FileManager_Ex::deleteFile(HTML_REALDIR  . "frontparts/bloc/plg_Status4Bloc.php");
        SC_Helper_FileManager_Ex::deleteFile(PLUGIN_HTML_REALDIR . "Status4Bloc/logo.png");
        SC_Helper_FileManager_Ex::deleteFile(PLUGIN_HTML_REALDIR . "Status4Bloc");
		
    }
    function enable($arrPlugin) {
        // nop
    }
    function insertBloc($arrPlugin) {
        $objQuery = SC_Query_Ex::getSingletonInstance();
        $sqlval_bloc = array();
        $sqlval_bloc['device_type_id'] = DEVICE_TYPE_PC;
        $sqlval_bloc['bloc_id'] = $objQuery->max('bloc_id', "dtb_bloc", "device_type_id = " . DEVICE_TYPE_PC) + 1;
        $sqlval_bloc['bloc_name'] = $arrPlugin['plugin_name'];
        $sqlval_bloc['tpl_path'] = "plg_Status4Bloc.tpl";
        $sqlval_bloc['filename'] = "plg_Status4Bloc";
        $sqlval_bloc['create_date'] = "CURRENT_TIMESTAMP";
        $sqlval_bloc['update_date'] = "CURRENT_TIMESTAMP";
        $sqlval_bloc['php_path'] = "frontparts/bloc/plg_Status4Bloc.php";
        $sqlval_bloc['deletable_flg'] = 0;
        $sqlval_bloc['plugin_id'] = $arrPlugin['plugin_id'];
        $objQuery->insert("dtb_bloc", $sqlval_bloc);
    }
    function disable($arrPlugin) {
        // nop
    }
}

