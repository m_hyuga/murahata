<?php
require_once(MODULE_REALDIR . 'mdl_sln/defines.php');
require_once(SLN_CLASS_PATH . "SLN_Config.php");

/* 管理設定
 *
* @package スマートリンクネットワーク決済モジュール
* @author SmartLink Network, Inc.
* @version 1.1.0
*/
$objectPage = new SLN_Config();
$objectPage->init();
$objectPage->process();