<?php
require_once(MODULE_REALDIR . 'mdl_sln/defines.php');

/**
 * SLN_C_Baseクラス（基底）
 *
 * @package スマートリンクネットワーク決済モジュール
 * @author SmartLink Network, Inc.
 * @version 1.1.0
*/
class SLN_C_Base {

	var $arrError = array();
	var $request_error = null;
	var $netError = false;
	var $arrResults = null;

	function setError($msg = false) {
		if ($msg === false) {
			$this->arrError = array();
		} else {
			$this->arrError[] = $msg;
		}
	}

	function getError() {
		return $this->arrError;
	}

	function setResults($arrResults) {
		$this->arrResults = $arrResults;
	}

	function getResults() {
		if (SC_Utils::isBlank($this->arrResults[0]) && !SC_Utils::isBlank($this->arrResults)) {
			return $this->arrResults;
		}
		return $this->arrResults[0];
	}

	function getSendData($sendKeyHash, $orderHash, $paramHash, $slnSettingHash) {
		$sendDataHash = array();
		foreach ($sendKeyHash as $key) {
			switch ($key) {
				case 'MerchantId':
					// 貴社を識別するためのID
					$sendDataHash[$key] = $slnSettingHash['MerchantId'];
					break;
				case 'MerchantPass':
					// 貴社を識別するためのパスワード
					$sendDataHash[$key] = $slnSettingHash['MerchantPass'];
					break;
				case 'TenantId':
					// テナント
					if (!SC_Utils::isBlank($slnSettingHash['TenantId'])) {
						$sendDataHash[$key] = $slnSettingHash['TenantId'];
					}
					break;
				case 'CardExp':
					// クレジットカードの有効期限
					$sendDataHash[$key] = $paramHash['CardExpYear'] . $paramHash['CardExpMonth'];
					break;
				case 'PayType':
					// 支払方法を識別するためのコード
					$sendDataHash[$key] = $paramHash['PayType'];
					break;
				case 'TransactionDate':	// 処理日付
				case 'SalesDate':	// カード利用明細に表示する利用日
					$sendDataHash[$key] = date("Ymd");
					break;
				case 'MerchantFree1':	// EC-CUBEの注文番号を入れる
					$sendDataHash[$key] = $orderHash['order_id'];
					break;
				case 'NameKanji':
					// ユーザ漢字氏名
					$sendDataHash[$key] = mb_substr(mb_convert_kana($orderHash['order_name01'] . $orderHash['order_name02'], 'KVSA', 'UTF-8'),0,20);
					break;
				case 'NameKana':
					// ユーザカナ氏名
					$sendDataHash[$key] = mb_substr(mb_convert_kana($orderHash['order_kana01'] . $orderHash['order_kana02'], 'KVSA', 'UTF-8'),0,20);;
					break;
				case 'TelNo':	// 電話番号
					// クレジットカードの場合は下4ケタ、オンライン収納の場合はハイフン付全部
					if (!empty($paramHash[$key])) {
						$sendDataHash[$key] = $paramHash[$key];
					} else {
						// 4桁-4桁-4桁はフォーマットエラーになる。その場合は最後の番号を消す
						if (strlen($orderHash['order_tel01'].$orderHash['order_tel02'].$orderHash['order_tel03']) >= 12) {
							$orderHash['order_tel03'] = substr($orderHash['order_tel03'],0,3);
						}
						$sendDataHash[$key] = $orderHash['order_tel01'] . '-' . $orderHash['order_tel02'] . '-' . $orderHash['order_tel03'];
					}
					break;
				case 'ReturnURL':	// 戻り先URL
					$sendDataHash[$key] = rtrim(HTTP_URL, '/') . SHOPPING_COMPLETE_URLPATH;
					if (SC_Display::detectDevice() === DEVICE_TYPE_MOBILE) {
						$sendDataHash[$key] .= '?' . ini_get("session.name") . '=' . session_id();
					}
					$sendDataHash[$key] = substr($sendDataHash[$key],0,128);
					break;
				case 'PayLimit':	// 支払期限
					$sendDataHash[$key] = date("YmdHi", strtotime("+7 day"));
					break;
				case 'Amount':	// 支払金額
					$sendDataHash[$key] = $orderHash['payment_total'];
					break;
				case 'ShouhinName':	// 商品名
					$sendDataHash[$key] = SLN_Util::getShouhinName($orderHash['order_id']);
					break;
				case 'KanaSei':
				case 'KanaMei':
					$sendDataHash[$key] = mb_convert_kana($paramHash[$key], 'kha', 'UTF-8');
					break;
				case 'KaiinId':
					if (!SC_Utils::isBlank($orderHash['customer_id']) && $orderHash['customer_id'] != '0' ) {
						$sendDataHash[$key] = sprintf("%013d",$orderHash['customer_id']);
					}
					break;
				case 'KaiinPass':
					if (!SC_Utils::isBlank($orderHash['customer_id']) && $orderHash['customer_id'] != '0' ) {
						$sendDataHash[$key] = substr(ereg_replace("[^0-9a-zA-Z]","",md5($orderHash['customer_id'] . AUTH_MAGIC)),0,12);
					}
					break;
				case 'SecCd':
					$sendDataHash[$key] = $paramHash['SecCd'];
					break;
				case 'Free1':
				case 'Free2':
				case 'Free3':
				case 'Free4':
				case 'Free5':
				case 'Free6':
				case 'Free7':
				case 'Comment':	// ご案内1
				case 'Free8':
				case 'Free9':
				case 'Free10':
				case 'Free11':
				case 'Free12':
				case 'Free13':
				case 'Free14':
				case 'Free15':
				case 'Free16':
					$sendDataHash[$key] = mb_convert_kana($slnSettingHash[$key], 'ASK', 'UTF-8');
					break;
					// あとは変数名中にあればそのまま使う
				default:
					if (isset($paramHash[$key])) {
						$sendDataHash[$key] = $paramHash[$key];
					} elseif (isset($orderHash[$key])) {
						$sendDataHash[$key] = $orderHash[$key];
					} elseif (isset($slnSettingHash[$key])) {
						$sendDataHash[$key] = $slnSettingHash[$key];
					}
			}
		}
		return $sendDataHash;
	}

	function orderRequest($url, $sendKeyHash, $orderHash, $paramHash, $slnSettingHash) {

		$orderHash = SLN_Util::setOrderIdToHash($orderHash);
		$sendDataHash = $this->getSendData($sendKeyHash, $orderHash, $paramHash, $slnSettingHash);

		$requestParamHash = $paramHash;
		$ret = $this->accessSlnServer($url, $sendDataHash);
		if ($ret) {
			$paramHash = $this->getResults();
		} else {
			$paramHash = array();
			$paramHash['request_error'] = $this->request_error;
		}

		$paramHash['OrderID'] = $orderHash['OrderID'];
		$paramHash['Amount'] = $orderHash['payment_total'];

		if (!SC_Utils::isBlank($requestParamHash['CardSeq'])) {
			$paramHash['CardSeq'] = $requestParamHash['CardSeq'];
		}

		if (!SC_Utils::isBlank($requestParamHash['pay_status'])) {
			$paramHash['pay_status'] = $requestParamHash['pay_status'];
		}
		if (!SC_Utils::isBlank($requestParamHash['success_pay_status']) && SC_Utils::isBlank($this->getError())) {
			$paramHash['pay_status'] = $requestParamHash['success_pay_status'];
		}else if (!SC_Utils::isBlank($requestParamHash['fail_pay_status']) && !SC_Utils::isBlank($this->getError())) {
			$paramHash['pay_status'] = $requestParamHash['fail_pay_status'];
		} elseif ($this->netError) {
			$paramHash['pay_status'] = SLN_PAY_STATUS_FAIL;
		}

		SLN_Util::setOrderPayHash($orderHash, $paramHash);

		if (!SC_Utils::isBlank($this->getError())) {
			return false;
		}

		return true;
	}

	function accessSlnServer($url, $sendDataHash, $admin = false) {

		// EC-CUBE折り返し処理
		if ($url == "") {
			$arrReturn = array();
			$arrReturn['ResponseCd'] = 'OK';
			$this->setResults($arrReturn);
			return true;
		}

		$objectReq = new HTTP_Request($url);
		$objectReq->setMethod(HTTP_REQUEST_METHOD_POST);
		foreach ($sendDataHash as $key => $value) {
			$objectReq->addPostData($key, $value);
		}

		$ret = $objectReq->sendRequest();
		if (PEAR::isError($ret)) {
			$msg = '通信エラーが発生しました。:' . $ret->getMessage();
			$this->setError($msg);
			$this->netError = true;
			return false;
		}

		// レスポンスコード一覧
		// http://www.faqs.org/rfcs/rfc2616.html
		$httpcode = $objectReq->getResponseCode();
		if ($httpcode != 200) {
			$msg = '決済サーバーエラーが発生しました。:HTTPレスポンスコード:(' . $httpcode . ')';
			$this->setError($msg);
			$this->netError = true;
			return false;
		}

		$responsebody = $objectReq->getResponseBody();

		if (SC_Utils::isBlank($responsebody)) {
			$msg = 'レスポンスデータエラーが発生しました。: レスポンスが空です。';
			$this->setError($msg);
			return false;
		}

		$arrReturn = $this->parseResponse($responsebody, $admin);

		$this->setResults($arrReturn);
		if (!SC_Utils::isBlank($this->getError())) {
			return false;
		}

		return true;
	}

	function parseResponse($string, $admin = false) {
		$arrReturn = array();
		$arrTmpAnd = explode('&', trim($string));
		foreach($arrTmpAnd as $str) {
			list($key, $val) = explode('=', $str);
			$arrReturn[$key] = trim($val);
		}
		if (isset($arrReturn['ResponseCd'])  && ($arrReturn['ResponseCd'] != 'OK')) {
			$this->setError($this->createErrCode($arrReturn['ResponseCd'], $admin));
			$this->request_error = $this->createErrCode($arrReturn['ResponseCd'], true);
		}
		return $arrReturn;
	}

	function createErrCode($codes, $admin = false) {
		require_once(SLN_CLASS_PATH . 'GetErrorMsg.php');

		$errorObjectMsg = new GetErrorMsg();
		$codeArray = array();
		$arrTmpl = explode('|', $codes);
		$max = count($arrTmpl);
		for($i = 0; $i < $max; $i++) {
			$codeArray[] = trim($arrTmpl[$i]);
		}

		// 表示するのは１つのみ
		$code = $codeArray[0];
		$arrErrMsg = $errorObjectMsg->getErrorMessageByCode($code) ;
		if ($admin) return $arrErrMsg['adminmessage'];
		return $arrErrMsg['message'];
	}
}