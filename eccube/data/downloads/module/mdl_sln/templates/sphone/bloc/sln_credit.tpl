<script type="text/javascript">//<![CDATA[
var isSubmitExec = true;

function slnCheckSubmit(sub_mode) {
    $('#sln_form_body').toggle();
    $('#sln_form_loading').toggle();

    if(isSubmitExec) {
        isSubmitExec = false;
        fnModeSubmit(sub_mode,'','');
        return false;
    } else {
        alert("決済処理中です。しばらくお待ち下さい。");
        return false;
    }
}
//]]>
</script>
<section id="sln_form_loading"
	style="<!--{if !$tpl_is_loding}-->display:none;<!--{/if}-->">
	<div class="information end">
		<p>決済処理中です。しばらくお待ち下さい。</p>
	</div>
</section>
	<section id="sln_form_body"
		style="<!--{if $tpl_is_loding}-->display:none;<!--{/if}-->">
		<div class="information end">
			<p>
				下記項目にクレジットカード情報をご入力下さい。<br />
				「<span class="attention">※</span>」印は入力必須項目です。<br />
				入力後、画面下部の「購入」ボタンをクリックして下さい。
			</p>
			<!--{assign var=key_name value="payment"}-->
			<p class="attention">
				<!--{$arrErr[$key_name]}-->
			</p>
		</div>
		<h3 class="subtitle"><!--{$tpl_title|h}-->入力</h3>
		<dl class="form_entry">
			<dt>
				カード番号<span class="attention">※</span>
			</dt>
			<dd>
				<!--{assign var=key_name value="CardNo"}-->
				<!--{if $arrErr[$key_name]}-->
				<span class="attentionSt"> <!--{$arrErr[$key_name]}-->
				</span>
				<!--{/if}-->
				<input type="text" name="<!--{$key_name}-->"
					maxlength="<!--{$arrForm[$key_name].length}-->"
					style="ime-mode: disabled; <!--{$arrErr[$key_name]|sfGetErrorColor}-->"
					size="16" class="box120" />
				<p class="mini">
					<span class="attention">ハイフンは間に入れず、番号のみを入力してください。</span>
				</p>
			</dd>

			<dt>
				カード有効期限<span class="attention">※</span>
			</dt>
			<dd>

				<!--{assign var=key_name_m value="CardExpMonth"}-->
				<!--{assign var=key_name_y value="CardExpYear"}-->
				<!--{if $arrErr[$key_name_m]}-->
				<span class="attentionSt"> <!--{$arrErr[$key_name_m]}-->
				</span>
				<!--{/if}-->
				<!--{if $arrErr[$key_name_y]}-->
				<span class="attentionSt"> <!--{$arrErr[$key_name_y]}-->
				</span>
				<!--{/if}-->
				<select name="<!--{$key_name_m}-->"
					style="<!--{$arrErr[$key_name_m]|sfGetErrorColor}-->"
					class="boxShort data-role-none">
					<option value="">&minus;&minus;</option>
					<!--{html_options options=$arrMonth}-->
				</select>月 &nbsp;/&nbsp; 20<select name="<!--{$key_name_y}-->"
					style="<!--{$arrErr[$key_name_y]|sfGetErrorColor}-->"
					class="boxShort data-role-none">
					<option value="">&minus;&minus;</option>
					<!--{html_options options=$arrYear}-->
				</select>年
			</dd>


			<!--{if in_array('KanaSei',$arrConfigs.attestation_assistance)}-->
			<dt>
				カード名義<span class="attention">※</span>
			</dt>
			<dd>
				<!--{assign var=key_name_s value="KanaSei"}-->
				<!--{assign var=key_name_m value="KanaMei"}-->
				<!--{if $arrErr[$key_name_s]}-->
				<span class="attentionSt"> <!--{$arrErr[$key_name_s]}-->
				</span>
				<!--{/if}-->
				<!--{if $arrErr[$key_name_m]}-->
				<span class="attentionSt"> <!--{$arrErr[$key_name_m]}-->
				</span>
				<!--{/if}-->
				姓:<input type="text" name="<!--{$key_name_s}-->"
					maxlength="<!--{$arrForm[$key_name_s].length}-->"
					style="<!--{$arrErr[$key_name_s]|sfGetErrorColor}-->" size="20"
					class="box120" /> &nbsp; 名:<input type="text" name="<!--{$key_name_m}-->"
					maxlength="<!--{$arrForm[$key_name_m].length}-->"
					style="<!--{$arrErr[$key_name_m]|sfGetErrorColor}-->" size="20"
					class="box120" />
				<p class="mini">
					<span class="attention">カードに記載の名前をご記入下さい。ご本人名義のカードをご使用下さい。</span><br />全角カナ文字入力（例：ヤマダ
					タロウ）
				</p>
			</dd>
			<!--{/if}-->


			<!--{if $arrConfigs.SecCd == '1'}-->
			<dt>
				セキュリティコード<span class="attention">※</span>
			</dt>
			<dd>

				<!--{assign var=key_name value="SecCd"}-->
				<!--{if $arrErr[$key_name]}-->
				<span class="attentionSt"> <!--{$arrErr[$key_name]}-->
				</span>
				<!--{/if}-->
				<input type="password" name="<!--{$key_name}-->"
					maxlength="<!--{$arrForm[$key_name].length}-->"
					style="ime-mode: disabled; <!--{$arrErr[$key_name]|sfGetErrorColor}-->"
					size="4" class="box60" />
				<p class="mini">
					<span class="attention">※カード裏面の署名欄(AMEXは除く）に記載されている末尾３桁～４桁の数字をご記入下さい。<br />※AMEXは表面にあります。(例: 1234)</span><br />半角入力
					(例: 123)
				</p>
			</dd>
			<!--{/if}-->

			<!--{if in_array('BirthDay',$arrConfigs.attestation_assistance)}-->
			<dt>
				生月日<span class="attention">※</span>
			</dt>
			<dd>
				<!--{assign var=key_name value="BirthDay"}-->
				<!--{if $arrErr[$key_name]}-->
				<span class="attentionSt"> <!--{$arrErr[$key_name]}-->
				</span>
				<!--{/if}-->
				<input type="text" name="<!--{$key_name}-->"
					maxlength="<!--{$arrForm[$key_name].length}-->"
					style="ime-mode: disabled; <!--{$arrErr[$key_name]|sfGetErrorColor}-->"
					size="4" class="box60" />
				<p class="mini">
					<span class="attention">※ご本人の誕生日の月日をご記入下さい(年は必要ありません)。</span>半角入力
					(例: 0430)
			</dd>
			<!--{/if}-->

			<!--{if in_array('TelNo',$arrConfigs.attestation_assistance)}-->
			<dt>
				電話番号(下4桁)<span class="attention">※</span>
			</dt>
			<dd>
				<!--{assign var=key_name value="TelNo"}-->
				<!--{if $arrErr[$key_name]}-->
				<span class="attentionSt"> <!--{$arrErr[$key_name]}-->
				</span>
				<!--{/if}-->
				<input type="text" name="<!--{$key_name}-->"
					maxlength="<!--{$arrForm[$key_name].length}-->"
					style="ime-mode: disabled; <!--{$arrErr[$key_name]|sfGetErrorColor}-->"
					size="4" class="box60" />
				<p class="mini">
					<span class="attention">※カード会社に登録していますご本人の電話番号下4桁をご記入下さい。</span>半角入力
					(例: 9876)

			</dd>
			<!--{/if}-->

			<dt>
				支払い方法<span class="attention">※</span>
			</dt>
			<dd>

				<!--{assign var=key_name value="PayType"}-->
				<!--{if $arrErr[$key_name]}-->
				<span class="attentionSt"> <!--{$arrErr[$key_name]}-->
				</span>
				<!--{/if}-->
				<select name="<!--{$key_name}-->"
					style="<!--{$arrErr[$key_name]|sfGetErrorColor}-->"
					class="data-role-none">
					<!--{html_options options=$arrPayMethod selected=$arrForm[$key_name].value|default:''}-->
				</select>
			</dd>
			<!--{if $paymentHash.regist_credit && $tpl_regist_card_form && $arrConfigs.member_regist == '1' && $arrConfigs.quick_accounts == '1'}-->

			<dt>カード情報登録</dt>
			<dd>
				<!--{assign var=key_name value="register_card"}-->
				<!--{if $arrErr[$key_name]}-->
				<span class="attentionSt"> <!--{$arrErr[$key_name]}-->
				</span>
				<!--{/if}-->
				<input type="checkbox" class="data-role-none radio_btn "
					name="<!--{$key_name}-->" value="1"

				<!--{if $arrForm[$key_name].value != ""}-->
				checked
				<!--{/if}-->
				> <label for="<!--{$key_name}-->"><span class="fb">このカードを登録する。</span> </label>
				<p class="mini">
					カード情報を登録すると次回より入力無しで購入出来ます。<br />カード情報は当店では保管いたしません。<br />決済代行会社のサービスを利用して安全に保管されます。
				</p>
			</dd>
			<!--{/if}-->
		</dl>

		<div class="btn_area">
			<ul class="btn_btm">
				<li>
					<a rel="external"
					href="javascript:void(slnCheckSubmit('next'));" class="btn" />購入</a>
				</li>
				<!--{if !$tpl_btn_next}-->
				<li><a rel="external"
					href="javascript:void(slnCheckSubmit('return'));" class="btn_back" />戻る</a>
				</li>
				<!--{/if}-->
			</ul>
		</div>
	</section>
