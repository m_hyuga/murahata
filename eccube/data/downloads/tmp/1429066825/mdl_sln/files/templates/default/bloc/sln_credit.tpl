<script type="text/javascript">//<![CDATA[
var isSubmitExec = true;

function slnCheckSubmit(mode) {
    $('#sln_form_body').slideToggle();
    $('#sln_form_loading').slideToggle();

    if(isSubmitExec) {
        isSubmitExec = false;
        fnModeSubmit(mode,'','');
        return false;
    } else {
        alert("決済処理中です。しばらくお待ち下さい。");
        return false;
    }
}
$(function() {
            <!--{$tpl_payment_onload}-->
});
//]]>
</script>
<div id="sln_form_loading"
	style="<!--{if !$tpl_is_loding}-->display:none;<!--{/if}-->">
	<div class="information">
		<p>決済処理中です。しばらくお待ち下さい。</p>
	</div>
</div>
<div id="sln_form_body"
	style="<!--{if $tpl_is_loding}-->display:none;<!--{/if}-->">
	<div class="information">
		<p>
			下記項目にクレジットカード情報をご入力下さい。<br />
			「<span class="attention">※</span>」印は入力必須項目です。<br />
			入力後、画面下部の「購入」ボタンをクリックして下さい。
		</p>
		<!--{assign var=key value="payment"}-->
		<p class="attention">
			<!--{$arrErr[$key]}-->
		</p>
	</div>
	<table summary="クレジットカード入力">
		<tr>
			<th class="alignR" style="width: 20%;">カード番号<span class="attention">※</span>
			</th>
			<td style="width: 80%;">
				<!--{assign var=key1 value="CardNo"}--> <!--{if $arrErr[$key1]}--> <span
				class="attentionSt"> <!--{$arrErr[$key1]}-->
			</span> <!--{/if}--> <input type="text" name="<!--{$key1}-->"
				value="" maxlength="<!--{$arrForm[$key1].length}-->"
				style="ime-mode: disabled; <!--{$arrErr[$key1]|sfGetErrorColor}-->"
				size="16" class="box150" />
				<p class="mini">
					<span class="attention">ハイフンは間に入れず、番号のみを入力してください。</span>
				</p>
			</td>
		</tr>
		<tr>
			<th class="alignR">カード有効期限<span class="attention">※</span>
			</th>
			<td>
				<!--{assign var=key_exp_m value="CardExpMonth"}--> <!--{assign var=key_exp_y value="CardExpYear"}-->
				<!--{if $arrErr[$key_exp_m]}--> <span class="attentionSt"> <!--{$arrErr[$key_exp_m]}-->
			</span> <!--{/if}--> <!--{if $arrErr[$key_exp_y]}--> <span
				class="attentionSt"> <!--{$arrErr[$key_exp_y]}-->
			</span> <!--{/if}--> <select name="<!--{$key_exp_m}-->"
				style="<!--{$arrErr[$key_exp_m]|sfGetErrorColor}-->">
					<option value="">&#45;&#45;</option>
					<!--{html_options options=$arrMonth}-->
			</select>月 &nbsp;/&nbsp; 20<select name="<!--{$key_exp_y}-->"
				style="<!--{$arrErr[$key_exp_y]|sfGetErrorColor}-->">
					<option value="">&#45;&#45;</option>
					<!--{html_options options=$arrYear}-->
			</select>年
			</td>
		</tr>

		<!--{if in_array('KanaSei',$arrConfigs.attestation_assistance)}-->
		<tr>
			<th class="alignR">カード名義<span class="attention">※</span>
			</th>
			<td>
				<!--{assign var=key1 value="KanaSei"}--> <!--{assign var=key2 value="KanaMei"}-->
				<!--{if $arrErr[$key1]}--> <span class="attentionSt"> <!--{$arrErr[$key1]}-->
			</span> <!--{/if}--> <!--{if $arrErr[$key2]}--> <span
				class="attentionSt"> <!--{$arrErr[$key2]}-->
			</span> <!--{/if}--> 姓:<input type="text" name="<!--{$key1}-->"
				maxlength="<!--{$arrForm[$key1].length}-->"
				style="<!--{$arrErr[$key1]|sfGetErrorColor}-->" size="20"
				class="box120" /> &nbsp; 名:<input type="text" name="<!--{$key2}-->"
				maxlength="<!--{$arrForm[$key2].length}-->"
				style="<!--{$arrErr[$key2]|sfGetErrorColor}-->" size="20"
				class="box120" />&nbsp;全角カナ文字入力（例：ヤマダ タロウ）
				<p class="mini">
					<span class="attention">ご本人名義のカードをご使用下さい。</span>
				</p>
			</td>
		</tr>
		<!--{/if}-->

		<!--{if $arrConfigs.SecCd == '1'}-->
		<tr>
			<th class="alignR">セキュリティコード<span class="attention">※</span>
			</th>
			<td>
				<!--{assign var=key value="SecCd"}--> <!--{if $arrErr[$key]}--> <span
				class="attentionSt"> <!--{$arrErr[$key]}-->
			</span> <!--{/if}--> <input type="password" name="<!--{$key}-->"
				maxlength="<!--{$arrForm[$key].length}-->"
				style="ime-mode: disabled; <!--{$arrErr[$key]|sfGetErrorColor}-->"
				size="4" class="box60" />&nbsp半角入力 (例: 123)
				<p class="mini">
					<span class="attention">※カード裏面の署名欄(AMEXは除く）に記載されている末尾３桁～４桁の数字をご記入下さい。<br />※AMEXは表面にあります。(例: 1234)</span>
					<br /> <img
						src="<!--{$smarty.const.PLUGIN_HTML_URLPATH}-->SLNCore/security_code.jpg"
						alt="セキュリティコード" width="410">
				</p>
			</td>
		</tr>
		<!--{/if}-->

		<!--{if in_array('BirthDay',$arrConfigs.attestation_assistance)}-->
		<tr>
			<th class="alignR">生月日<span class="attention">※</span>
			</th>
			<td>
				<!--{assign var=key value="BirthDay"}--> <!--{if $arrErr[$key]}--> <span
				class="attentionSt"> <!--{$arrErr[$key]}-->
			</span> <!--{/if}--> <input type="text" name="<!--{$key}-->"
				maxlength="<!--{$arrForm[$key].length}-->"
				style="ime-mode: disabled; <!--{$arrErr[$key]|sfGetErrorColor}-->"
				size="4" class="box60" />&nbsp半角入力 (例: 0430)
				<p class="mini">
					<span class="attention">※ご本人の誕生日の月日をご記入下さい(年は必要ありません)。</span>

			</td>
		</tr>
		<!--{/if}-->

		<!--{if in_array('TelNo',$arrConfigs.attestation_assistance)}-->
		<tr>
			<th class="alignR">電話番号(下4桁)<span class="attention">※</span>
			</th>
			<td>
				<!--{assign var=key value="TelNo"}--> <!--{if $arrErr[$key]}--> <span
				class="attentionSt"> <!--{$arrErr[$key]}-->
			</span> <!--{/if}--> <input type="text" name="<!--{$key}-->"
				maxlength="<!--{$arrForm[$key].length}-->"
				style="ime-mode: disabled; <!--{$arrErr[$key]|sfGetErrorColor}-->"
				size="4" class="box60" />&nbsp半角入力 (例: 9876)
				<p class="mini">
					<span class="attention">※カード会社に登録していますご本人の電話番号下4桁をご記入下さい。</span>

			</td>
		</tr>
		<!--{/if}-->

		<tr>
			<th class="alignR">支払い方法<span class="attention">※</span>
			</th>
			<td>
				<!--{assign var=key1 value="PayType"}--> <!--{if $arrErr[$key1]}-->
				<span class="attentionSt"> <!--{$arrErr[$key1]}-->
			</span> <!--{/if}--> <select name="<!--{$key1}-->"
				style="<!--{$arrErr[$key1]|sfGetErrorColor}-->">
					<!--{html_options options=$arrPayMethod selected=$arrForm[$key1].value|escape}-->
			</select>
			</td>
		</tr>
		<!--{if $paymentHash.regist_credit && $tpl_regist_card_form && $arrConfigs.member_regist == '1' && $arrConfigs.quick_accounts == '1'}-->
		<tr>
			<th class="alignR">カード情報登録</th>
			<td>
				<!--{assign var=key_regist value="register_card"}--> <!--{if $arrErr[$key_regist]}-->
				<span class="attentionSt"> <!--{$arrErr[$key_regist]}-->
			</span> <!--{/if}--> <!--{if !$tpl_pg_regist_card_force}--> <input
				type="checkbox" name="<!--{$key_regist}-->" value="1"
			<!--{if $arrForm[$key_regist].value != ""}-->checked<!--{/if}--> >
			 <label
				for="<!--{$key_regist}-->">このカードを登録する。</label> <!--{else}--> <input
				type="hidden" name="<!--{$key_regist}-->" value="1" /> 自動でカード登録します。 <!--{/if}-->
				<p class="mini">
					カード情報を登録すると次回より入力無しで購入出来ます。<br />カード情報は当店では保管いたしません。<br />決済代行会社のサービスを利用して安全に保管されます。
				</p>
			</td>
		</tr>
		<!--{/if}-->
	</table>

	<div class="information" style="border: 1px solid #CCCCCC; padding: 0.5em;">
		<p>以上の内容で間違いなければ、下記「購入」ボタンをクリックして下さい。<br />
		<span class="attention">※通信状況などにより画面が切り替るまで少々時間がかかる場合があります。</span>
		</p>
	</div>

	<div class="btn_area">
		<ul>
			<!--{if !$tpl_btn_next}-->
			<li><input type="image" onmouseover="chgImg('<!--{$TPL_URLPATH}-->img/button/btn_back_on.jpg',this)"
				onmouseout="chgImg('<!--{$TPL_URLPATH}-->img/button/btn_back.jpg',this)"
				onclick="return slnCheckSubmit('return');"
				src="<!--{$TPL_URLPATH}-->img/button/btn_back.jpg" alt="戻る"
				name="back" id="back" border="0" />
			</li>
			<!--{/if}-->
			<li>
				<!--{if $tpl_btn_next}--> <input type="image"
				onmouseover="chgImgImageSubmit('<!--{$smarty.const.ROOT_URLPATH}-->plugin/SLNCore/btn_bloc_buy_on.jpg',this)"
				onmouseout="chgImgImageSubmit('<!--{$smarty.const.ROOT_URLPATH}-->plugin/SLNCore/btn_bloc_buy.jpg',this)"
				onclick="return slnCheckSubmit('next');"
				src="<!--{$smarty.const.ROOT_URLPATH}-->plugin/SLNCore/btn_bloc_buy.jpg"
				alt="購入" name="next" id="next" border="0" /> <!--{else}--> <input
				type="image" onmouseover="chgImgImageSubmit('<!--{$smarty.const.ROOT_URLPATH}-->plugin/SLNCore/btn_bloc_buy_on.jpg',this)"
				onmouseout="chgImgImageSubmit('<!--{$smarty.const.ROOT_URLPATH}-->plugin/SLNCore/btn_bloc_buy.jpg',this)"
				onclick="return slnCheckSubmit('next');"
				src="<!--{$smarty.const.ROOT_URLPATH}-->plugin/SLNCore/btn_bloc_buy.jpg"
				alt="購入" name="next" id="next" border="0" /> <!--{/if}-->
			</li>
		</ul>
	</div>
</div>
