<?php
require_once(SLN_CLASS_PATH . 'SLN_C_RegistCredit.php');
require_once(SLN_CLASS_PATH . 'SLN_C_Member.php');
require_once(SLN_CLASS_PATH . 'SLN_C_Util.php');

/**
 * SLN_RegistCreditクラス（登録済みクレジットカードでの購入）
 *
 * @package スマートリンクネットワーク決済モジュール
 * @author SmartLink Network, Inc.
 * @version 1.1.0
*/
class SLN_RegistCredit {

	function initParam(&$objectFormParam, &$paymentHash, &$orderHash) {
		$objectFormParam->addParam("お支払い方法", "PayType", INT_LEN, 'n', array("MAX_LENGTH_CHECK", "GRAPH_CHECK"), "");
	}

	function getFormBloc() {
		$deviceTypeId = SC_Display::detectDevice();
		$objectMdl =& SLN::getInstance();
		$arrBlocId = $objectMdl->getSubData('bloc_setting');
		$blocId = $arrBlocId['sln_regist_credit'][ $deviceTypeId ];
		if (empty($blocId)) {
			return null;
		}
		$layout = new SC_Helper_PageLayout();
		$arrBloc = $layout->getBlocs($deviceTypeId, 'bloc_id = ?', array($blocId), true);
		return $arrBloc[0]['tpl_path'];
	}

	function updateOrderStatusNew($orderId, &$objectPurchase) {
		$objectQuery =& SC_Query::getSingletonInstance();
		$objectQuery->begin();
		$sqlval = array();
		$objectPurchase->sfUpdateOrderStatus($orderId, ORDER_NEW, null, null, $sqlval);
		$objectQuery->commit();
	}

	function nextAction(&$objectPage, &$objectPurchase, &$objectFormParam, &$orderHash)
	{
		$objectPage->arrErr = $objectFormParam->checkError();
		if (!SC_Utils::isBlank($objectPage->arrErr)) {
			return;
		}

		$objectClient = new SLN_C_RegistCredit();
		$result = $objectClient->requestPayment($orderHash, $objectFormParam->getHashArray());
		if (!$result) {
			$arrErr = $objectClient->getError();
			$objectPage->arrErr['payment'] = '※ 決済処理でエラーが発生しました。<br />' . implode('<br />', $arrErr);
			return;
		}

		$arrResults = $objectClient->getResults();
		$this->updateOrderStatusNew($orderHash['order_id'], $objectPurchase);
		$objectPurchase->sendOrderMail($orderHash['order_id']);
		$paramHash = SLN_Util::getOrderPayHash($orderHash['order_id']);
		$this->cardRegist($orderHash, $paramHash);
		SC_Response::sendRedirect(SHOPPING_COMPLETE_URLPATH);
		$objectPage->actionExit();
	}

	function loadAction(&$objectPage, &$orderHash)
	{
		$arrCustomer = SC_Helper_Customer::sfGetCustomerData($orderHash['customer_id']);
		$objectClientMember = new SLN_C_Member();
		$ret = $objectClientMember->searchCard($arrCustomer);
		if(!$ret || $objectClientMember->arrResults['KaiinStatus'] != 0) {
			$objectPage->arrErr['payment'] = '※ 登録済みクレジットカードが見つかりませんでした。';
			return;
		}

		$objectPage->dataHash = $objectClientMember->arrResults;
		$objectSess = new SC_Session();
		if ($objectSess->GetSession('plg_slnquick_payment_confirm')) {
			$objectSess->SetSession('plg_slnquick_payment_confirm', false);
			$objectPage->tpl_payment_onload = "isSubmitExec = false; fnModeSubmit('next','','');";
			$objectPage->tpl_is_loding = true;
		}else {
			$objectPage->tpl_is_loading = false;
		}
	}

	function returnAction(&$objectPurchase) {
		$objectPurchase->rollbackOrder($_SESSION['order_id'], ORDER_CANCEL, true);
		SC_Response::sendRedirect(SHOPPING_CONFIRM_URLPATH);
		SC_Response::actionExit();
	}

	function prepareObjectPage(&$objectPage)
	{
		$arrPayMethod = SLN_Util::getCreditPayMethod();
		$objectPage->arrPayMethod = array();
		foreach ($objectPage->arrConfigs['payKbnKaisu'] as $pay_method) {
			if(!SC_Utils::isBlank($arrPayMethod[$pay_method])) {
				$objectPage->arrPayMethod[$pay_method] = $arrPayMethod[$pay_method];
			}
		}
	}

	function actionByMode($mode, &$objectFormParam, &$orderHash, &$objectPage) {
		$this->prepareObjectPage($objectPage);
		$objectPurchase = new SC_Helper_Purchase();

		switch($mode) {
			case 'next':
				$this->nextAction($objectPage, $objectPurchase, $objectFormParam, $orderHash);
			case 'load':
				$this->loadAction($objectPage, $orderHash);
				break;
			case 'return':
				$this->returnAction($objectPurchase);
				break;
			default:
				$objectPage->tpl_payment_onload = "fnModeSubmit('load','','');";
				$objectPage->tpl_is_loding = true;
				break;
		}
	}

	function cardRegist($orderHash, $paramHash = array())
	{
		$arrCustomer = SC_Helper_Customer::sfGetCustomerData($orderHash['customer_id']);

		$objectClientMember = new SLN_C_Member();
		$ret = $objectClientMember->searchCard($arrCustomer);
		if (!$ret ||  $objectClientMember->arrResults['KaiinStatus'] != 0) {
			return;
		}

		$objectClient = new SLN_C_Util();
		$objectClient->exec4MemAdd($orderHash, $paramHash);
	}
}