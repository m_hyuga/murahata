<?php
require_once CLASS_EX_REALDIR . 'page_extends/mypage/LC_Page_AbstractMypage_Ex.php';
require_once(SLN_CLASS_PATH . 'SLN_C_Member.php');

/**
 * SLN_ChangeCardクラス（保存カード変更）
 *
 * @package スマートリンクネットワーク決済モジュール
 * @author SmartLink Network, Inc.
 * @version 1.1.0
*/
class SLN_ChangeCard extends LC_Page_AbstractMypage_Ex {

	function init() {
		parent::init();
		$this->tpl_subtitle = '登録済みカード情報変更';

		$objectDate = new SC_Date(date('Y'), date('Y') + 15);
		$this->arrYear = $objectDate->getZeroYear();
		$this->arrMonth = $objectDate->getZeroMonth();

		$this->objectMdl =& SLN::getInstance();
		$this->arrConfigs = $this->objectMdl->getConfigs();

		$this->httpCacheControl('nocache');
	}

	function process() {
		parent::process();
	}

	function action() {

		$objectCustomer		= new SC_Customer();
		$objectFormParam	= new SC_FormParam();
		$objectClient		= new SLN_C_Member();

		// 現在カード情報が保存されているか確認
		$beforeResults = array();
		$customer_id = $objectCustomer->getValue('customer_id');
		$arrCustomer = SC_Helper_Customer::sfGetCustomerData($customer_id);
		$ret = $objectClient->searchCard($arrCustomer);
		if ($ret) {
			$beforeResults = $objectClient->arrResults;
		}
		$objectClient->setError(false);
		switch ($this->getMode()) {
			case 'inval':
				$arrForm  = $objectFormParam->getHashArray();
				$ret = $objectClient->invalCard($arrCustomer); // 無効化
				if ($ret) {
					$this->success = true;
				} else {
					$arrErr = $objectClient->getError();
					$this->arrErr['error'] = '※ 登録クレジットカード無効化でエラーが発生しました。<br />' . implode('<br />', $arrErr);
				}
				$objectFormParam = new SC_FormParam();
				$this->registParam($objectFormParam);
				$this->arrForm = $objectFormParam->getFormParamList();
				break;
			case 'uninval':
				$arrForm  = $objectFormParam->getHashArray();
				$ret = $objectClient->unInvalCard($arrCustomer); // 無効化解除
				if ($ret) {
					$this->success = true;
				} else {
					$arrErr = $objectClient->getError();
					$this->arrErr['error'] = '※ 登録クレジットカード無効化解除でエラーが発生しました。<br />' . implode('<br />', $arrErr);
				}
				$objectFormParam = new SC_FormParam();
				$this->registParam($objectFormParam);
				$this->arrForm = $objectFormParam->getFormParamList();
				break;
			case 'regist':
				$this->registParam($objectFormParam);
				$objectFormParam->setParam($_POST);
				$this->arrErr = $objectFormParam->checkError();
				if (SC_Utils::isBlank($this->arrErr)) {
					// 会員無効化の場合は解除する
					if ($beforeResults['KaiinStatus'] == 1 || $beforeResults['KaiinStatus'] == 2 || $beforeResults['KaiinStatus'] == 3) {
						$ret = $objectClient->unInvalCard($arrCustomer); // 無効化解除
					} else {
						$ret = true;
					}
					if ($ret) {
						sleep(5); // 決済サーバーからの通知待ち
						$arrForm  = $objectFormParam->getHashArray();
						$ret = $objectClient->saveCard($arrCustomer, $arrForm);
						if ($ret) {
							$this->success = true;
						} else {
							$arrErr = $objectClient->getError();
							$this->arrErr['error2'] = '※ クレジットカード登録でエラーが発生しました。<br />' . implode('<br />', $arrErr);
							$this->arrForm = $objectFormParam->getFormParamList();
							if ($beforeResults['KaiinStatus'] == 1 || $beforeResults['KaiinStatus'] == 3) {
								$objectClient->invalCard($arrCustomer); // 再度無効化
							}
						}
					} else {
						$arrErr = $objectClient->getError();
						$this->arrErr['error2'] = '※ 登録クレジットカード無効化解除でエラーが発生しました。<br />' . implode('<br />', $arrErr);
						$this->arrForm = $objectFormParam->getFormParamList();
					}

				} else {
					$this->arrForm = $objectFormParam->getFormParamList();
				}
				break;
			case 'change':
				$this->registParam($objectFormParam);
				$objectFormParam->setParam($_POST);
				$this->arrErr = $objectFormParam->checkError();
				if (SC_Utils::isBlank($this->arrErr)) {
					// 会員無効化の場合は解除する
					if ($beforeResults['KaiinStatus'] == 1 || $beforeResults['KaiinStatus'] == 3) {
						$ret = $objectClient->unInvalCard($arrCustomer); // 無効化解除
					} else {
						$ret = true;
					}
					if ($ret) {
						sleep(5); // 決済サーバーからの通知待ち
						$arrForm  = $objectFormParam->getHashArray();
						$ret = $objectClient->changeCard($arrCustomer, $arrForm);
						if ($ret) {
							$this->success = true;
						} else {
							$arrErr = $objectClient->getError();
							$this->arrErr['error2'] = '※ クレジットカード更新でエラーが発生しました。<br />' . implode('<br />', $arrErr);
							$this->arrForm = $objectFormParam->getFormParamList();
							if ($beforeResults['KaiinStatus'] == 1 || $beforeResults['KaiinStatus'] == 3) {
								$objectClient->invalCard($arrCustomer); // 再度無効化
							}
						}
					} else {
						$arrErr = $objectClient->getError();
						$this->arrErr['error2'] = '※ 登録クレジットカード無効化解除でエラーが発生しました。<br />' . implode('<br />', $arrErr);
						$this->arrForm = $objectFormParam->getFormParamList();
					}
				} else {
					$this->arrForm = $objectFormParam->getFormParamList();
				}
				break;
			default:
				$this->registParam($objectFormParam);
				$this->arrForm = $objectFormParam->getFormParamList();
				break;
		}

		$objectClient = new SLN_C_Member();
		$ret = $objectClient->searchCard($arrCustomer);
		if ($ret) {
			$this->dataHash = $objectClient->arrResults;
		}
	}

	function registParam(&$objectFormParam) {

		$objectQuery =& SC_Query::getSingletonInstance();
		$ret = $objectQuery->get('sub_data', 'dtb_module', 'module_code =?', array(SLN_CODE));
		$subData = unserialize($ret);

		$objectFormParam->addParam(		"カード番号", 		"CardNo",		16,	'n',	array("EXIST_CHECK", "MAX_LENGTH_CHECK", "NUM_CHECK"));
		$objectFormParam->addParam(		"カード有効期限年",	"CardExpYear",	2,	'n',	array("EXIST_CHECK", "MAX_LENGTH_CHECK", "NUM_CHECK"));
		$objectFormParam->addParam(		"カード有効期限月",	"CardExpMonth", 2,	'n',	array("EXIST_CHECK", "MAX_LENGTH_CHECK", "NUM_CHECK"));
		if(in_array('KanaSei', $subData['user_settings']['attestation_assistance'])) {
			$objectFormParam->addParam(	"カード名義:姓",		"KanaSei",		10,	'KVCa',	array("EXIST_CHECK", "MAX_LENGTH_CHECK", "SPTAB_CHECK", "KANA_CHECK"));
			$objectFormParam->addParam(	"カード名義:名",		"KanaMei",		10,	'KVCa',	array("EXIST_CHECK", "MAX_LENGTH_CHECK", "SPTAB_CHECK", "KANA_CHECK"));
		}
		if($subData['user_settings']['SecCd'] == "1") {
			$objectFormParam->addParam(	"セキュリティコード",		"SecCd",		4,	'n',	array("EXIST_CHECK", "MAX_LENGTH_CHECK", "NUM_CHECK"), "");
		}
		if(in_array('BirthDay', $subData['user_settings']['attestation_assistance'])) {
			$objectFormParam->addParam(	"生月日",			"BirthDay", 	4,	'a',	array("EXIST_CHECK", "MAX_LENGTH_CHECK", "NUM_CHECK"));
		}
		if(in_array('TelNo', $subData['user_settings']['attestation_assistance'])) {
			$objectFormParam->addParam(	"電話番号(下4桁)",	"TelNo",		4,	'n',	array("EXIST_CHECK", "MAX_LENGTH_CHECK", "NUM_CHECK"));
		}
	}
}