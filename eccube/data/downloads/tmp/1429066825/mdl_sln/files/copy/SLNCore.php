<?php

require_once(MODULE_REALDIR . 'mdl_sln/defines.php');
require_once(SLN_CLASS_PATH . 'SLN_Core.php');

/**
 * プラグインのメインクラス（フックを渡すためだけに作成される）
 *
 * @package スマートリンクネットワーク決済プラグイン
 * @author SmartLink Network, Inc.
 * @version 1.1.0
*/
class SLNCore extends SC_Plugin_Base {

	public function __construct(array $arrSelfInfo) {
		parent::__construct($arrSelfInfo);
	}

	function install($arrPlugin){
	}
	function uninstall($arrPlugin){
	}
	function enable($arrPlugin){
	}
	function disable($arrPlugin){
	}
	function register(SC_Helper_Plugin $objectHelperPlugin) {
		return parent::register($objectHelperPlugin, $priority);
	}

	function prefilterTransform(&$source, LC_Page_Ex $objectPage, $filename) {
		$class_name = get_class($objectPage);
		$object = new SLN_Core();
		$object->actionPrefilterTransform($class_name, $source, $objectPage, $filename, $this);
	}

	function hookActionBefore(LC_Page_Ex $objectPage) {
		$this->callHookAction('before', $objectPage);
	}

	function hookActionAfter(LC_Page_Ex $objectPage) {
		$this->callHookAction('after', $objectPage);
	}

	function hookActionMode(LC_Page_Ex $objectPage) {
		$this->callHookAction('mode', $objectPage);
	}

	function callHookAction($hook_point, &$objectPage) {
		$class_name = get_class($objectPage);
		$object = new SLN_Core();
		$object->actionHook($class_name, $hook_point, $objectPage, $this);
	}
}
