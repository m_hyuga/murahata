<?php
require_once(SLN_CLASS_PATH . 'SLN_C_CVS.php');

/**
 * SLN_CVSクラス（収納代行用）
 *
 * @package スマートリンクネットワーク決済モジュール
 * @author SmartLink Network, Inc.
 * @version 1.1.0
*/
class SLN_CVS {

	function initParam(&$objectFormParam, &$paymentHash, &$orderHash) {
	}

	function getFormBloc() {
		return;
	}

	function actionByMode($mode, &$objectFormParam, &$orderHash, &$objectPage) {

		$objectPurchase = new SC_Helper_Purchase();

		// 決済実行
		$objectClient = new SLN_C_CVS();
		$result = $objectClient->requestPayment($orderHash, array());

		if (!SC_Utils::isBlank($objectPage->arrErr)) {
			$arrErr = $objectClient->getError();
			$objectPage->arrErr['payment'] = '※ 決済でエラーが発生しました。<br />' . implode('<br />', $arrErr);
		} else {
			// 注文完了メールの送信
			$objectPurchase->sendOrderMail($orderHash['order_id']);
			exit;
		}
		return true;
	}
}