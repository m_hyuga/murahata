<?php
require_once(MODULE_REALDIR . 'mdl_sln/defines.php');
require_once(SLN_CLASS_PATH . 'SLN_C_Base.php');

/**
 * SLN_C_Creditクラス（クレジットカード用）
 *
 * @package スマートリンクネットワーク決済モジュール
 * @author SmartLink Network, Inc.
 * @version 1.1.0
*/
class SLN_C_Credit extends SLN_C_Base {

	function requestPayment($orderHash, $paramHash) {
		$objectMdl =& SLN::getInstance();
		$slnSettingHash = $objectMdl->getConfigs();

		$accessUrl = $slnSettingHash['credit_connection_place1'];

		$sendKeyHash = array(
				'MerchantId',
				'MerchantPass',
				'TenantId',
				'OrderID',
				'Amount',
				'TdTenantName',
				'TransactionDate',
				'OperateId',
				'CardNo',
				'PayType',
				'CardExp',
				'MerchantFree1'
		);

		// 設定値によって送信する内容を変更
		if (!SC_Utils::isBlank($slnSettingHash['SecCd'])) {
			$sendKeyHash[] = 'SecCd';
		}
		if (in_array('KanaSei',  $slnSettingHash['attestation_assistance'])) {
			$sendKeyHash[] = 'KanaSei';
			$sendKeyHash[] = 'KanaMei';
		}
		if (in_array('BirthDay', $slnSettingHash['attestation_assistance'])) $sendKeyHash[] = 'BirthDay';
		if (in_array('TelNo',    $slnSettingHash['attestation_assistance'])) $sendKeyHash[] = 'TelNo';

		$paramHash['pay_status'] = '';
		$paramHash['fail_pay_status'] = '';

		// 仮売上(1Auth) 即時売上(1Gathering)
		if ($slnSettingHash['OperateId'] ==  '1Auth') {
			$paramHash['OperateId'] = '1Auth';
			$paramHash['success_pay_status'] = SLN_PAY_STATUS_AUTH;
		} elseif ($slnSettingHash['OperateId'] ==  '1Gathering'){
			$paramHash['OperateId'] = '1Gathering';
			$paramHash['success_pay_status'] = SLN_PAY_STATUS_CAPTURE;
		} else {
			return false;
		}

		return $this->orderRequest($accessUrl, $sendKeyHash, $orderHash, $paramHash, $slnSettingHash);
	}
}