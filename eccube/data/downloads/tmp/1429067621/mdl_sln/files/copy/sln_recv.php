<?php
require_once('../require.php');
require_once(MODULE_REALDIR . 'mdl_sln/defines.php');
require_once(SLN_CLASS_PATH . "SLN_Recv.php");

/** 決済通知先設定
 *
 * @package スマートリンクネットワーク決済モジュール
 * @author SmartLink Network, Inc.
 * @version 1.1.0
*/
$objectPage = new SLN_Recv();
$objectPage->init();
$objectPage->process();