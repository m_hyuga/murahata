<?php
session_cache_limiter('private-no-expire');
require_once(MODULE_REALDIR . 'mdl_sln/defines.php');
require_once(SLN_CLASS_PATH . "SLN_Helper.php");

/**
 * 支払設定
 *
 * @package スマートリンクネットワーク決済モジュール
 * @author SmartLink Network, Inc.
 * @version 1.1.0
*/

$objectPage = new SLN_Helper();
$objectPage->init();
$objectPage->process();