<!--{if ($sln_payid == SLN_PAYID_CREDIT || $sln_payid == SLN_PAYID_REGIST_CREDIT || $sln_payid == SLN_PAYID_CVS) && $arrForm.status.value != $smarty.const.ORDER_PENDING }-->

<script type="text/javascript">//<![CDATA[
$(function() {
    var select_payment_id = $('select[name="payment_id"]').val();
    $('select[name=payment_id]').attr('onchange','');
    $('select[name=payment_id]').unbind();
    $('select[name=payment_id]').change(function() {
        $('select[name=payment_id]').val(select_payment_id);
        alert('お支払い方法の変更は行わないで下さい。');
    });
});
//]]>
</script>

<!--{/if}-->