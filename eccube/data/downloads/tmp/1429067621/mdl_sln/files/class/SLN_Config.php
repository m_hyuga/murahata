<?php
require_once(MODULE_REALDIR . 'mdl_sln/defines.php');
require_once(CLASS_EX_REALDIR . "page_extends/admin/LC_Page_Admin_Ex.php");
require_once(SLN_CLASS_PATH . "SLN_Util.php");

/**
 * SLN_Configクラス（設定情報の管理）
 *
 * @package スマートリンクネットワーク決済モジュール
 * @author SmartLink Network, Inc.
 * @version 1.1.0
*/
class SLN_Config extends LC_Page_Admin_Ex {

	function init() {
		parent::init();
		$objectMdl =& SLN::getInstance();
		$this->tpl_mainpage = SLN_TEMPLATE_PATH. 'admin/sln_config.tpl';
		$this->arrPayments = SLN_Util::getPaymentTypeNames();

		// オプションタグ設定
		$this->setOptions();
	}

	function process() {
		$this->action();
		$this->sendResponse();
	}

	function action() {
		$objectMdl =& SLN::getInstance();
		$objectMdl->install();

		$registerResult		= $this->getDefaultMode();
		$this->arrForm		= $registerResult[0];
		$this->tpl_onload	= $registerResult[1];

		$mode = $this->getMode();
		if ($mode == 'register') {
			$registerResult		= $this->registerMode($_POST);
			$this->arrForm		= $registerResult[0];
			$this->arrErr		= $registerResult[1];
			$this->tpl_onload	= $registerResult[2];

			if (SC_Utils::isBlank($this->arrErr)) {
				$this->registPage($this->arrForm['is_tpl_init']['value']);
				$plugin_id = $this->registPlugin();
				$this->registBloc($this->arrForm['enable_payment_type']['value'], $plugin_id, $this->arrForm['is_tpl_init']['value']);
			}
		}

		$this->tpl_is_module_regist = $this->isRegistPaymentModule();
		$this->setTemplate($this->tpl_mainpage);
	}

	function getDefaultMode() {
		$objectMdl =& SLN::getInstance();
		$subData = $objectMdl->getConfigs();
		$tpl_onload = '';
		$objectForm = $this->initParam($subData);
		return array($objectForm->getFormParamList(), $tpl_onload);
	}

	function registerMode(&$paramHash) {

		$objectSess = new SC_Session();
		$objectMdl =& SLN::getInstance();

		$objectForm = $this->initParam($paramHash);
		$objectForm->arrDefault["SecCd"] = '1';
		if ($arrErr = $objectForm->checkError()) {
			return array($objectForm->getFormParamList(), $arrErr, '');
		}

		if ($arrErr = $this->checkError($objectForm)) {
			return array($objectForm->getFormParamList(), $arrErr, '');
		}

		$arrForm = $objectForm->getHashArray();
		$objectMdl->registerSubData($arrForm, 'user_settings');

		$objectQuery =& SC_Query::getSingletonInstance();
		$objectQuery->begin();
		$objectQuery->update('dtb_payment', array('del_flg' => 1), 'module_code = ?', array(SLN_CODE));

		foreach ($arrForm['enable_payment_type'] as $payment_type_id) {
			//ユーザ画面「お支払方法の指定」の文言 を設定
			if($payment_type_id == SLN_PAYID_CVS){

				//利用できるオンライン収納方法
				global $arrOnlinePaymentMethod;
				$this->arrOnlinePaymentMethod = $arrOnlinePaymentMethod;

				global $arrShortPaymentMethod;
				$this->arrOnlinePaymentMethod = $arrShortPaymentMethod;

				$paymentStr = '';
				if(!empty($arrOnlinePaymentMethod[SLN_ONLINE_PAYMENT_METHOD1]) || !empty($arrPaymentMethod[SLN_ONLINE_PAYMENT_METHOD2])){
					$paymentStr .= $arrShortPaymentMethod[SLN_ONLINE_SHORT_PAYMENT_METHOD1].' ';
				}
				if(!empty($arrOnlinePaymentMethod[SLN_ONLINE_PAYMENT_METHOD3])){
					$paymentStr .= $arrShortPaymentMethod[SLN_ONLINE_SHORT_PAYMENT_METHOD3].' ';
				}
				if(!empty($arrOnlinePaymentMethod[SLN_ONLINE_PAYMENT_METHOD4])){
					$paymentStr .= $arrShortPaymentMethod[SLN_ONLINE_SHORT_PAYMENT_METHOD4].' ';
				}
				if(!empty($arrOnlinePaymentMethod[SLN_ONLINE_PAYMENT_METHOD5])){
					$paymentStr .= $arrShortPaymentMethod[SLN_ONLINE_SHORT_PAYMENT_METHOD5].' ';
				}
				if(!empty($arrOnlinePaymentMethod[SLN_ONLINE_PAYMENT_METHOD6])){
					$paymentStr .= $arrShortPaymentMethod[SLN_ONLINE_SHORT_PAYMENT_METHOD6].' ';
				}
			} else {
				$paymentStr = $this->arrPayments[ $payment_type_id ];
			}

			$dataHash = array(
					'fix' => 3,
					'payment_method' => $paymentStr,
					'creator_id' => $objectSess->member_id,
					'update_date' => 'CURRENT_TIMESTAMP',
					'module_path' => SLN_PATH . 'payment.php',
					'module_code' => SLN_CODE,
					'del_flg' => 0
			);
			$dataHash[SLN_PAYMENT_COL_PAYID] = $payment_type_id;

			$arrPayment = $this->getPaymentFromDB($payment_type_id);

			// 初期登録か更新か
			if (count($arrPayment) > 0){
				$where = "module_code = ? AND " . SLN_PAYMENT_COL_PAYID . " = ?";
				$whereHash = array(SLN_CODE, (string)$payment_type_id);
				$objectQuery->update('dtb_payment', $dataHash, $where, $whereHash);
			} else {
				// ランクは設定可能な最大値にする
				$max_rank = $objectQuery->max('rank', 'dtb_payment');
				$dataHash["create_date"] = "CURRENT_TIMESTAMP";
				$dataHash["rank"] = $max_rank + 1;
				$dataHash['payment_id'] = $objectQuery->nextVal('dtb_payment_payment_id');
				$dataHash = array_merge($this->getDefaultPaymentConfig($payment_type_id), $dataHash);

				$objectQuery->insert("dtb_payment", $dataHash);
			}
		}

		$objectQuery->commit();

		$tpl_onload = 'alert("設定情報を保存しました。");window.close();';

		return array($objectForm->getFormParamList(), $arrErr, $tpl_onload);
	}

	function initParam($dataHash = array()) {
		$objectForm = new SC_FormParam();

		$objectForm->addParam('決済方法', 'enable_payment_type', INT_LEN, 'n', array('EXIST_CHECK', 'NUM_CHECK'), isset($dataHash['enable_payment_type']) ? $dataHash['enable_payment_type'] : "");

		//共通設定
		$objectForm->addParam("マーチャントID", "MerchantId", MERCHANTID_LEN, "KVa", array("EXIST_CHECK", "MAX_LENGTH_CHECK", "SPTAB_CHECK" ,"ALNUM_CHECK"));
		$objectForm->addParam("マーチャントパスワード", "MerchantPass", MERCHANTPASS_LEN, "KVa", array("EXIST_CHECK", "MAX_LENGTH_CHECK", "SPTAB_CHECK" ,"ALNUM_CHECK"));
		$objectForm->addParam("店舗コード", "TenantId", TENPOCODE_LEN, "KVa", array("MAX_LENGTH_CHECK", "SPTAB_CHECK"));

		$objectForm->addParam('有効にする決済方法', 'enable_payment_type', INT_LEN, 'n', array('EXIST_CHECK', 'NUM_CHECK'), isset($dataHash['enable_payment_type']) ? $dataHash['enable_payment_type'] : "");

		//クレジットカード決済
		$objectForm->addParam("接続先", "credit_connection_place");

		$objectForm->addParam("接続先-カード決済 オンライン取引", "credit_connection_place1");
		$objectForm->addParam("接続先-会員情報登録電文", "credit_connection_place2");
		$objectForm->addParam("接続先-PC 用支払先選択画面リダイレクトURL", "credit_connection_place3");
		$objectForm->addParam("接続先-モバイル用支払先選択画面リダイレクトURL", "credit_connection_place4");
		$objectForm->addParam("接続先-オンライン取引接続用URL", "credit_connection_place5");

		$objectForm->addParam("支払回数", "payKbnKaisu","","", array("EXIST_CHECK"));
		$objectForm->addParam("セキュリティコード", "SecCd");
		$objectForm->addParam("認証アシスト項目", "attestation_assistance");
		$objectForm->addParam("カード決済手続き", "OperateId");
		$objectForm->addParam("会員登録機能", "member_regist");
		$objectForm->addParam("クイック決済", "quick_accounts");

		//オンライン収納代行
		$objectForm->addParam("利用できるオンライン収納決済方法", "OnlinePaymentMethod");
		$objectForm->addParam("フリーエリア1","Free1",FREEAREA_LEN,"KV",array("MAX_LENGTH_CHECK", "SPTAB_CHECK"));
		$objectForm->addParam("フリーエリア2","Free2",FREEAREA_LEN,"KV",array("MAX_LENGTH_CHECK", "SPTAB_CHECK"));
		$objectForm->addParam("フリーエリア3","Free3",FREEAREA_LEN,"KV",array("MAX_LENGTH_CHECK", "SPTAB_CHECK"));
		$objectForm->addParam("フリーエリア4","Free4",FREEAREA_LEN,"KV",array("MAX_LENGTH_CHECK", "SPTAB_CHECK"));
		$objectForm->addParam("フリーエリア5","Free5",FREEAREA_LEN,"KV",array("MAX_LENGTH_CHECK", "SPTAB_CHECK"));
		$objectForm->addParam("フリーエリア6","Free6",FREEAREA_LEN,"KV",array("MAX_LENGTH_CHECK", "SPTAB_CHECK"));
		$objectForm->addParam("フリーエリア7","Free7",FREEAREA_LEN,"KV",array("MAX_LENGTH_CHECK", "SPTAB_CHECK"));
		$objectForm->addParam("ご案内1","Comment",ANNAI_LEN,"KV",array("MAX_LENGTH_CHECK", "SPTAB_CHECK"));
		$objectForm->addParam("ご案内2","Free8",ANNAI_LEN,"KV",array("MAX_LENGTH_CHECK", "SPTAB_CHECK"));
		$objectForm->addParam("ご案内3","Free9",ANNAI_LEN,"KV",array("MAX_LENGTH_CHECK", "SPTAB_CHECK"));
		$objectForm->addParam("ご案内4","Free10",ANNAI_LEN,"KV",array("MAX_LENGTH_CHECK", "SPTAB_CHECK"));
		$objectForm->addParam("ご案内5","Free11",ANNAI_LEN,"KV",array("MAX_LENGTH_CHECK", "SPTAB_CHECK"));
		$objectForm->addParam("ご案内6","Free12",ANNAI_LEN,"KV",array("MAX_LENGTH_CHECK", "SPTAB_CHECK"));
		$objectForm->addParam("ご案内7","Free13",ANNAI_LEN,"KV",array("MAX_LENGTH_CHECK", "SPTAB_CHECK"));
		$objectForm->addParam("ご案内8","Free14",ANNAI_LEN,"KV",array("MAX_LENGTH_CHECK", "SPTAB_CHECK"));
		$objectForm->addParam("ご案内9","Free15",ANNAI_LEN,"KV",array("MAX_LENGTH_CHECK", "SPTAB_CHECK"));
		$objectForm->addParam("ご案内10","Free16",ANNAI_LEN,"KV",array("MAX_LENGTH_CHECK", "SPTAB_CHECK"));
		$objectForm->addParam("問い合わせ先","Free17",QUESTIONSAKI_LEN,"KVa",array("MAX_LENGTH_CHECK", "SPTAB_CHECK"));
		$objectForm->addParam("問合せ電話","Free18",QUESTIONTEL_LEN,"KVa",array("MAX_LENGTH_CHECK", "SPTAB_CHECK"));
		$objectForm->addParam("問い合わせ時間","Free19",QUESTIONTIME_LEN,"KVa",array("MAX_LENGTH_CHECK", "SPTAB_CHECK","GRAPH_CHECK"));
		$objectForm->addParam("ご案内タイトル","Title",CUSTANNNAITITLE_LEN ,"KVa",array("MAX_LENGTH_CHECK", "SPTAB_CHECK"));
		$objectForm->addParam("お支払方法の指定の文言", "payOnlineWording");

		$objectForm->setParam($dataHash);
		$objectForm->convParam();
		return $objectForm;
	}

	static function checkError(SC_FormParam &$objFormParam){
		$arrErr = array();
		$arrParams = $objFormParam->getHashArray();
		if (empty($arrParams['Free18'])) return $arrErr;
		if(!preg_match('/^[0-9\-]+$/', $arrParams['Free18'])){
			$arrErr['Free18'] = '半角数字ハイフンで入力してください。';
		}
		return $arrErr;
	}

	function getPaymentFromDB($type){
		$objectQuery =& SC_Query::getSingletonInstance();
		return $objectQuery->select("module_id", "dtb_payment", "module_code = ? AND " . SLN_PAYMENT_COL_PAYID . " = ?", array(SLN_CODE, (string)$type));
	}

	function isRegistPaymentModule() {
		$objectQuery =& SC_Query::getSingletonInstance();
		if($objectQuery->count('dtb_payment', 'module_code = ?', array(SLN_CODE))) {
			return true;
		}
		return false;
	}

	function registPlugin() {

		$arrPluginInfo = array(
				'PLUGIN_CODE' => 'SLNCore',
				'PLUGIN_NAME' => 'スマートリンクネットワーク決済プラグイン',
				'CLASS_NAME' => 'SLNCore',
				'PLUGIN_VERSION' => '1.0',
				'PLUGIN_CODE' => 'SLNCore',
				'COMPLIANT_VERSION' => '2.13.0',
				'AUTHOR' => 'スマートリンクネットワーク',
				'DESCRIPTION' => 'スマートリンクネットワーク決済モジュールの動作に必要なプラグインです。決済を利用する場合に、本プラグインの無効化や削除はしないで下さい。',
				'PLUGIN_SITE_URL' => 'http://www.smartlink-network.jp/',
				'AUTHOR_SITE_URL' => 'http://www.smartlink-network.jp/',
		);

		// フックポイント一覧
		$hook_point = array(
				array("LC_Page_Admin_Basis_PaymentInput_action_after", 'hookActionAfter'),
				array("LC_Page_Shopping_Payment_action_after", 'hookActionAfter'),
				array("LC_Page_Shopping_Payment_action_before", 'hookActionBefore'),
				array("LC_Page_Shopping_Complete_action_before", 'hookActionBefore'),
				array("LC_Page_Admin_Order_Status_action_before", 'hookActionBefore'),
				array("LC_Page_Admin_Order_Edit_action_before", 'hookActionBefore'),
				array("LC_Page_Admin_Order_Edit_action_after", 'hookActionAfter'),
				array("LC_Page_Admin_Order_action_before", 'hookActionBefore'),
				array("LC_Page_Admin_Order_action_after", 'hookActionAfter'),
				array("LC_Page_Mypage_Refusal_action_before", 'hookActionBefore'),
				array("LC_Page_Admin_Customer_action_after", 'hookActionAfter'),
				array("LC_Page_Products_Detail_action_before", 'hookActionBefore'),
				array("LC_Page_Products_List_action_before", 'hookActionBefore'),
				array("LC_Page_Cart_action_before", 'hookActionBefore'),
				array("LC_Page_Shopping_action_before", 'hookActionBefore'),
				array("LC_Page_Shopping_Payment_action_before", 'hookActionBefore'),
				array("LC_Page_Shopping_Deliv_action_before", 'hookActionBefore'),
				array("LC_Page_Shopping_Confirm_action_before", 'hookActionBefore'),
				array("LC_Page_Shopping_Confirm_action_after", 'hookActionAfter'),
				array("prefilterTransform", 'prefilterTransform')
		);

		$arr_sqlval_plugin = array();
		$arr_sqlval_plugin['plugin_name']		= $arrPluginInfo['PLUGIN_NAME'];
		$arr_sqlval_plugin['plugin_code']		= $arrPluginInfo['PLUGIN_CODE'];
		$arr_sqlval_plugin['class_name']		= $arrPluginInfo['CLASS_NAME'];
		$arr_sqlval_plugin['author']			= $arrPluginInfo['AUTHOR'];
		$arr_sqlval_plugin['author_site_url']	= $arrPluginInfo['AUTHOR_SITE_URL'];
		$arr_sqlval_plugin['plugin_site_url']	= $arrPluginInfo['PLUGIN_SITE_URL'];
		$arr_sqlval_plugin['plugin_version']	= $arrPluginInfo['PLUGIN_VERSION'];
		$arr_sqlval_plugin['compliant_version']	= $arrPluginInfo['COMPLIANT_VERSION'];
		$arr_sqlval_plugin['plugin_description'] = $arrPluginInfo['DESCRIPTION'];
		$arr_sqlval_plugin['priority']			= '0';
		$arr_sqlval_plugin['enable']			= PLUGIN_ENABLE_TRUE;
		$arr_sqlval_plugin['update_date']		= 'CURRENT_TIMESTAMP';

		// プラグインコードからIDを検索
		$pluginHash = SC_Plugin_Util::getPluginByPluginCode($arrPluginInfo['PLUGIN_CODE']);
		$plugin_id = null;
		if (!empty($pluginHash)) {
			$plugin_id = $pluginHash['plugin_id'];
		}

		// プラグイン情報をDB登録.
		$objectQuery =& SC_Query::getSingletonInstance();
		$objectQuery->begin();

		// 登録or更新
		if (SC_Utils::isBlank($plugin_id)) {
			$plugin_id = $objectQuery->nextVal('dtb_plugin_plugin_id');
			$arr_sqlval_plugin['plugin_id'] = $plugin_id;
			$objectQuery->insert('dtb_plugin', $arr_sqlval_plugin);
		} else {
			$objectQuery->update('dtb_plugin', $arr_sqlval_plugin, 'plugin_id = ?', array($plugin_id));
		}

		// フックポイント一回削除
		$objectQuery->delete('dtb_plugin_hookpoint', 'plugin_id = ?', array($plugin_id));

		// フックポイント保存
		foreach ($hook_point as $h) {
			$arr_sqlval_plugin_hookpoint = array();
			$id = $objectQuery->nextVal('dtb_plugin_hookpoint_plugin_hookpoint_id');
			$arr_sqlval_plugin_hookpoint['plugin_hookpoint_id'] = $id;
			$arr_sqlval_plugin_hookpoint['plugin_id'] = $plugin_id;
			$arr_sqlval_plugin_hookpoint['hook_point'] = $h[0];
			$arr_sqlval_plugin_hookpoint['callback'] = $h[1];
			$arr_sqlval_plugin_hookpoint['update_date'] = 'CURRENT_TIMESTAMP';
			$objectQuery->insert('dtb_plugin_hookpoint', $arr_sqlval_plugin_hookpoint);
		}
		$objectQuery->commit();

		// 親ディレクトリが無ければ作成
		if (!file_exists(PLUGIN_UPLOAD_REALDIR)) {
			mkdir(PLUGIN_UPLOAD_REALDIR, 0777);
		}
		if (!file_exists(PLUGIN_HTML_REALDIR)) {
			mkdir(PLUGIN_HTML_REALDIR, 0777);
		}

		// プラグイン用ディレクトリの作成
		$plugin_dir_path = PLUGIN_UPLOAD_REALDIR . 'SLNCore/';
		if (!file_exists($plugin_dir_path)) {
			mkdir($plugin_dir_path, 0777);
		}
		$plugin_html_dir = PLUGIN_HTML_REALDIR . 'SLNCore/';
		if (!file_exists($plugin_html_dir)) {
			mkdir($plugin_html_dir, 0777);
		}

		// 必要なファイルをコピー
		copy(SLN_COPY_PATH . "security_code.jpg", 				$plugin_html_dir . "security_code.jpg");
		copy(SLN_COPY_PATH . "btn_quick.png", 					$plugin_html_dir . "btn_quick.png");
		copy(SLN_COPY_PATH . "btn_quick_on.png", 				$plugin_html_dir . "btn_quick_on.png");
		copy(SLN_COPY_PATH . "btn_bloc_buy.jpg",				$plugin_html_dir . "btn_bloc_buy.jpg");
		copy(SLN_COPY_PATH . "btn_bloc_buy_on.jpg",				$plugin_html_dir . "btn_bloc_buy_on.jpg");
		copy(SLN_COPY_PATH . "plugin_info.php",					$plugin_dir_path . "plugin_info.php");
		copy(SLN_COPY_PATH . "SLNCore.php", 					$plugin_dir_path . "SLNCore.php");
		copy(SLN_COPY_PATH . "payment_status.php", HTML_REALDIR . ADMIN_DIR . "order/payment_status.php");
		copy(SLN_COPY_PATH . "card_info.php", 					HTML_REALDIR . "mypage/card_info.php");
		copy(SLN_COPY_PATH . "sln_recv.php", 					HTML_REALDIR . USER_DIR . "sln_recv.php");

		// パーミッションの設定
		chmod(HTML_REALDIR . ADMIN_DIR . "order/payment_status.php", 0644);
		chmod(HTML_REALDIR . "mypage/card_info.php", 0644);
		chmod(HTML_REALDIR . USER_DIR . "sln_recv.php", 0644);

		// ページデータの保存
		$page_name = 'MYページ/登録済カード情報変更';
		$url = 'mypage/card_info.php';
		$filename = 'mypage/card_info';
		$tpl_data = file_get_contents(SLN_TEMPLATE_PATH . 'default/sln_card_info.tpl');
		$device_type_id = DEVICE_TYPE_PC;
		$page_id = $this->setPageData($tpl_data, $page_name, $url, $filename, $device_type_id);

		SC_Utils::clearCompliedTemplate();
		return $plugin_id;
	}

	function getDefaultPaymentConfig($payment_type_id) {
		$dataHash = array();
		$dataHash['charge'] = '0';
		$dataHash['rule_max'] = '1';
		$arrDate['charge_flag'] = '2'; //支払方法設定画面の手数料を設定不可にする。

		switch ($payment_type_id) {
			case SLN_PAYID_CREDIT:
			case SLN_PAYID_REGIST_CREDIT:
				$dataHash['upper_rule'] = CREDIT_PAYMENT_MAX;
				break;
			case SLN_PAYID_CVS:
				$dataHash['upper_rule'] = CONVENIENCE_PAYMENT_MAX;
				break;
		}
		$dataHash['upper_rule_max'] = $dataHash['upper_rule'];
		return $dataHash;
	}

	function registPage() {
		$pageIdHash = array();
		// 決済画面をデザインテンプレートに足す
		$page_name = '商品購入/決済画面';
		$module_url = 'shopping/load_payment_module.php';
		$filename = 'shopping/load_payment_module';

		$tpl_data = file_get_contents(SLN_TEMPLATE_PATH . 'default/sln_payment.tpl');
		$device_type_id = DEVICE_TYPE_PC;
		$page_id = $this->setPageData($tpl_data, $page_name, $module_url, $filename, $device_type_id);
		$pageIdHash[ $filename ][ $device_type_id ] = $page_id;

		$tpl_data = file_get_contents(SLN_TEMPLATE_PATH . 'sphone/sln_payment.tpl');
		$device_type_id = DEVICE_TYPE_SMARTPHONE;
		$page_id = $this->setPageData($tpl_data, $page_name, $module_url, $filename, $device_type_id);
		$pageIdHash[ $filename ][ $device_type_id ] = $page_id;

		$tpl_data = file_get_contents(SLN_TEMPLATE_PATH . 'mobile/sln_payment.tpl');
		$device_type_id = DEVICE_TYPE_MOBILE;
		$page_id = $this->setPageData($tpl_data, $page_name, $module_url, $filename, $device_type_id);
		$pageIdHash[ $filename ][ $device_type_id ] = $page_id;

		$objectMdl =& SLN::getInstance();
		$objectMdl->registerSubData($pageIdHash, 'page_setting');
	}

	function registBloc($arrPaymentTypeId, $plugin_id) {
		$blocIdHash = array();
		foreach ($arrPaymentTypeId as $payment_type_id) {
			$filename = "";
			if ($payment_type_id == SLN_PAYID_CREDIT) {
				$filename ="sln_credit";
			} elseif ($payment_type_id == SLN_PAYID_REGIST_CREDIT) {
				$filename ="sln_regist_credit";
			} elseif ($payment_type_id == SLN_PAYID_CVS) {
				$filename ="sln_cvs";
			}
			$bloc_name = $this->arrPayments[$payment_type_id] . "入力フォーム";

			$bloc_data = file_get_contents(SLN_TEMPLATE_PATH . 'default/bloc/' . $filename . '.tpl');
			$device_type_id = DEVICE_TYPE_PC;
			$bloc_id = $this->setBlocData($plugin_id, $bloc_data, $device_type_id, $bloc_name, $filename, "");
			$blocIdHash[ $filename ][ $device_type_id ] = $bloc_id;

			$bloc_data = file_get_contents(SLN_TEMPLATE_PATH . 'sphone/bloc/' . $filename . '.tpl');
			$device_type_id = DEVICE_TYPE_SMARTPHONE;
			$bloc_id = $this->setBlocData($plugin_id, $bloc_data, $device_type_id, $bloc_name, $filename, "");
			$blocIdHash[ $filename ][ $device_type_id ] = $bloc_id;

			$bloc_data = file_get_contents(SLN_TEMPLATE_PATH . 'mobile/bloc/' . $filename . '.tpl');
			$device_type_id = DEVICE_TYPE_MOBILE;
			$bloc_id = $this->setBlocData($plugin_id, $bloc_data, $device_type_id, $bloc_name, $filename, "");
			$blocIdHash[ $filename ][ $device_type_id ] = $bloc_id;
		}

		$objectMdl =& SLN::getInstance();
		$objectMdl->registerSubData($blocIdHash, 'bloc_setting');
	}

	function setOptions() {
		//支払方法
		global $arrPaymentMethod;
		$this->arrPaymentMethod = $arrPaymentMethod;

		//接続先
		global $arrCreditConnectionPlace;
		$this->arrCreditConnectionPlace = array();
		$this->arrCreditConnectionPlace['--'] = "--";
		$this->arrCreditConnectionPlace = $arrCreditConnectionPlace;

		//支払回数(一括払い～分割72回）
		global $arrPayKbnKaisu;
		$this->arrPayKbnKaisu = $arrPayKbnKaisu;

		//支払回数(リボルビング払い、ボーナス）
		global $arrPayKbnKaisu;
		$this->arrPayKbnKaisu = $arrPayKbnKaisu;

		//セキュリティコード
		global $arrSecurityCode;
		$this->arrSecurityCode = $arrSecurityCode;

		//認証アシスト項目
		global $arrAssistance;
		$this->arrAssistance = $arrAssistance;

		//カード決済手続き
		global $arrCardProcedure;
		$this->arrCardProcedure = $arrCardProcedure;

		//会員登録機能
		global $arrMemberRegist;
		$this->arrMemberRegist = $arrMemberRegist;

		//クイック決済
		global $arrQuickAccounts;
		$this->arrQuickAccounts = $arrQuickAccounts;

		//オンライン収納代行 - 決済方法
		global $arrOnlinePaymentMethod;
		$this->arrOnlinePaymentMethod = $arrOnlinePaymentMethod;

		//お支払方法の指定の文言
		global $arrPayOnlineWording;
		$this->arrPayOnlineWording = $arrPayOnlineWording;
	}

	function setPageData($tpl_data, $page_name, $url, $filename, $device_type_id) {
		$objectLayout = new SC_Helper_PageLayout();
		$objectQuery =& SC_Query::getSingletonInstance();
		$objectQuery->begin();

		$tpl_dir = $objectLayout->getTemplatePath($device_type_id);
		$tpl_path = $filename . '.tpl';

		$arrExists = $objectLayout->getPageProperties($device_type_id, null, 'device_type_id = ? and filename = ?', array($device_type_id, $filename));

		$exists_file = $tpl_dir . $arrExists[0]['filename'] . '.tpl';
		if (file_exists($exists_file)) {
			unlink($exists_file);
		}

		$table = 'dtb_pagelayout';
		$valueHash = array(
				'device_type_id' => $device_type_id,
				'header_chk' => 1,
				'footer_chk' => 1,
				'page_name' => $page_name,
				'url' => $url,
				'filename' => $filename,
				'edit_flg' => '2',
				'update_url' => $_SERVER['HTTP_REFERER'],
				'update_date' =>  'CURRENT_TIMESTAMP'
		);

		$objectQuery->setOrder('');
		if (SC_Utils::isBlank($arrExists[0]['page_id'])) {
			$valueHash['page_id'] = 1 + $objectQuery->max('page_id', $table, 'device_type_id = ?', array($device_type_id));
			$valueHash['create_date'] = 'CURRENT_TIMESTAMP';
			$objectQuery->insert($table, $valueHash);
			$page_id = $valueHash['page_id'];
		} else {
			$objectQuery->update($table, $valueHash, 'page_id = ? AND device_type_id = ?', array($arrExists[0]['page_id'], $device_type_id));
			$page_id = $arrExists[0]['page_id'];
		}

		$tpl_path = $tpl_dir . $filename . '.tpl';

		if (!SC_Helper_FileManager::sfWriteFile($tpl_path, $tpl_data)) {
			$objectQuery->rollback();
			return false;
		}
		$objectQuery->commit();
		return $page_id;
	}


	function setBlocData($plugin_id, $bloc_data, $device_type_id, $bloc_name, $filename, $php_path = "") {
		$objectLayout = new SC_Helper_PageLayout();
		$objectQuery = SC_Query::getSingletonInstance();
		$objectQuery->begin();

		$bloc_dir = $objectLayout->getTemplatePath($device_type_id) . BLOC_DIR;
		$tpl_path = $filename . '.tpl';

		if (!SC_Utils::isBlank($plugin_id)) {
			$where = 'filename = ?';
			$arrval = array($filename);
		} else {
			$where = 'filename = ?';
			$arrval = array($filename);
		}

		$arrExists = $objectLayout->getBlocs($device_type_id, $where, $arrval);
		$exists_file = $bloc_dir . $arrExists[0]['filename'] . '.tpl';
		if (file_exists($exists_file)) {
			unlink($exists_file);
		}

		$sqlHash = array(
				'device_type_id' => $device_type_id,
				'bloc_name' => $bloc_name,
				'tpl_path' => $filename . '.tpl',
				'filename' => $filename,
				'update_date' => "CURRENT_TIMESTAMP",
				'deletable_flg' => 0,
				'plugin_id' => $plugin_id
		);
		if (!SC_Utils::isBlank($php_path)) {
			$sqlHash['php_path'] = $php_path;
		}

		$objectQuery->setOrder('');

		if (SC_Utils::isBlank($arrExists[0]['bloc_id'])) {
			$sqlHash['bloc_id'] = $objectQuery->max('bloc_id', "dtb_bloc", "device_type_id = ?", array($device_type_id)) + 1;
			$sqlHash['create_date'] = "CURRENT_TIMESTAMP";
			$objectQuery->insert("dtb_bloc", $sqlHash);
			$bloc_id = $sqlHash['bloc_id'];
		} else {
			$objectQuery->update("dtb_bloc", $sqlHash, "device_type_id = ? AND bloc_id = ?", array($device_type_id, $arrExists[0]['bloc_id']));
			$bloc_id = $arrExists[0]['bloc_id'];
		}

		$bloc_path = $bloc_dir . $tpl_path;
		if (!SC_Helper_FileManager::sfWriteFile($bloc_path, $bloc_data)) {
			$objectQuery->rollback();
			return false;
		}

		$objectQuery->commit();
		return $bloc_id;
	}
}