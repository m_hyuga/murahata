<?php /* Smarty version 2.6.27, created on 2015-06-29 16:29:45
         compiled from /home/murahata/www/data/Smarty/templates/default/products/detail.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'script_escape', '/home/murahata/www/data/Smarty/templates/default/products/detail.tpl', 38, false),array('modifier', 'count', '/home/murahata/www/data/Smarty/templates/default/products/detail.tpl', 40, false),array('modifier', 'h', '/home/murahata/www/data/Smarty/templates/default/products/detail.tpl', 60, false),array('modifier', 'strlen', '/home/murahata/www/data/Smarty/templates/default/products/detail.tpl', 75, false),array('modifier', 'sfCalcIncTax', '/home/murahata/www/data/Smarty/templates/default/products/detail.tpl', 92, false),array('modifier', 'number_format', '/home/murahata/www/data/Smarty/templates/default/products/detail.tpl', 92, false),array('modifier', 'nl2br_html', '/home/murahata/www/data/Smarty/templates/default/products/detail.tpl', 95, false),)), $this); ?>

<script type="text/javascript">//<![CDATA[
    // 規格2に選択肢を割り当てる。
    function fnSetClassCategories(form, classcat_id2_selected) {
        var $form = $(form);
        var product_id = $form.find('input[name=product_id]').val();
        var $sele1 = $form.find('select[name=classcategory_id1]');
        var $sele2 = $form.find('select[name=classcategory_id2]');
        eccube.setClassCategories($form, product_id, $sele1, $sele2, classcat_id2_selected);
    }
//]]></script>
		<div class="cf">
		<div class="side_area">
			<div class="facebook"><iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Ffruitmurahata%3Ffref%3Dts&amp;width=236&amp;height=338&amp;colorscheme=light&amp;show_faces=false&amp;header=true&amp;stream=true&amp;show_border=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:236px; height:338px;" allowtransparency="true"></iframe></div>
			<div class="rec_menu">
			<div class="reccomend">
				<h3><img src="<?php echo ((is_array($_tmp=$this->_tpl_vars['TPL_URLPATH'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
img/top/ttl_rec.jpg" alt="おすすめ商品" width="127" height="35"></h3>
                                        <?php if (count(((is_array($_tmp=$this->_tpl_vars['arrPageLayout']['MainFoot'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp))) > 0): ?>
                        <?php $_from = ((is_array($_tmp=$this->_tpl_vars['arrPageLayout']['MainFoot'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['MainFootKey'] => $this->_tpl_vars['MainFootItem']):
?>
                            <!-- ▼<?php echo ((is_array($_tmp=$this->_tpl_vars['MainFootItem']['bloc_name'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
 -->
                            <?php if (((is_array($_tmp=$this->_tpl_vars['MainFootItem']['php_path'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)) != ""): ?>
                                <?php require_once(SMARTY_CORE_DIR . 'core.smarty_include_php.php');
smarty_core_smarty_include_php(array('smarty_file' => ((is_array($_tmp=$this->_tpl_vars['MainFootItem']['php_path'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)), 'smarty_assign' => '', 'smarty_once' => false, 'smarty_include_vars' => array('items' => ((is_array($_tmp=$this->_tpl_vars['MainFootItem'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))), $this); ?>

                            <?php else: ?>
                                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ((is_array($_tmp=$this->_tpl_vars['MainFootItem']['tpl_path'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)), 'smarty_include_vars' => array('items' => ((is_array($_tmp=$this->_tpl_vars['MainFootItem'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                            <?php endif; ?>
                            <!-- ▲<?php echo ((is_array($_tmp=$this->_tpl_vars['MainFootItem']['bloc_name'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
 -->
                        <?php endforeach; endif; unset($_from); ?>
                    <?php endif; ?>
                    			</div>
			</div><!-- /reccomend -->
		</div>
		<?php if (count ( ((is_array($_tmp=$this->_tpl_vars['arrRelativeCat'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)) ) > 0): ?>
		<div id="main_area" class="detail">
			<?php if (((is_array($_tmp=$this->_tpl_vars['arrRelativeCat']['0']['0']['category_id'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)) == 1): ?>
				<h3><img src="<?php echo ((is_array($_tmp=$this->_tpl_vars['TPL_URLPATH'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
img/cakes/ttl_main.jpg" alt="洋菓子メニュー"></h3>
				<div class="ttl_deta cf">
					<h4><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProduct']['name'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('h', true, $_tmp) : smarty_modifier_h($_tmp)); ?>
</h4>
					<p class="his_back"><a href="<?php echo ((is_array($_tmp=@TOP_URL)) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
products/list.php?category_id=1"><span>&#x25B6;</span>商品一覧に戻る</a></p>
				</div>
				<?php elseif (((is_array($_tmp=$this->_tpl_vars['arrRelativeCat']['0']['0']['category_id'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)) == 6): ?>
				<h3><img src="<?php echo ((is_array($_tmp=$this->_tpl_vars['TPL_URLPATH'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
img/gift/ttl_main.jpg" alt="ショッピングメニュー"></h3>
				<div class="ttl_deta cf">
					<h4><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProduct']['name'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('h', true, $_tmp) : smarty_modifier_h($_tmp)); ?>
</h4>
					<p class="his_back"><a href="<?php echo ((is_array($_tmp=@TOP_URL)) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
products/list.php?category_id=6"><span>&#x25B6;</span>商品一覧に戻る</a></p>
				</div>
				<?php endif; ?>
			<?php endif; ?>
			<div class="detail_area cf">
				<div class="deta_left">
                    <?php $this->assign('key', 'main_image'); ?>
                    <!--★画像★-->
                    <?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProduct']['main_large_image'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('strlen', true, $_tmp) : strlen($_tmp)) >= 1): ?>
                        <a
                            href="<?php echo ((is_array($_tmp=@IMAGE_SAVE_URLPATH)) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProduct']['main_large_image'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('h', true, $_tmp) : smarty_modifier_h($_tmp)); ?>
"
                            class="colorbox" 
														title="<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProduct']['name'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('h', true, $_tmp) : smarty_modifier_h($_tmp)); ?>
"
                        >
                    <?php endif; ?>
                        <img src="<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrFile'][$this->_tpl_vars['key']]['filepath'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('h', true, $_tmp) : smarty_modifier_h($_tmp)); ?>
" alt="<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProduct']['name'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('h', true, $_tmp) : smarty_modifier_h($_tmp)); ?>
" class="picture" />
                    <?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProduct']['main_large_image'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('strlen', true, $_tmp) : strlen($_tmp)) >= 1): ?>
                        </a>
                    <?php endif; ?>
				</div>
				<?php if (count ( ((is_array($_tmp=$this->_tpl_vars['arrRelativeCat'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)) ) > 0): ?>
					<?php if (((is_array($_tmp=$this->_tpl_vars['arrRelativeCat']['0']['0']['category_id'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)) == 1): ?>
				<div class="deta_right cakes_datail">
					<p class="price">								<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=((is_array($_tmp=$this->_tpl_vars['arrProductsClassList'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp))) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
								税込 <?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProductsClassList'][$this->_sections['i']['index']]['price02'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('sfCalcIncTax', true, $_tmp, ((is_array($_tmp=$this->_tpl_vars['arrSiteInfo']['tax'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)), ((is_array($_tmp=$this->_tpl_vars['arrSiteInfo']['tax_rule'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp))) : SC_Helper_DB_Ex::sfCalcIncTax($_tmp, ((is_array($_tmp=$this->_tpl_vars['arrSiteInfo']['tax'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)), ((is_array($_tmp=$this->_tpl_vars['arrSiteInfo']['tax_rule'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))))) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
円<?php if (((is_array($_tmp=$this->_tpl_vars['arrProductsClassList'][$this->_sections['i']['index']]['classcategory_name1'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)) != ''): ?> / <?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProductsClassList'][$this->_sections['i']['index']]['classcategory_name1'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('h', true, $_tmp) : smarty_modifier_h($_tmp)); ?>
<br>
								<?php endif; ?> <?php if (((is_array($_tmp=$this->_tpl_vars['arrProductsClassList'][$this->_sections['i']['index']]['classcategory_name2'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)) != ''): ?> <?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProductsClassList'][$this->_sections['i']['index']]['classcategory_name2'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('h', true, $_tmp) : smarty_modifier_h($_tmp)); ?>
<br>
								<?php endif; ?><?php endfor; endif; ?></p>
						<p class="caption"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProduct']['main_comment'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('nl2br_html', true, $_tmp) : smarty_modifier_nl2br_html($_tmp)); ?>
</p>
						<p class="notice">※洋菓子メニューはむらはたのフルーツパーラーにて<br>&emsp;販売しています。</p>
					</div>
					<?php elseif (((is_array($_tmp=$this->_tpl_vars['arrRelativeCat']['0']['0']['category_id'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)) == 6 || ((is_array($_tmp=$this->_tpl_vars['arrRelativeCat']['0']['0']['category_id'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)) == 7 || ((is_array($_tmp=$this->_tpl_vars['arrRelativeCat']['0']['0']['category_id'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)) == 8): ?>
					<form name="form1" id="form1" method="post" action="?">
					<input type="hidden" name="<?php echo ((is_array($_tmp=@TRANSACTION_ID_NAME)) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['transactionid'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
" />
					<input type="hidden" name="mode" value="cart" />
					<input type="hidden" name="product_id" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['tpl_product_id'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
" />
					<input type="hidden" name="product_class_id" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['tpl_product_class_id'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
" id="product_class_id" />
					<input type="hidden" name="favorite_product_id" value="" />
					<div class="deta_right gift_datail">
						<div class="price cf">
							<div class="left">
								<p>								<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=((is_array($_tmp=$this->_tpl_vars['arrProductsClassList'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp))) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
								税込 <?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProductsClassList'][$this->_sections['i']['index']]['price02'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('sfCalcIncTax', true, $_tmp, ((is_array($_tmp=$this->_tpl_vars['arrSiteInfo']['tax'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)), ((is_array($_tmp=$this->_tpl_vars['arrSiteInfo']['tax_rule'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp))) : SC_Helper_DB_Ex::sfCalcIncTax($_tmp, ((is_array($_tmp=$this->_tpl_vars['arrSiteInfo']['tax'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)), ((is_array($_tmp=$this->_tpl_vars['arrSiteInfo']['tax_rule'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))))) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
円<?php if (((is_array($_tmp=$this->_tpl_vars['arrProductsClassList'][$this->_sections['i']['index']]['classcategory_name1'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)) != ''): ?> / <?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProductsClassList'][$this->_sections['i']['index']]['classcategory_name1'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('h', true, $_tmp) : smarty_modifier_h($_tmp)); ?>
<br>
								<?php endif; ?> <?php if (((is_array($_tmp=$this->_tpl_vars['arrProductsClassList'][$this->_sections['i']['index']]['classcategory_name2'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)) != ''): ?> <?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProductsClassList'][$this->_sections['i']['index']]['classcategory_name2'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('h', true, $_tmp) : smarty_modifier_h($_tmp)); ?>
<br>
								<?php endif; ?><?php endfor; endif; ?></p>
								<?php if (((is_array($_tmp=$this->_tpl_vars['tpl_stock_find'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp))): ?>
									<p class="num">個 数
										<select name="quantity">
											<option value="選択">&#9660;選択</option>
											<?php unset($this->_sections['myloop']);
$this->_sections['myloop']['name'] = 'myloop';
$this->_sections['myloop']['start'] = (int)1;
$this->_sections['myloop']['loop'] = is_array($_loop=21) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['myloop']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['myloop']['show'] = true;
$this->_sections['myloop']['max'] = $this->_sections['myloop']['loop'];
if ($this->_sections['myloop']['start'] < 0)
    $this->_sections['myloop']['start'] = max($this->_sections['myloop']['step'] > 0 ? 0 : -1, $this->_sections['myloop']['loop'] + $this->_sections['myloop']['start']);
else
    $this->_sections['myloop']['start'] = min($this->_sections['myloop']['start'], $this->_sections['myloop']['step'] > 0 ? $this->_sections['myloop']['loop'] : $this->_sections['myloop']['loop']-1);
if ($this->_sections['myloop']['show']) {
    $this->_sections['myloop']['total'] = min(ceil(($this->_sections['myloop']['step'] > 0 ? $this->_sections['myloop']['loop'] - $this->_sections['myloop']['start'] : $this->_sections['myloop']['start']+1)/abs($this->_sections['myloop']['step'])), $this->_sections['myloop']['max']);
    if ($this->_sections['myloop']['total'] == 0)
        $this->_sections['myloop']['show'] = false;
} else
    $this->_sections['myloop']['total'] = 0;
if ($this->_sections['myloop']['show']):

            for ($this->_sections['myloop']['index'] = $this->_sections['myloop']['start'], $this->_sections['myloop']['iteration'] = 1;
                 $this->_sections['myloop']['iteration'] <= $this->_sections['myloop']['total'];
                 $this->_sections['myloop']['index'] += $this->_sections['myloop']['step'], $this->_sections['myloop']['iteration']++):
$this->_sections['myloop']['rownum'] = $this->_sections['myloop']['iteration'];
$this->_sections['myloop']['index_prev'] = $this->_sections['myloop']['index'] - $this->_sections['myloop']['step'];
$this->_sections['myloop']['index_next'] = $this->_sections['myloop']['index'] + $this->_sections['myloop']['step'];
$this->_sections['myloop']['first']      = ($this->_sections['myloop']['iteration'] == 1);
$this->_sections['myloop']['last']       = ($this->_sections['myloop']['iteration'] == $this->_sections['myloop']['total']);
?>
											<option value="<?php echo ((is_array($_tmp=$this->_sections['myloop']['index'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
"><?php echo ((is_array($_tmp=$this->_sections['myloop']['index'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
</option>
											<?php endfor; endif; ?>
										</select>
										<?php if (((is_array($_tmp=$this->_tpl_vars['arrErr']['quantity'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)) != ""): ?>
												<p class="attention">選択してください</p>
										<?php endif; ?>
										</p>
									</div>
									<div class="btn_cart"><a href="javascript:void(document.form1.submit())"><img src="<?php echo ((is_array($_tmp=$this->_tpl_vars['TPL_URLPATH'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
img/gift/btn_cart.png" alt="カートに入れる" width="178" height="57"></a></div>
									<?php else: ?>
									<p class="attention">申し訳ありません、ただいま売り切れです</p>
									</div>
									<?php endif; ?>
						</div>
						<p class="caption"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProduct']['main_comment'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('nl2br_html', true, $_tmp) : smarty_modifier_nl2br_html($_tmp)); ?>
</p>
						<ul class="info">
							<?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProduct']['hinban_code'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('strlen', true, $_tmp) : strlen($_tmp)) >= 1): ?>
							<li>品番号	<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProduct']['hinban_code'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('nl2br_html', true, $_tmp) : smarty_modifier_nl2br_html($_tmp)); ?>
</li>
							<?php endif; ?>
							<?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProduct']['maker_code'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('strlen', true, $_tmp) : strlen($_tmp)) >= 1): ?>
							<li>メーカー品番	<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProduct']['maker_code'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('nl2br_html', true, $_tmp) : smarty_modifier_nl2br_html($_tmp)); ?>
</li>
							<?php endif; ?>
							<?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProduct']['maker_name'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('strlen', true, $_tmp) : strlen($_tmp)) >= 1): ?>
							<li>出荷店								<?php $this->assign('maker_url', ((is_array($_tmp=$this->_tpl_vars['arrProduct']['comment1'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp))); ?>
							<?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProduct']['maker_name'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('strlen', true, $_tmp) : strlen($_tmp)) >= 1): ?>
											<?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['maker_url'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('strlen', true, $_tmp) : strlen($_tmp)) >= 1): ?>
													<a href="<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['maker_url'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('h', true, $_tmp) : smarty_modifier_h($_tmp)); ?>
" target="_blank"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProduct']['maker_name'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('h', true, $_tmp) : smarty_modifier_h($_tmp)); ?>
</a>
											<?php else: ?>
													<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProduct']['maker_name'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('h', true, $_tmp) : smarty_modifier_h($_tmp)); ?>

											<?php endif; ?>
							<?php endif; ?>
							</li>
							<?php endif; ?>
							<?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProduct']['moshikomi_code'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('strlen', true, $_tmp) : strlen($_tmp)) >= 1): ?>
							<li>申込番号	<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProduct']['moshikomi_code'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('nl2br_html', true, $_tmp) : smarty_modifier_nl2br_html($_tmp)); ?>
</li>
							<?php endif; ?>
						</ul>
					</div>
				</div>
				<?php endif; ?>
			<?php endif; ?>
    </form>
		</div><!-- main_area -->