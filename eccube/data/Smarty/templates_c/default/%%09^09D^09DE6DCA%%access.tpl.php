<?php /* Smarty version 2.6.27, created on 2015-06-29 16:31:55
         compiled from /home/murahata/www/data/Smarty/templates/default/about/access.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'script_escape', '/home/murahata/www/data/Smarty/templates/default/about/access.tpl', 2, false),)), $this); ?>
	<div id="content" class="cf">
		<h3><img src="<?php echo ((is_array($_tmp=$this->_tpl_vars['TPL_URLPATH'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
img/about/ttl_access.jpg" alt="access アクセス"></h3>

			<ul id="about_area">
				<li class="access">
					<h4>アクセス</h4>
					<div class="cf">
						<div class="ac_left">
							<a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['TPL_URLPATH'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
img/about/cbox_01.jpg" class="colorbox" rel="gl1" title="本店"><img src="<?php echo ((is_array($_tmp=$this->_tpl_vars['TPL_URLPATH'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
img/about/access_img_01.jpg" alt=""><br>本店</a>
							<a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['TPL_URLPATH'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
img/about/cbox_02.jpg" class="colorbox" rel="gl1" title="本店"><img src="<?php echo ((is_array($_tmp=$this->_tpl_vars['TPL_URLPATH'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
img/about/access_img_02.jpg" alt=""><br>本店</a>
							<a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['TPL_URLPATH'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
img/about/cbox_03.jpg" class="colorbox" rel="gl1" title="法人ギフト営業部"><img src="<?php echo ((is_array($_tmp=$this->_tpl_vars['TPL_URLPATH'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
img/about/access_img_03.jpg" alt=""><br>法人ギフト営業部</a>
							<a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['TPL_URLPATH'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
img/about/cbox_04.jpg" class="colorbox" rel="gl1" title="富山店"><img src="<?php echo ((is_array($_tmp=$this->_tpl_vars['TPL_URLPATH'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
img/about/access_img_04.jpg" alt=""><br>富山店</a>
						</div>
						<div class="txt_access">
							<h5>本店</h5>
							<div class="ac_deta cf">
								<p>&#12306;920-0855　金沢市武蔵町2-12　本店第一ビル1F<br>TEL 076-224-6800</p>
								<a href="https://www.google.co.jp/maps/place/%E3%83%95%E3%83%AB%E3%83%BC%E3%83%84%E3%83%91%E3%83%BC%E3%83%A9%E3%83%BC+%E3%82%80%E3%82%89%E3%81%AF%E3%81%9F/@36.570986,136.654466,17z/data=!3m1!4b1!4m2!3m1!1s0x5ff83370121d0b2f:0x4737f3b7b1c8cf03" target="_blank"><img src="<?php echo ((is_array($_tmp=$this->_tpl_vars['TPL_URLPATH'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
img/about/btn_map.jpg" width="63" height="29"></a>
							</div>
							<h5>フルーツパーラー</h5>
							<div class="ac_deta parlor cf">
								<p>&#12306;920-0855　金沢市武蔵町2-12　本店第一ビル2F<br>TEL 076-224-6800<br>営業時間 10:00〜19:00（L.O 18：30）</p>
								<a href="https://www.google.co.jp/maps/place/%E3%83%95%E3%83%AB%E3%83%BC%E3%83%84%E3%83%91%E3%83%BC%E3%83%A9%E3%83%BC+%E3%82%80%E3%82%89%E3%81%AF%E3%81%9F/@36.570986,136.654466,17z/data=!3m1!4b1!4m2!3m1!1s0x5ff83370121d0b2f:0x4737f3b7b1c8cf03" target="_blank"><img src="<?php echo ((is_array($_tmp=$this->_tpl_vars['TPL_URLPATH'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
img/about/btn_map.jpg" width="63" height="29"></a>
							</div>
							<h5>業務用卸部</h5>
							<div class="ac_deta cf">
								<p>&#12306;920-0855　金沢市武蔵町2-12　本店第二ビル1F<br>TEL 076-233-1200</p>
								<a href="https://www.google.co.jp/maps/place/%E3%80%92920-0855/@36.5718507,136.6541487,17z/data=!3m1!4b1!4m2!3m1!1s0x5ff8336fe7ae3b8f:0x79a5302cb5cfea74" target="_blank"><img src="<?php echo ((is_array($_tmp=$this->_tpl_vars['TPL_URLPATH'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
img/about/btn_map.jpg" width="63" height="29"></a>
							</div>
							<h5>法人ギフト営業部</h5>
							<div class="ac_deta cf">
								<p>&#12306;921-0816　野々市市新庄２丁目62番地<br>
								TEL 076-248-7877</p>
								<a href="https://www.google.co.jp/maps/place/%E3%80%92921-8824+%E7%9F%B3%E5%B7%9D%E7%9C%8C%E9%87%8E%E3%80%85%E5%B8%82%E5%B8%82%E6%96%B0%E5%BA%84%EF%BC%92%E4%B8%81%E7%9B%AE%EF%BC%96%EF%BC%92/@36.4970628,136.6093073,17z/data=!3m1!4b1!4m2!3m1!1s0x5ff849ee2be34b19:0x812008c998df1abc" target="_blank"><img src="<?php echo ((is_array($_tmp=$this->_tpl_vars['TPL_URLPATH'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
img/about/btn_map.jpg" width="63" height="29"></a>
							</div>
							<h5>富山店</h5>
							<div class="ac_deta cf">
								<p>&#12306;930-0953　富山県富山市秋吉34-1<br>
								TEL 076-464-5980</p>
								<a href="https://www.google.co.jp/maps/place/%E3%80%92930-0953+%E5%AF%8C%E5%B1%B1%E7%9C%8C%E5%AF%8C%E5%B1%B1%E5%B8%82%E7%A7%8B%E5%90%89%EF%BC%93%EF%BC%94/@36.6844819,137.2470383,17z/data=!3m1!4b1!4m2!3m1!1s0x5ff790e26a291a6d:0xc69ad49c1bf8f886" target="_blank"><img src="<?php echo ((is_array($_tmp=$this->_tpl_vars['TPL_URLPATH'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
img/about/btn_map.jpg" width="63" height="29"></a>
							</div>
							<h5>総務部</h5>
							<div class="ac_deta cf">
								<p>&#12306;920-0855　金沢市武蔵町2-12　本店第二ビル3F<br>
								TEL (代)076-233-1282</p>
								<a href="https://www.google.co.jp/maps/place/%E3%80%92920-0855/@36.5718507,136.6541487,17z/data=!3m1!4b1!4m2!3m1!1s0x5ff8336fe7ae3b8f:0x79a5302cb5cfea74" target="_blank"><img src="<?php echo ((is_array($_tmp=$this->_tpl_vars['TPL_URLPATH'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
img/about/btn_map.jpg" width="63" height="29"></a>
							</div>
							<p class="note">※　写真をクリックしますと、拡大表示されます。</p>
						</div>
					</div>
				</li>
			</ul>

	</div>