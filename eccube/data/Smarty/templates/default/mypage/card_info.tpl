<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->

<div id="mypagecolumn">
	<div id="main_area" class="other_page">
	<h2><!--{$tpl_title|h}--></h2>
	<!--{include file=$tpl_navi}-->
	<div id="mycontents_area">
		<h3>登録済クレジットカード情報</h3>
		<!--{if !$dataHash || $dataHash.KaiinStatus != 0}-->
		<p>登録されているクレジットカード情報はありません。</p>
		<!--{else}-->
		<!--{if $arrErr}-->
		<div class="information">
			<!--{assign var=key value="CardSeq"}-->
			<p class="attentionSt">
				<!--{$arrErr[$key]}-->
			</p>
			<!--{assign var=key value="error"}-->
			<p class="attentionSt">
				<!--{$arrErr[$key]}-->
			</p>
		</div>
		<!--{/if}-->
		<!--{if $success}-->
		<div class="information">
			<p class="attention">更新が完了しました。</p>
		</div>
		<!--{/if}-->

		<table summary="登録済クレジットカード情報">
			<tr>
				<th class="alignC" colspan="2" style="width: 100%;">登録済クレジットカード情報</th>
			</tr>
			<!--{assign var=key1 value="CardSeq"}-->
			<tr>
				<td  class="alignR" style="width: 20%;">カード番号</td><td><!--{$dataHash.CardNo|substr:0:4}-->*********<!--{$dataHash.CardNo|substr:-3}--></td>
			</tr>
			<tr>
				<td class="alignR" style="width: 20%;">有効期限</td><td><!--{$dataHash.CardExp|substr:0:2|h}-->年<!--{$dataHash.CardExp|substr:2:2|h}-->月</td>
			</tr>
				<!--{if $dataHash.HolderName != ''}--><tr><td class="alignR" style="width: 20%;">カード名義</td><td><!--{$dataHash.HolderName}--></td></tr>
				<!--{/if}-->
			</tr>
		</table>

		<div class="btn_area">
			<ul>
				<li>
					<form name="form1" id="form1" method="post" action="?">
						<input type="hidden"
							name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->"
							value="<!--{$transactionid}-->" /> <input type="hidden"
							name="mode" value="inval" /> <input type="submit" class="submit_button"
							value="カード情報を削除する" />
					</form>
				</li>
			</ul>
		</div>
		<!--{/if}-->

		<!--{if !$success}-->
		<h3>
			カード情報を
			<!--{if !$dataHash}-->
			新規
			<!--{else}-->
			更新
			<!--{/if}-->
			登録
		</h3>
		<form name="form2" id="form2" method="post" action="?" autocomplete="off">
			<input type="hidden"
				name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->"
				value="<!--{$transactionid}-->" /> <input type="hidden" name="mode"
				value="<!--{if !$dataHash}-->regist<!--{else}-->change<!--{/if}-->" />
			<div class="information">
				<p>
					下記項目にご入力下さい。「<span class="attention">※</span>」印は入力必須項目です。<br />
					入力後、一番下の「登録する」ボタンをクリックして下さい。
				</p>
				<!--{assign var=key value="error2"}-->
				<p class="attentionSt">
					<!--{$arrErr[$key]}-->
				</p>
			</div>

			<table summary="クレジットカード入力">
				<tr>
					<th class="alignR" style="width: 20%;">カード番号<span class="attention">※</span>
					</th>
					<td style="width: 80%;">
						<!--{assign var=key_CardNo value="CardNo"}--> <!--{if $arrErr[$key_CardNo]}-->
						<span class="attentionSt">
							<!--{$arrErr[$key_CardNo]}-->
					</span>
					<!--{/if}-->
					<input type="text" name="<!--{$key_CardNo}-->" maxlength="<!--{$arrForm[$key_CardNo].length}-->" style="ime-mode: disabled; <!--{$arrErr[$key_CardNo]|sfGetErrorColor}-->" size="16" class="box150" />
					<p class="mini">
						<span class="attention">ハイフンは間に入れず、番号のみを入力してください。</span>
					</p>
					</td>
				</tr>
				<tr>
					<th class="alignR">カード有効期限<span class="attention">※</span>
					</th>
					<td>
						<!--{assign var=key_exp_m value="CardExpMonth"}--> <!--{assign var=key_exp_y value="CardExpYear"}-->
						<!--{if $arrErr[$key_exp_m]}-->
						<span class="attentionSt">
							<!--{$arrErr[$key_exp_m]}-->
					</span>
					<!--{/if}--> <!--{if $arrErr[$key_exp_y]}-->
						<span class="attention">
							<!--{$arrErr[$key_exp_y]}-->
					</span>
					<!--{/if}--> <select name="<!--{$key_exp_m}-->"
						style="<!--{$arrErr[$key_exp_m]|sfGetErrorColor}-->">
							<option value="">&minus;&minus;</option>
							<!--{html_options options=$arrMonth}-->
					</select>月 &nbsp;/&nbsp; 20<select name="<!--{$key_exp_y}-->"
						style="<!--{$arrErr[$key_exp_y]|sfGetErrorColor}-->">
							<option value="">&minus;&minus;</option>
							<!--{html_options options=$arrYear}-->
					</select>年
					</td>
				</tr>
				<!--{if in_array('KanaSei',$arrConfigs.attestation_assistance)}-->
				<tr>
					<th class="alignR">カード名義<span class="attention">※</span>
					</th>
					<td>
						<!--{assign var=key_sei value="KanaSei"}--> <!--{assign var=key_mei value="KanaMei"}-->
						<!--{if $arrErr[$key_sei]}-->
						<span class="attentionSt">
							<!--{$arrErr[$key_sei]}-->
					</span>
					<!--{/if}--> <!--{if $arrErr[$key_mei]}-->
						<span class="attentionSt">
							<!--{$arrErr[$key_mei]}-->
					</span>
					<!--{/if}--> 姓:<input type="text" name="<!--{$key_sei}-->" maxlength="<!--{$arrForm[$key_sei].length}-->" style="<!--{$arrErr[$key_sei]|sfGetErrorColor}-->" size="20" class="box120" /> &nbsp;
					名:
					<input type="text" name="<!--{$key_mei}-->" maxlength="<!--{$arrForm[$key_mei].length}-->" style="<!--{$arrErr[$key_mei]|sfGetErrorColor}-->" size="20" class="box120" />
					&nbsp全角カナ文字入力（例：ヤマダ タロウ）
						<p class="mini">
							<span class="attention">ご本人名義のカードをご使用下さい。</span>
						</p>
					</td>
				</tr>
				<!--{/if}-->

				<!--{if $arrConfigs.SecCd == '1'}-->
				<tr>
					<th class="alignR">セキュリティコード<span class="attention">※</span>
					</th>
					<td>
						<!--{assign var=key value="SecCd"}--> <!--{if $arrErr[$key]}-->
						<span class="attentionSt">
							<!--{$arrErr[$key]}-->
					</span>
					<!--{/if}--> <input type="password" name="<!--{$key}-->" maxlength="<!--{$arrForm[$key].length}-->" style="ime-mode: disabled; <!--{$arrErr[$key]|sfGetErrorColor}-->" size="4" class="box60" />&nbsp半角入力 (例: 123)
						<p class="mini">
							<span class="attention">※カード裏面の署名欄(AMEXは除く）に記載されている末尾３桁～４桁の数字をご記入下さい。<br />※AMEXは表面にあります。(例: 1234)</span>
							<br /> <img
								src="<!--{$smarty.const.PLUGIN_HTML_URLPATH}-->SLNCore/security_code.jpg"
								alt="セキュリティコード" width="410" />
						</p>
					</td>
				</tr>
				<!--{/if}-->

				<!--{if in_array('BirthDay',$arrConfigs.attestation_assistance)}-->
				<tr>
					<th class="alignR">生月日<span class="attention">※</span>
					</th>
					<td>
						<!--{assign var=key value="BirthDay"}--> <!--{if $arrErr[$key]}-->
						<span class="attentionSt">
							<!--{$arrErr[$key]}-->
					</span>
					<!--{/if}--> <input type="text" name="<!--{$key}-->" maxlength="<!--{$arrForm[$key].length}-->" style="ime-mode: disabled; <!--{$arrErr[$key]|sfGetErrorColor}-->" size="4" class="box60" />&nbsp半角入力 (例: 0430)
						<p class="mini">
							<span class="attention">※ご本人の誕生日の月日をご記入下さい(年は必要ありません)。</span>

					</td>
				</tr>
				<!--{/if}-->

				<!--{if in_array('TelNo',$arrConfigs.attestation_assistance)}-->
				<tr>
					<th class="alignR">電話番号(下4桁)<span class="attention">※</span>
					</th>
					<td>
						<!--{assign var=key value="TelNo"}--> <!--{if $arrErr[$key]}-->
						<span class="attentionSt">
							<!--{$arrErr[$key]}-->
					</span>
					<!--{/if}--> <input type="text" name="<!--{$key}-->" maxlength="<!--{$arrForm[$key].length}-->" style="ime-mode: disabled; <!--{$arrErr[$key]|sfGetErrorColor}-->" size="4" class="box60" />&nbsp半角入力 (例: 9876)
						<p class="mini">
							<span class="attention">※カード会社に登録していますご本人の電話番号下4桁をご記入下さい。</span>

					</td>
				</tr>
				<!--{/if}-->
			</table>

			<div class="btn_area">
				<ul>
					<li><input type="submit" class="submit_button" value="カード情報を登録" /></li>
				</ul>
			</div>
		</form>
		<!--{/if}-->
	</div>
</div>
</div>
</div>
