<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->

<!--{strip}-->
    <!--{if count($arrBestProducts) > 0}-->
		<div class="slide">
			<ul id="bxslider_02">
				<!--{foreach from=$arrBestProducts item=arrProduct name="recommend_products"}-->
				<li>
					<img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct.main_list_image|sfNoImageMainList|h}-->" alt="slide">
					<p><!--{$smarty.const.SALE_PRICE_TITLE}--><br>¥<!--{$arrProduct.price02_min_inctax|n2s}--></p>
				</li>
				<!--{if $smarty.foreach.recommend_products.iteration % 2 === 0}-->
				<!--{/if}-->
			<!--{/foreach}-->
			</ul>
		</div>
    <!--{/if}-->
<!--{/strip}-->
