<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->

<script type="text/javascript">//<![CDATA[
    // 規格2に選択肢を割り当てる。
    function fnSetClassCategories(form, classcat_id2_selected) {
        var $form = $(form);
        var product_id = $form.find('input[name=product_id]').val();
        var $sele1 = $form.find('select[name=classcategory_id1]');
        var $sele2 = $form.find('select[name=classcategory_id2]');
        eccube.setClassCategories($form, product_id, $sele1, $sele2, classcat_id2_selected);
    }
//]]></script>
		<div class="cf">
		<div class="side_area">
			<div class="facebook"><iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Ffruitmurahata%3Ffref%3Dts&amp;width=236&amp;height=338&amp;colorscheme=light&amp;show_faces=false&amp;header=true&amp;stream=true&amp;show_border=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:236px; height:338px;" allowtransparency="true"></iframe></div>
			<div class="rec_menu">
			<div class="reccomend">
				<h3><img src="<!--{$TPL_URLPATH}-->img/top/ttl_rec.jpg" alt="おすすめ商品" width="127" height="35"></h3>
                    <!--{* ▼メイン下部 *}-->
                    <!--{if $arrPageLayout.MainFoot|@count > 0}-->
                        <!--{foreach key=MainFootKey item=MainFootItem from=$arrPageLayout.MainFoot}-->
                            <!-- ▼<!--{$MainFootItem.bloc_name}--> -->
                            <!--{if $MainFootItem.php_path != ""}-->
                                <!--{include_php file=$MainFootItem.php_path items=$MainFootItem}-->
                            <!--{else}-->
                                <!--{include file=$MainFootItem.tpl_path items=$MainFootItem}-->
                            <!--{/if}-->
                            <!-- ▲<!--{$MainFootItem.bloc_name}--> -->
                        <!--{/foreach}-->
                    <!--{/if}-->
                    <!--{* ▲メイン下部 *}-->
			</div>
			</div><!-- /reccomend -->
		</div>
		<!--{if count($arrRelativeCat) > 0}-->
		<div id="main_area" class="detail">
			<!--{if $arrRelativeCat.0.0.category_id == 1}-->
				<h3><img src="<!--{$TPL_URLPATH}-->img/cakes/ttl_main.jpg" alt="洋菓子メニュー"></h3>
				<div class="ttl_deta cf">
					<h4><!--{$arrProduct.name|h}--></h4>
					<p class="his_back"><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=1"><span>&#x25B6;</span>商品一覧に戻る</a></p>
				</div>
				<!--{elseif $arrRelativeCat.0.0.category_id == 6}-->
				<h3><img src="<!--{$TPL_URLPATH}-->img/gift/ttl_main.jpg" alt="ショッピングメニュー"></h3>
				<div class="ttl_deta cf">
					<h4><!--{$arrProduct.name|h}--></h4>
					<p class="his_back"><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=6"><span>&#x25B6;</span>商品一覧に戻る</a></p>
				</div>
				<!--{/if}-->
			<!--{/if}-->
			<div class="detail_area cf">
				<div class="deta_left">
                    <!--{assign var=key value="main_image"}-->
                    <!--★画像★-->
                    <!--{if $arrProduct.main_large_image|strlen >= 1}-->
                        <a
                            href="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct.main_large_image|h}-->"
                            class="colorbox" 
														title="<!--{$arrProduct.name|h}-->"
                        >
                    <!--{/if}-->
                        <img src="<!--{$arrFile[$key].filepath|h}-->" alt="<!--{$arrProduct.name|h}-->" class="picture" />
                    <!--{if $arrProduct.main_large_image|strlen >= 1}-->
                        </a>
                    <!--{/if}-->
				</div>
				<!--{if count($arrRelativeCat) > 0}-->
					<!--{if $arrRelativeCat.0.0.category_id == 1}-->
				<div class="deta_right cakes_datail">
					<p class="price"><!--{* 規格テーブル *}-->
								<!--{section name=i loop=$arrProductsClassList}-->
								税込 <!--{$arrProductsClassList[i].price02|sfCalcIncTax:$arrSiteInfo.tax:$arrSiteInfo.tax_rule|number_format}-->円<!--{if $arrProductsClassList[i].classcategory_name1 != ''}--> / <!--{$arrProductsClassList[i].classcategory_name1|h}--><br>
								<!--{/if}--> <!--{if $arrProductsClassList[i].classcategory_name2 != ''}--> <!--{$arrProductsClassList[i].classcategory_name2|h}--><br>
								<!--{/if}--><!--{/section}--></p>
						<p class="caption"><!--{$arrProduct.main_comment|nl2br_html}--></p>
						<p class="notice">※洋菓子メニューはむらはたのフルーツパーラーにて<br>&emsp;販売しています。</p>
					</div>
					<!--{elseif $arrRelativeCat.0.0.category_id == 6 || $arrRelativeCat.0.0.category_id == 7 || $arrRelativeCat.0.0.category_id == 8}-->
					<form name="form1" id="form1" method="post" action="?">
					<input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
					<input type="hidden" name="mode" value="cart" />
					<input type="hidden" name="product_id" value="<!--{$tpl_product_id}-->" />
					<input type="hidden" name="product_class_id" value="<!--{$tpl_product_class_id}-->" id="product_class_id" />
					<input type="hidden" name="favorite_product_id" value="" />
					<div class="deta_right gift_datail">
						<div class="price cf">
							<div class="left">
								<p><!--{* 規格テーブル *}-->
								<!--{section name=i loop=$arrProductsClassList}-->
								税込 <!--{$arrProductsClassList[i].price02|sfCalcIncTax:$arrSiteInfo.tax:$arrSiteInfo.tax_rule|number_format}-->円<!--{if $arrProductsClassList[i].classcategory_name1 != ''}--> / <!--{$arrProductsClassList[i].classcategory_name1|h}--><br>
								<!--{/if}--> <!--{if $arrProductsClassList[i].classcategory_name2 != ''}--> <!--{$arrProductsClassList[i].classcategory_name2|h}--><br>
								<!--{/if}--><!--{/section}--></p>
								<!--{if $tpl_stock_find}-->
									<p class="num">個 数
										<select name="quantity">
											<option value="選択">&#9660;選択</option>
											<!--{section name=myloop start=1 loop=21 step=1}-->
											<option value="<!--{$smarty.section.myloop.index}-->"><!--{$smarty.section.myloop.index}--></option>
											<!--{/section}-->
										</select>
										<!--{if $arrErr.quantity != ""}-->
												<p class="attention">選択してください</p>
										<!--{/if}-->
										</p>
									</div>
									<div class="btn_cart"><a href="javascript:void(document.form1.submit())"><img src="<!--{$TPL_URLPATH}-->img/gift/btn_cart.png" alt="カートに入れる" width="178" height="57"></a></div>
									<!--{else}-->
									<p class="attention">申し訳ありません、ただいま売り切れです</p>
									</div>
									<!--{/if}-->
						</div>
						<p class="caption"><!--{$arrProduct.main_comment|nl2br_html}--></p>
						<ul class="info">
							<!--{if $arrProduct.hinban_code|strlen >= 1}-->
							<li>品番号	<!--{$arrProduct.hinban_code|nl2br_html}--></li>
							<!--{/if}-->
							<!--{if $arrProduct.maker_code|strlen >= 1}-->
							<li>メーカー品番	<!--{$arrProduct.maker_code|nl2br_html}--></li>
							<!--{/if}-->
							<!--{if $arrProduct.maker_name|strlen >= 1}-->
							<li>出荷店	<!--{* ▼メーカー *}-->
							<!--{assign var=maker_url value=$arrProduct.comment1}-->
							<!--{if $arrProduct.maker_name|strlen >= 1}-->
											<!--{if $maker_url|strlen >= 1}-->
													<a href="<!--{$maker_url|h}-->" target="_blank"><!--{$arrProduct.maker_name|h}--></a>
											<!--{else}-->
													<!--{$arrProduct.maker_name|h}-->
											<!--{/if}-->
							<!--{/if}-->
							<!--{* ▲メーカー *}--></li>
							<!--{/if}-->
							<!--{if $arrProduct.moshikomi_code|strlen >= 1}-->
							<li>申込番号	<!--{$arrProduct.moshikomi_code|nl2br_html}--></li>
							<!--{/if}-->
						</ul>
					</div>
				</div>
				<!--{/if}-->
			<!--{/if}-->
    </form>
		</div><!-- main_area -->
