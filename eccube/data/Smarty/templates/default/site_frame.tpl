<!--{printXMLDeclaration}--><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->
<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ja" xml:lang="ja">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$smarty.const.CHAR_CODE}-->" />
<title><!--{if $tpl_subtitle|strlen >= 1}--><!--{$tpl_subtitle|h}--> / <!--{elseif $tpl_title|strlen >= 1}--><!--{$tpl_title|h}--> / <!--{/if}--><!--{$arrSiteInfo.shop_name|h}--></title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta name="viewport" content="width=1080, user-scalable=yes" >
<link rel="stylesheet" href="http://www.css-reset.com/css/meyer.css" />
<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->js/jquery.bxslider/jquery.bxslider.css" />
<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->js/colorbox-master/colorbox.css" type="text/css" media="all" />
<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->css/import.css" type="text/css" media="all" />
<!--{assign var=page_concept value="`$smarty.const.ROOT_URLPATH`concept/index.php"}-->
<!--{assign var=page_about1 value="`$smarty.const.ROOT_URLPATH`about/index.php"}-->
<!--{assign var=page_about2 value="`$smarty.const.ROOT_URLPATH`about/greeting.php"}-->
<!--{assign var=page_about3 value="`$smarty.const.ROOT_URLPATH`about/history.php"}-->
<!--{assign var=page_about4 value="`$smarty.const.ROOT_URLPATH`about/story.php"}-->
<!--{assign var=page_about5 value="`$smarty.const.ROOT_URLPATH`about/access.php"}-->
<!--{if $arrPageLayout.author|strlen >= 1}-->
    <meta name="author" content="<!--{$arrPageLayout.author|h}-->" />
<!--{/if}-->
<meta name="Description" content="フルーツむらはたでは様々なフルーツの販売を行っております。フルーツの他、フルーツを使用したゼリー、ジャムなどの加工食品の販売も行っております。" />
<!--{if $arrPageLayout.description|strlen >= 1}-->
    <meta name="description" content="<!--{$arrPageLayout.description|h}-->" />
<!--{elseif $arrPageLayout.keyword|strlen >= 1}-->
    <meta name="keywords" content="<!--{$arrPageLayout.keyword|h}-->" />
<!--{elseif $arrPageLayout.meta_robots|strlen >= 1}-->
    <meta name="robots" content="<!--{$arrPageLayout.meta_robots|h}-->" />
<!--{/if}-->
<!--{if $tpl_page_class_name == "LC_Page_Index" }-->
		<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->css/top.css" type="text/css" media="all" />
<!--{elseif ($tpl_page_class_name == "LC_Page_Products_Detail" || $tpl_page_class_name == "LC_Page_Products_List")&& $tpl_page_class_name != "LC_Page_Index" }-->
		<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->css/cakes.css" type="text/css" media="all" />
		<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->css/gift.css" type="text/css" media="all" />
    <link rel="stylesheet" href="<!--{$smarty.const.ROOT_URLPATH}-->js/jquery.ui/theme/jquery.ui.core.css" type="text/css" media="all" />
    <link rel="stylesheet" href="<!--{$smarty.const.ROOT_URLPATH}-->js/jquery.ui/theme/jquery.ui.tooltip.css" type="text/css" media="all" />
    <link rel="stylesheet" href="<!--{$smarty.const.ROOT_URLPATH}-->js/jquery.ui/theme/jquery.ui.theme.css" type="text/css" media="all" />
<!--{elseif $smarty.server.PHP_SELF==$page_concept}-->
		<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->css/concept.css" type="text/css" media="all" />
<!--{elseif $smarty.server.PHP_SELF==$page_about1 ||  $smarty.server.PHP_SELF==$page_about2 || $smarty.server.PHP_SELF==$page_about3 || $smarty.server.PHP_SELF==$page_about4 || $smarty.server.PHP_SELF==$page_about5 }-->
		<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->css/about.css" type="text/css" media="all" />
<!--{else}-->
		<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->css/other.css" type="text/css" media="all" />
<!--{/if}-->
<link rel="alternate" type="application/rss+xml" title="RSS" href="<!--{$smarty.const.HTTP_URL}-->rss/<!--{$smarty.const.DIR_INDEX_PATH}-->" />
<!--[if lt IE 9]>
<script src="<!--{$smarty.const.ROOT_URLPATH}-->js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->
<script src="<!--{$smarty.const.ROOT_URLPATH}-->js/jquery-2.1.1.min.js"></script>
<!--<![endif]-->
<script type="text/javascript" src="<!--{$smarty.const.ROOT_URLPATH}-->js/eccube.js"></script>
<!-- #2342 次期メジャーバージョン(2.14)にてeccube.legacy.jsは削除予定.モジュール、プラグインの互換性を考慮して2.13では残します. -->
<script type="text/javascript" src="<!--{$smarty.const.ROOT_URLPATH}-->js/eccube.legacy.js"></script>
<!--{if $tpl_page_class_name === "LC_Page_Abouts"}-->
    <!--{if ($smarty.server.HTTPS != "") && ($smarty.server.HTTPS != "off")}-->
        <script type="text/javascript" src="https://maps-api-ssl.google.com/maps/api/js?sensor=false"></script>
    <!--{else}-->
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <!--{/if}-->
<!--{/if}-->
<script type="text/javascript" src="<!--{$TPL_URLPATH}-->js/common.js"></script>
<script type="text/javascript" src="<!--{$TPL_URLPATH}-->js/jquery.bxslider/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="<!--{$TPL_URLPATH}-->js/jQueryAutoHeight.js"></script>
<script type="text/javascript" src="<!--{$TPL_URLPATH}-->js/colorbox-master/jquery.colorbox-min.js"></script>
	<script type="text/javascript">
	$(function(){
//		$('#bxslider').bxSlider();
		$('#bxslider_02').bxSlider({
			pager: false,
			controls: true,
			slideWidth: 134
		});
		$('.colorbox').colorbox();
		$('.cake_menu li,.gift_menu li').autoHeight({column:4, clear:1});
	});
	</script>

<!--{strip}-->
    <!--{* ▼Head COLUMN*}-->
    <!--{if $arrPageLayout.HeadNavi|@count > 0}-->
        <!--{* ▼上ナビ *}-->
        <!--{foreach key=HeadNaviKey item=HeadNaviItem from=$arrPageLayout.HeadNavi}-->
            <!--{* ▼<!--{$HeadNaviItem.bloc_name}--> *}-->
            <!--{if $HeadNaviItem.php_path != ""}-->
                <!--{include_php file=$HeadNaviItem.php_path items=$HeadNaviItem}-->
            <!--{else}-->
                <!--{include file=$HeadNaviItem.tpl_path}-->
            <!--{/if}-->
            <!--{* ▲<!--{$HeadNaviItem.bloc_name}--> *}-->
        <!--{/foreach}-->
        <!--{* ▲上ナビ *}-->
    <!--{/if}-->
    <!--{* ▲Head COLUMN*}-->
<!--{/strip}-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63795719-1', 'auto');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

</script>
</head>

<!-- ▼BODY部 スタート -->
<!--{include file='./site_main.tpl'}-->
<!-- ▲BODY部 エンド -->

</html>
