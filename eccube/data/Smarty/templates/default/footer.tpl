<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->

<!--▼FOOTER-->
<!--{strip}-->
<footer class="cf">
	<nav>
	<ul class="cf">
		<li><a href="<!--{$smarty.const.TOP_URL}-->">ホーム</a></li>
		<li><a href="<!--{$smarty.const.HTTPS_URL}-->mypage/login.php">マイページ</a></li>
		<li><a href="<!--{$smarty.const.CART_URL}-->">カート</a></li>
		<li><a href="<!--{$smarty.const.TOP_URL}-->guide/">ご利用ガイド</a></li>
		<li><a href="<!--{$smarty.const.TOP_URL}-->about/">会社概要</a></li>
		<li><a href="<!--{$smarty.const.TOP_URL}-->guide/privacy.php">個人情報保護方針</a></li>
		<li><a href="<!--{$smarty.const.TOP_URL}-->order/index.php">特定商取引に関する法律に基づく表記</a></li>
	</ul>
	</nav>
	<div class="copy"><p>Copyright Fruit Murahata,Inc.All Rights Reserved.</p></div>
	<ul class="any_country cf">
		<li><a href="<!--{$smarty.const.TOP_URL}-->tw/" target="_blank"><img src="<!--{$TPL_URLPATH}-->img/common/btn_tw.jpg" alt="" width="25" height="15"></a></li>
		<li><a href="<!--{$smarty.const.TOP_URL}-->en/" target="_blank"><img src="<!--{$TPL_URLPATH}-->img/common/btn_en.jpg" alt="" width="25" height="15"></a></li>
	</ul>
</footer>
<!--{/strip}-->
<!--▲FOOTER-->
