<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA    02111-1307, USA.
 *}-->

<!--▼HEADER-->
<!--{strip}-->
	<header>
		<div id="header" class="cf">
			<h1><a href="<!--{$smarty.const.TOP_URL}-->"><img src="<!--{$TPL_URLPATH}-->img/common/main_logo.jpg" alt="MURAHATA" width="98" height="40"></a></h1>
			<ul class="fll cf">
				<li><a href="<!--{$smarty.const.TOP_URL}-->"><img src="<!--{$TPL_URLPATH}-->img/common/nav_home_off.jpg" alt="Home - ホーム" width="91" height="102"></a></li>
<!--{assign var=page_concept value="`$smarty.const.ROOT_URLPATH`concept/index.php"}-->
<!--{if $smarty.server.PHP_SELF==$page_concept}-->
				<li><a href="<!--{$smarty.const.TOP_URL}-->concept/" class="on"><img src="<!--{$TPL_URLPATH}-->img/common/nav_concept_on.jpg" alt="Concept - コンセプト" width="91" height="102"></a></li>
				<!--{else}-->
				<li><a href="<!--{$smarty.const.TOP_URL}-->concept/"><img src="<!--{$TPL_URLPATH}-->img/common/nav_concept_off.jpg" alt="Concept - コンセプト" width="91" height="102"></a></li>
<!--{/if}-->
<!--{if ($arrSearchData.category_id == 1 || $arrRelativeCat.0.0.category_id == 1) && $tpl_page_class_name !== "LC_Page_Index" }-->
				<li><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=1" class="on"><img src="<!--{$TPL_URLPATH}-->img/common/nav_cakes_on.jpg" alt="Cakes - 洋菓子メニュー" width="91" height="102"></a></li>
				<!--{else}-->
				<li><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=1"><img src="<!--{$TPL_URLPATH}-->img/common/nav_cakes_off.jpg" alt="Cakes - 洋菓子メニュー" width="91" height="102"></a></li>
<!--{/if}-->
<!--{if $arrSearchData.category_id == 6 || $arrSearchData.category_id == 7 || $arrSearchData.category_id == 8 || $arrRelativeCat.0.0.category_id == 6 || $arrRelativeCat.0.0.category_id == 7 || $arrRelativeCat.0.0.category_id == 8 }-->
				<li><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=6" class="on"><img src="<!--{$TPL_URLPATH}-->img/common/nav_gift_on.jpg" alt="Shop - ショッピング" width="91" height="102"></a></li>
				<!--{else}-->
				<li><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=6"><img src="<!--{$TPL_URLPATH}-->img/common/nav_gift_off.jpg" alt="Gift - ギフト一覧" width="91" height="102"></a></li>
<!--{/if}-->
<!--{assign var=page_about1 value="`$smarty.const.ROOT_URLPATH`about/index.php"}-->
<!--{assign var=page_about2 value="`$smarty.const.ROOT_URLPATH`about/greeting.php"}-->
<!--{assign var=page_about3 value="`$smarty.const.ROOT_URLPATH`about/history.php"}-->
<!--{assign var=page_about4 value="`$smarty.const.ROOT_URLPATH`about/story.php"}-->
<!--{if $smarty.server.PHP_SELF==$page_about1 ||  $smarty.server.PHP_SELF==$page_about2 || $smarty.server.PHP_SELF==$page_about3 || $smarty.server.PHP_SELF==$page_about4 }-->
				<li><a href="<!--{$smarty.const.TOP_URL}-->about/" class="on"><img src="<!--{$TPL_URLPATH}-->img/common/nav_about_on.jpg" alt="About - 会社概要" width="91" height="102"></a></li>
				<!--{else}-->
				<li><a href="<!--{$smarty.const.TOP_URL}-->about/"><img src="<!--{$TPL_URLPATH}-->img/common/nav_about_off.jpg" alt="About - 会社概要" width="91" height="102"></a></li>
<!--{/if}-->
<!--{assign var=page_about5 value="`$smarty.const.ROOT_URLPATH`about/access.php"}-->
<!--{if $smarty.server.PHP_SELF==$page_about5 }-->
				<li><a href="<!--{$smarty.const.TOP_URL}-->about/access.php" class="on"><img src="<!--{$TPL_URLPATH}-->img/common/nav_access_on.jpg" alt="Access - アクセス" width="91" height="102"></a></li>
				<!--{else}-->
				<li><a href="<!--{$smarty.const.TOP_URL}-->about/access.php"><img src="<!--{$TPL_URLPATH}-->img/common/nav_access_off.jpg" alt="Access - アクセス" width="91" height="102"></a></li>
<!--{/if}-->
			</ul>
			<div id="head_right">
				<ul class="mypage cf">
					<li><a href="<!--{$smarty.const.HTTPS_URL}-->mypage/login.php"><span>マイページ</span></a></li>
					<li class="cart"><a href="<!--{$smarty.const.CART_URL}-->">カート <span>(<!--{include_php file=`$smarty.const.HTML_REALDIR`frontparts/bloc/cart.php}-->)</span></a></li>
				</ul>
				<ul class="guide cf">
					<li><a href="<!--{$smarty.const.TOP_URL}-->guide/"><img src="<!--{$TPL_URLPATH}-->img/common/btn_guide.jpg" alt="ご利用ガイド" width="77" height="10"></a></li>
					<li><a href="<!--{$smarty.const.TOP_URL}-->contact/"><img src="<!--{$TPL_URLPATH}-->img/common/btn_inq.jpg" alt="お問い合わせ" width="78" height="10"></a></li>
				</ul>
				<img src="<!--{$TPL_URLPATH}-->img/common/img_tel.jpg" alt="TEL 076-224-6800" width="200" height="36" class="head_tel">
			</div><!-- /head_right -->
		</div>
	</header>
<!--{/strip}-->
<!--▲HEADER-->
