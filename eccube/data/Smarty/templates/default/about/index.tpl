	<div id="content" class="cf">
		<h3><img src="<!--{$TPL_URLPATH}-->img/about/ttl_main.jpg" alt="about 会社概要"></h3>
		<div class="side_area">
			<div class="ttl_cate cf">
				<ul class="cate_list cf">
					<li><a href="<!--{$smarty.const.TOP_URL}-->about/"><span class="red_rarr">&#x25B6;</span>会社概要</a></li>
					<li><a href="<!--{$smarty.const.TOP_URL}-->about/greeting.php"><span class="red_rarr">&#x25B6;</span>代表ご挨拶</a></li>
					<li><a href="<!--{$smarty.const.TOP_URL}-->about/history.php"><span class="red_rarr">&#x25B6;</span>沿革</a></li>
					<li><a href="<!--{$smarty.const.TOP_URL}-->about/story.php"><span class="red_rarr">&#x25B6;</span>会社ストーリー</a></li>
				</ul>
			</div>
		</div>

		<div id="main_area">
			<ul id="about_area">
				<li class="outline">
					<h4>会社概要</h4>
					<table>
						<tr>
							<th>商号</th>
							<td>株式会社フルーツむらはた</td>
						</tr>
						<tr>
							<th>所在地</th>
							<td>
								<dl class="cf">
									<dt>本社</dt>
									<dd>&#12306;920-0855　石川県金沢市武蔵町2-12<br>TEL 076-233-1282　FAX 076-261-0202<br>	E-mail <a href="mailto:shop@murahata.co.jp">shop@murahata.co.jp</a><br>URL <a href="http://www.murahata.co.jp" target="_blank">http://www.murahata.co.jp</a></dd>
									<dt>野々市店</dt>
									<dd>&#12306;921-8824  石川県野々市市新庄2-62<br>	TEL 076-248-7877　FAX 076-248-7885<br>	E-mail <a href="mailto:gift_sales@murahata.co.jp">gift_sales@murahata.co.jp</a></dd>
									<dt>富山店</dt>
									<dd>&#12306;930-0953　富山県富山市秋吉34-1<br>TEL 076-464-5980　FAX 076-464-5981<br>	E-mail <a href="mailto:toyama-b@murahata.co.jp">toyama-b@murahata.co.jp</a></dd>
								</dl>
							</td>
						</tr>
						<tr>
							<th>代表取締役会長</th>
							<td>村端　儀一</td>
						</tr>
						<tr>
							<th>代表取締役社長</th>
							<td>村端　一男</td>
						</tr>
						<tr>
							<th>創業</th>
							<td>大正3年4月</td>
						</tr>
						<tr>
							<th>資本金</th>
							<td>1,000万円</td>
						</tr>
						<tr>
							<th>従業員数</th>
							<td>50名(パート含む) </td>
						</tr>
						<tr>
							<th>主な取引先</th>
							<td>石川・富山・福井県のホテル、料亭、洋菓子店</td>
						</tr>
						<tr>
							<th>取扱種目</th>
							<td>高級果物、業務用フルーツ、カットフルーツ、洋菓子、<br>
							各種ギフト</td>
						</tr>
						<tr>
							<th>取引銀行</th>
							<td>北國銀行本店、北陸銀行東大通支店、金沢信用金庫武蔵支店、<br>
							三井住友銀行金沢支店、金沢中央信用組合本店、城北信用金庫築地支店</td>
						</tr>
					</table>
				</li>
			</ul>
		</div><!-- main_area -->

	</div>
