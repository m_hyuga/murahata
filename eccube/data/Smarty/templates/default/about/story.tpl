	<div id="content" class="cf">
		<h3><img src="<!--{$TPL_URLPATH}-->img/about/ttl_main.jpg" alt="about 会社概要"></h3>
		<div class="side_area">
			<div class="ttl_cate cf">
				<ul class="cate_list cf">
					<li><a href="<!--{$smarty.const.TOP_URL}-->about/"><span class="red_rarr">&#x25B6;</span>会社概要</a></li>
					<li><a href="<!--{$smarty.const.TOP_URL}-->about/greeting.php"><span class="red_rarr">&#x25B6;</span>代表ご挨拶</a></li>
					<li><a href="<!--{$smarty.const.TOP_URL}-->about/history.php"><span class="red_rarr">&#x25B6;</span>沿革</a></li>
					<li><a href="<!--{$smarty.const.TOP_URL}-->about/story.php"><span class="red_rarr">&#x25B6;</span>会社ストーリー</a></li>
				</ul>
			</div>
		</div>

		<div id="main_area">
			<ul id="about_area">
				<li class="story">
					<h4>会社ストーリー</h4>
					<div class="cf">
						<div class="st_left"><img src="<!--{$TPL_URLPATH}-->img/about/story_img_01.jpg" alt=""><img src="<!--{$TPL_URLPATH}-->img/about/story_img_02.jpg" alt=""><img src="<!--{$TPL_URLPATH}-->img/about/story_img_03.jpg" alt=""><img src="<!--{$TPL_URLPATH}-->img/about/story_img_04.jpg" alt=""></div>
						<div class="txt_story">
							<h5><span>創業〜疎開</span></h5>
							<p>1914年（大正３年）、村端与三松によって東京都文京区に加賀屋果物店として創業致しました。その後、起こる第二次世界大戦による被災の為、東京店舗の閉店を余儀なくされ石川県金沢市に疎開しました。</p>
							<h5><span>疎開〜事業再開</span></h5>
							<p>戦後復興の渦中であった1946年（昭和21年）、石川県金沢市近江町市場にて村端果物店として事業再開を果たします。</p>
							<h5><span>ブランド化に着手</span></h5>
							<p>昭和30年代に入ると経済成長に伴い豊かになっていく市場から、顧客ニーズは専門店の時代になってくると捉え高級フルーツ販売に力を注いでいきます。<br>同時に、その当時では珍しい自社包装紙の開発などブランド化を図り専門店としての認知度向上に努めました。</p>
							<h5><span>バナナ店/籠盛店/業務用卸部開業</span></h5>
							<p>昭和40年代に入ると経済成長著しい市場に合わせ、近江町市場内にバナナ専門店、冠婚葬祭が大衆化されていくにつれて需要の伸びに応えるべく籠盛店を開業していきます。同時に料亭や旅館の厨房でのフルーツ需要も増えていった時代でした。<br>またその頃、洋菓子業界でもひとつの変革の時代を迎えていました。バタークリームから生クリームが主役になってきたのです。生クリームと生のフルーツとの組合せが好評を得てフルーツの需要が増えていったのもこの時代でした。<br><br>以降、フルーツやフルーツギフトは日本人のライフスタイルに無くてはならないものになってくるであろうと考え、高品質の選果システムの構築、ラッピングへのこだわり等を追求してきました。</p>
						</div>
					</div>
				</li>
			</ul>
		</div><!-- main_area -->

	</div>
