	<div id="content" class="cf">
		<h3><img src="<!--{$TPL_URLPATH}-->img/about/ttl_main.jpg" alt="about 会社概要"></h3>
		<div class="side_area">
			<div class="ttl_cate cf">
				<ul class="cate_list cf">
					<li><a href="<!--{$smarty.const.TOP_URL}-->about/"><span class="red_rarr">&#x25B6;</span>会社概要</a></li>
					<li><a href="<!--{$smarty.const.TOP_URL}-->about/greeting.php"><span class="red_rarr">&#x25B6;</span>代表ご挨拶</a></li>
					<li><a href="<!--{$smarty.const.TOP_URL}-->about/history.php"><span class="red_rarr">&#x25B6;</span>沿革</a></li>
					<li><a href="<!--{$smarty.const.TOP_URL}-->about/story.php"><span class="red_rarr">&#x25B6;</span>会社ストーリー</a></li>
				</ul>
			</div>
		</div>

		<div id="main_area">
			<ul id="about_area">
				<li class="history">
					<h4>沿革</h4>
					<table>
						<tr>
							<th>創業（大正3年）</th>
							<td>村端与三松、東京都文京区白山にて加賀屋果物店を創業</td>
						</tr>
						<tr>
							<th>設立（昭和21年3月）</th>
							<td>被災の為閉店、金沢に疎開</td>
						</tr>
						<tr>
							<th>昭和21年2月</th>
							<td>金沢市近江町市場に村端果物店として再開</td>
						</tr>
						<tr>
							<th>昭和42年6月</th>
							<td>バナナ店を近江町市場内に出店、バナナ専門の卸、小売を営業</td>
						</tr>
						<tr>
							<th>昭和46年9月</th>
							<td>冠婚葬祭かご盛店を武蔵町13-28に出店、結婚式場専門にかご盛の卸業務を計る</td>
						</tr>
						<tr>
							<th>昭和53年5月</th>
							<td>株式会社フルーツむらはたに改組</td>
						</tr>
						<tr>
							<th>昭和58年4月</th>
							<td>本店第一ビル・第二ビル完成、業務用卸部を本店第二ビル内に併合</td>
						</tr>
						<tr>
							<th>昭和63年9月</th>
							<td>石川郡野々市町（現 野々市市）新庄にかご盛工場（600坪）を建設</td>
						</tr>
						<tr>
							<th>昭和63年12月</th>
							<td>金沢市下近江町26番地（近江町市場内）に地下1F地上4Fの近江町店をオープン</td>
						</tr>
						<tr>
							<th>平成14年4月</th>
							<td>フルーツパーラーリニューアルオープン</td>
						</tr>
						<tr>
							<th>平成18年9月</th>
							<td>タイ王国にて関連会社MURAHATA（THAILAND）CO.,LTD.設立</td>
						</tr>
						<tr>
							<th>平成18年12月</th>
							<td>バンコク市内のショッピングモールCENTRALWORLDに<br>
							FRUITS PARLORをオープン</td>
						</tr>
						<tr>
							<th>平成19年8月</th>
							<td>近江町店を閉店</td>
						</tr>
						<tr>
							<th>平成22年3月</th>
							<td>バンコク店閉鎖</td>
						</tr>
						<tr>
							<th>平成24年10月</th>
							<td>富山営業所（物流拠点）開設</td>
						</tr>
						<tr>
							<th>平成26年8月</th>
							<td>タイ王国にて関連会社MURAHATA FRUIT CO., LTD.設立</td>
						</tr>
						<tr>
							<th>平成26年9月</th>
							<td>富山営業所を富山県富山市秋吉に移転し、富山店としてオープン</td>
						</tr>
						<tr>
							<th>平成27年3月</th>
							<td>MURAHATA (THAILAND) CO., LTD解散手続き完了</td>
						</tr>
					</table>
				</li>
			</ul>
		</div><!-- main_area -->

	</div>
