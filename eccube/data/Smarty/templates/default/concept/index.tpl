	<div id="content" class="cf">
		<h3><img src="<!--{$TPL_URLPATH}-->img/concept/ttl_main.jpg" alt="コンセプト"></h3>

		<div id="main_area">
			<div class="bg_rep">
				<h4><img src="<!--{$TPL_URLPATH}-->img/concept/img_catch.png" alt="フルーツ専門店としてフルーツの価値、美しさを追求し提案・販売を通じて社会に貢献し食文化の進展に寄与する" width="518" height="54"></h4>
				<p>わたしたちはフルーツの専門店として<br>どこよりもフルーツの価値とその美しさを追求し続けます。<br>わたしたちはお客さまにより質の高いフルーツの<br>提案および販売を通じて社会に貢献し<br>食文化の進展に寄与する「むらはた」でありたいと考えています。</p>
			</div>
		</div><!-- main_area -->

	</div>
